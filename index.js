require('dotenv-flow').config({silent: true});
const Koa = require('koa')
const path = require('path')
const staticFIles = require('koa-static')
const fs = require('fs')
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const errorHandle = require('koa-json-error');
const cors = require('koa2-cors');
const jwtMiddleware = require('koa-jwt');

const knex = require('./src/libs/knexConfig');
const {jwtDecode, jwtEncode} = require('./src/libs/jwt');
const {comparePassword, cryptPassword} = require('./src/libs/bcrypt');
const {randomString} = require('./src/components/data/random');

const server = require('./src/server');
const { createReadStream } = require ('fs');
const helmet = require("koa-helmet");

const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.REACT_APP_GOOGLE_LOGIN);

const router = new Router();
const app = new Koa()

app.use(helmet());

app.use(errorHandle({
    format: function formatError(err) {
    	return {
    		res: false,
    		errors: err.message
    	};
    }
}));

if (process.env.NODE_ENV === 'development') {
	app.use(cors({
		origin: '*',
	})); 
}

if (process.env.NODE_ENV === 'production') {
	// app.use(cors({
	// 	origin: process.env.HTTPS_DOMAIN
	// })); 
}

app.use(staticFIles(path.resolve('build')));
app.use(staticFIles(path.resolve('public')));

app.use(async (ctx, next) => {
	if (ctx.request.url !== '/api/user-check-google' && (ctx.request.url).includes('api/')){
		app.use(jwtMiddleware({
			secret: process.env.REACT_APP_JWT_SECRET,
		}));
		await next();
	} else {
		await next();
	}
})


// app.use(
//   jwtMiddleware({
//     secret: process.env.REACT_APP_JWT_SECRET,
//   }).unless({
//     path: [
//     	'/',
//     	'/login',
//     	'/dashboard',
// 			'/dashboard/add-monitor',
// 			'/dashboard/edit-monitor',
// 			'/charts',
// 			'/charts-part2',
// 			'/charts-part3',
// 			'/forms',
// 			'/validation',
// 			'/images',
// 			'/table2',
// 			'/others',
// 			'/maps',
// 			'/reporting-API',
// 			'/reporting-API-page2',
// 			'/start',
// 			'/call-tracking',
// 			'/choose-elements',
// 			'/management',
// 			'/google-search-console',
// 			'/cro',
//
// 			'/api/user-check-google',
//     	//'/api/user_check'
//     ],
//   }),
// );

// app.use(function(ctx){
// 	if (ctx.url.match(/^\/api/)) {
// 		ctx.body = 'protected\n';
// 	}
// });

app.use(async (ctx, next) => {
   try {
   // 		function dontAccess(){ можно установить просроченный токен, дабы фронт отваливался
   // 			return {
   // 				body: false,
   // 				token: false
   // 			}
			// }

      if (ctx.request.url !== '/api/user-check-google' && (ctx.request.url).includes('api/')){
     		const tokenAuth = ctx.request.header.authorization

     		const resultToken = tokenAuth.substr(7);
     		const jwtRes = await jwtDecode(resultToken);

     		let objectResult = {}

     		if (jwtRes &&
     			jwtRes.hasOwnProperty('userId') && //+
     			jwtRes.hasOwnProperty('time') &&  //+
     			jwtRes.hasOwnProperty('key') && //+
     			jwtRes.hasOwnProperty('iat') &&
     			jwtRes.hasOwnProperty('exp') //!
     			) {
     			const userArray = await knex('users')
     				.select('*')
     				.where('userId', jwtRes['userId'])
     			if (userArray.length === 0){
     				await next()
     				ctx.response.body = {
     					body: false,
     					token: 'error'
     				}
     				return;
     			}

     			const correctKey = await comparePassword(jwtRes['key'],userArray[0]['key']);
     			
     			if (!correctKey){
     				await next()
     				ctx.response.body = {
     					body: false,
     					token: 'error'
     				}
     				return;
     			}

     			const dateNow = new Date();
     			if (dateNow.getTime() > jwtRes['time']) {
     				const randString = randomString(20);
						const key = await cryptPassword(randString);

     				const token = await jwtEncode({
							userId: jwtRes['userId'],
							time: dateNow.getTime() + parseInt(process.env.REACT_APP_JWT_UPDATE_KEY, 10)*60000,
							key: randString
						});

     				await knex('users')
							.where('userId', jwtRes['userId'])
					  	.update({ 'key': key })

					  objectResult.token = token
					  
     			} 

     			await next()
     			objectResult.body = ctx.response.body
     			ctx.response.body = objectResult

     		} else {
     			await next()
   				ctx.response.body = {
   					body: false,
   					token: 'error'
   				}
     			return;
     		}
      } else {
      	await next()
    	}
//     	console.log('ticket=>' + ticket);

//     	const payload = ticket.getPayload();
//     	console.log(payload);

//     	if (payload['email'] === process.env.REACT_APP_ADMIN_EMAIL1 ||
// 		  	payload['email'] === process.env.REACT_APP_ADMIN_EMAIL2){	
// 		  	await next()
// 	  	}
			
//     } else {
//     	await next()
//   	}
  } catch (err) {
    console.log(err)
  }
})

// app.use(async (ctx, next) => {
//   try {
//     if (ctx.request.url !== '/api/user_check' && (ctx.request.url).includes('api/')){
//     	const token = ctx.request.header.authorization
    	
//     	const resultToken = token.substr(7);

//     	const jwtRes = await jwtDecode(resultToken);

//     	let dbJwt = await knex('users')
// 				.where('id', jwtRes.id)

// 			let result = await comparePassword((jwtRes.id).toString(), dbJwt[0]['jwt'])
			
// 			if (result){
// 				await next()
// 			} 
//     } else {
//     	await next()
//   	}
//   } catch (err) {
//     //console.log(err)
//   }
// })

app.use(bodyParser());
app.use(server());

router.get(
	'*',
  async (ctx, next) => {
	  ctx.type = 'html';
	  ctx.body = createReadStream('./build/index.html');
});

app.use(router.routes());
app.use(router.allowedMethods());

if (process.env.NODE_ENV === 'test') {
	module.exports = app.listen(process.env.PORT)
} else {
	app.listen(process.env.PORT || 3001)
}