const initialState = {
	tableSessions: false,
	sessionsChart: false,
	goalConversionRate: false,
	sessionsBySources: false,
	sessionsByUserType: false,
	tableCalls: false,

	ifOneIsTrue: false
}

export default function start(state=initialState, action){
  if (action.type === 'SELECTED_EL_CHANGE') {
  	const stateNow = { ...state };
		//objRezult[action.payload[0]] = action.payload[1];
		if (stateNow[action.payload]){
			stateNow[action.payload] = false
		} else {
			stateNow[action.payload] = true
		}

		stateNow['ifOneIsTrue'] = false
		Object.keys(stateNow).forEach(function(key) {
			if (stateNow[key] && key!=='ifOneIsTrue'){
				stateNow['ifOneIsTrue'] = true
				return
			}
		})
		return stateNow;
  }

  if (action.type === 'SELECTED_EL_CHANGE_ALL') {
  	let objRezult = { ...state };
  	let objParams = { ...action.payload };

  	objRezult = Object.assign(objRezult, objParams);

  	objRezult['ifOneIsTrue'] = false
		Object.keys(objRezult).forEach(function(key) {
			if (objRezult[key] && key!=='ifOneIsTrue'){
				objRezult['ifOneIsTrue'] = true
				return
			}
		})

  	return objRezult;
  }

  if (action.type === 'SELECTED_EL_RESET') {
  	return initialState;
  }

  return state;
}