const {today, todayMinus7, diffBetweenDate} = require('../components/data/date')

const initialState = {
	dateFrom: todayMinus7,
	dateTo: today,
	diffDate: diffBetweenDate(todayMinus7, today),
	filtersList: {},
	filtersListSelect: {},
	filterStringBackend:{
		sources: '',
		sessionsBySourcesF: '',
		others: '',
		userType: '',
		result: '',
		resultSessionsBySourcesF: ''
	},
	//sessionsBySourcesFilter: '',
	TableSessions: {
		Sessions: 0,
		SessionsDelta: 0,

		Goal: 0,
		GoalDelta: 0,

		Bounces: 0,
		BouncesDelta: 0,

		Avg: 0,
		AvgDelta: 0
	},
	sessionsChart: [],
	goalConversionRate: [],
	sessionsBySources: [],
	sessionsByUserType: [],
	loading: {
		tableSessions: false,
		sessionsChart: false,
		filters: false,
		goalConversionChart: false,
		sessionsBySources: false,
		sessionsByUserType: false,
		tableCalls: false
	}
};

export default function start(state=initialState, action){
  if (action.type === 'FILTER_CHANGE_ALL') {
  	let objRezult = { ...state };
  	let objParams = { ...action.payload };

  	objRezult = Object.assign(objRezult, objParams);
  	return objRezult;
  }

  if (action.type === 'FILTER_RESET') {
  	return initialState;
  }

  if (action.type === 'LOADING_CHANGE') {
		const objRezult = { ...state };
		objRezult['loading'][action.payload[0]] = action.payload[1];
		return objRezult;
  }

  return state;
}