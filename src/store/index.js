import { combineReducers } from 'redux';
//import { routerReducer } from 'react-router-redux'

//import start from './start';
import message from './message';
import storeFilter from './storeFilter';
import st_callTracking from './st_callTracking';
import st_selected_elements from './st_selected_elements';
import st_management from './st_management';
import st_googleSearch from "./st_googleSearch";
import st_cro from "./st_cro";


export default combineReducers({
  //routing: routerReducer,
  //start,
  message,
  storeFilter,
  st_callTracking,
  st_selected_elements,
  st_management,
  st_googleSearch,
  st_cro
});