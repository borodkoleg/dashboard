const initialState = {
	message: ''
};

export default function start(state=initialState, action){
  if (action.type === 'MESSAGE_CHANGE') {
  	return {
      ...state,
      message: action.payload
    }
  }

  return state;
}