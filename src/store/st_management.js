const initialState = {
  accountId: '',
  webPropertyId: '',
  profiles: [],
  currentProfile: '',
  defProfile: '',
  filtersAll: []
}

export default function start(state=initialState, action){
  if (action.type === 'MANAGEMENT_CHANGE_ALL') {
    let objRezult = { ...state };
    let objParams = { ...action.payload };

    objRezult = Object.assign(objRezult, objParams);
    return objRezult;
  }

  if (action.type === 'MANAGEMENT_RESET') {
    return initialState;
  }

  return state;
}