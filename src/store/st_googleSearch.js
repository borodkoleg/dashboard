const {today, todayMinus7} = require('../components/data/date')

const initialState = {
  currentSiteIndex: 0,
  currentSiteUrl: '',

  sites: [],
  queryImpressions: [],
  landingPages: [],

  dateFrom: todayMinus7,
  dateTo: today,

  loading: {
    sites: true,
    queryImpressions: true,
    landingPages: true
  }
}

export default function start(state=initialState, action){
  if (action.type === 'GOOGLE_SEARCH_CHANGE_ALL') {
    let objRezult = { ...state };
    let objParams = { ...action.payload };

    objRezult = Object.assign(objRezult, objParams);
    return objRezult;
  }

  if (action.type === 'GOOGLE_SEARCH_RESET') {
    return initialState;
  }

  if (action.type === 'GOOGLE_SEARCH_LOADING') {
    const objRezult = { ...state };
    objRezult['loading'][action.payload[0]] = action.payload[1];
    return objRezult;
  }

  return state;
}