const initialState = {
  newReturningUsers: [],
  landingPages: [],
  groupedChannels: [],
  viewIdCro: '',
  loading: {
    newReturningUsers: false,
    landingPages: false,
    groupedChannels: false
  }
};

export default function start(state=initialState, action){
  if (action.type === 'CRO_CHANGE_ALL') {
    let objRezult = { ...state };
    let objParams = { ...action.payload };

    objRezult = Object.assign(objRezult, objParams);
    return objRezult;
  }

  if (action.type === 'CRO_LOADING') {
    const objRezult = { ...state };
    objRezult['loading'][action.payload[0]] = action.payload[1];
    return objRezult;
  }

  if (action.type === 'CRO_RESET') {
    return initialState;
  }

  if (action.type === 'CRO_LOADING_START') {
    let objRezult = { ...state };

    objRezult.loading = {
      newReturningUsers: true,
      landingPages: true,
      groupedChannels: true
    }
    return objRezult;
  }

  return state;
}