const initialState = {
	test: ''
};

export default function start(state=initialState, action){
  if (action.type === 'TEST_CHANGE') {
  	return {
      ...state,
      test: action.payload
    }
  }

  return state;
}