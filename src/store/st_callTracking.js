const initialState = {
	calls: {
		rows: [],
		tracking_number: ''
	}
}

export default function start(state=initialState, action){
  if (action.type === 'CALL_CHANGE_ALL') {
  	let objRezult = { ...state };
  	let objParams = { ...action.payload };

  	objRezult = Object.assign(objRezult, objParams);
  	return objRezult;
  }

  if (action.type === 'CALL_CHANGE_CALLS') {
  	const objRezult = { ...state };
		objRezult['calls'][action.payload[0]] = action.payload[1];
		return objRezult;
  }

  if (action.type === 'CALL_RESET') {
  	return initialState;
  }

  // if (action.type === 'LOADING_CHANGE') {
		// const objRezult = { ...state };
		// objRezult['loading'][action.payload[0]] = action.payload[1];
		// return objRezult;
  // }

  return state;
}