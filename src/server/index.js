const combineRouters = require('koa-combine-routers');

const users = require('./users')
const monitors = require('./monitors')
const images = require('./images')
const googleReporting = require('./googleReporting')
const sr_callTracking = require('./sr_callTracking')
const sr_management_API = require('./sr_management_API')
const sr_googleSearchC = require('./sr_googleSearchC')
const sr_cro = require('./sr_cro')

const router = combineRouters(
	users,
	monitors,
	images,
	googleReporting,
	sr_callTracking,
	sr_management_API,
	sr_googleSearchC,
	sr_cro
)

module.exports = router;