require('dotenv-flow').config({silent: true});
const Router = require('koa-router');
const router = new Router();
const knex = require('../libs/knexConfig');
const multer = require('@koa/multer');
const Path = require('path');
const fs = require('fs');
const {isEngString} = require('../components/data/validation');

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads')
  },
  filename: function (req, file, cb) {
  	const title = req.body.title;
		
    cb(null, `${title}${Path.extname(file.originalname)}`)
  }
})

const upload = multer({ 
	storage: storage,
	limits: {fileSize: 5000000},
	fileFilter: function(req, file, cb){
		checkFile(file, cb);
    //checkFileType(file, cb);
  }
}).fields([{
  name: 'file',
  maxCount: 1
}])

function checkFile(file, cb){

	if (!checkFileType(file)){
		cb('Error: Images Only!');
	}

	return arrayFiles().then((res) => {
		if (res && res.length > 10){
			return cb('Error: Images limit!');
		} else {
			return cb(null,true);
		}
	})
}

function checkFileType(file){
  const filetypes = /jpeg|jpg|png|gif|bmp/
  const extname = filetypes.test(Path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  if(mimetype && extname){
  	return true;
      //return cb(null,true);
  } else {
  	return false;
      //cb('Error: Images Only!');
  }
}

function arrayFiles(){
	return new Promise(function(resolve) {
		let arrayFiles = [];
		let counter = 0;
		fs.readdir('./public/uploads', (err, files) => {
		  files.forEach(file => {
		    arrayFiles.push({file: file, id: counter, title: ''});
		    counter += 1;
		  });
		  resolve(arrayFiles);
		});
	});
}
router.post('/api/get-images', async (ctx, next) => {
	try {
		
		let result = await knex.raw(`
			SELECT 
			*
			FROM images
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/save-image', async (ctx, next) => {
	try {
		await upload(ctx, next)
		//const id = ctx.request.body.id; //phone id
		//const objFile = ctx.request.body.objFile;

		//objFileParse = JSON.parse(objFile);

		////console.log(objFileParse);
		////console.log('ctx.request.files', ctx.request.files);
    ////console.log('ctx.files', ctx.files['file']);

    // if (ctx.files['file'] && ctx.files['file'].length > 0){

    // }

		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (
			err === 'Error: Images Only!' ||
			err === 'Error: Images limit!'
			) {
			ctx.response.status = 403;
			ctx.body = err;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/get-images-in-directory', async (ctx, next) => {
	try {
		
		let result = await arrayFiles();

		ctx.response.status = 200;
		ctx.body = result;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/remove-image', async (ctx, next) => {
	try {
		const file = ctx.request.body.file;

		if (!isEngString(file)){
			ctx.response.status = 400;
			ctx.body = false;
		}

		let promise = await new Promise(function(resolve) {
			fs.unlink('./public/uploads/' + file, (err) => {
			  if (err) {
			  	console.log(err);
			    resolve(false);
			  }
			  //file removed
			  resolve(true);
			})
		});

		if (promise){
			ctx.response.status = 200;
			ctx.body = true;
		} else {
			ctx.response.status = 400;
			ctx.body = false;
		}
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;