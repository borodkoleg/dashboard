const request = require('supertest');
const server = require('../../../index');
const knex = require('../../libs/knexConfig');

beforeAll(async done => {
	await knex('accounts_calls').truncate()
	await knex('accounts_settings').truncate()

	token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxMTgwMDk1NjcwMTk1NDYzMzc5OTciLCJ0aW1lIjoxNTc4MTgxNzE2MzcyLCJrZXkiOiI0WW5jTWpyUUxLT0pudWg2TkhkZyIsImlhdCI6MTU3ODE1NzcxNiwiZXhwIjoxNTc4MTc1NzE2fQ.bk1D3rvxdQbNnWcXMNaU4K4j6eYiuXGVywOIpH30qHU';

	ct_accaunt = 46131

  done();
});

afterAll(() => {
  server.close();
});

describe('ctm', () => {
	test('[0]', async () => {
		const startDate = "2019-11-10"
		const endDate = "2019-11-14"
		const stD = new Date(startDate)
		const endD = new Date(endDate)

		const response = await request(server)
      .post('/api/ctm-list-of-calls-duration')
      .send({
        startDate: startDate,
        endDate: endDate
  		})
  		.set('Authorization', token)

  	//save to db is async, and I need to do pause for correct test
  	await new Promise(resolve => setTimeout(resolve, 1000));	

	  const db_calls = await knex('accounts_calls')
	  	.where('account_id', ct_accaunt)
	  const db_settings = await knex('accounts_settings')
	  	.where('account_id', ct_accaunt)

	  let testDb = 0
	  if (new Date(db_calls[0]['date']) >= stD){
	  	testDb += 1
	  }

	  if (new Date(db_calls[db_calls.length - 1]['date']) <= endD){
	  	testDb += 1
	  }

	  expect(testDb).toEqual(2)

	  expect(db_settings[0]['min_date']).toEqual("2019-11-10")
	  expect(db_settings[0]['max_date']).toEqual("2019-11-14")

	 	expect(response.type).toEqual('application/json')
	  expect(response.status).toEqual(200);
	})

	test('[1]', async () => {
		const startDate = "2019-11-10"
		const endDate = "2019-11-12"
		const stD = new Date(startDate)
		const endD = new Date(endDate)

		//calls length don't change after and before request
		//settings db don't change
		const db_calls = await knex('accounts_calls')
	  	.where('account_id', ct_accaunt)
	  const db_settings = await knex('accounts_settings')
	  	.where('account_id', ct_accaunt)


		const response = await request(server)
      .post('/api/ctm-list-of-calls-duration')
      .send({
        startDate: startDate,
        endDate: endDate
  		})
  		.set('Authorization', token)

  	const db_calls_after = await knex('accounts_calls')
	  	.where('account_id', ct_accaunt)
	  const db_settings_after = await knex('accounts_settings')
	  	.where('account_id', ct_accaunt)

  	expect(db_settings[0]['min_date']).toEqual(db_settings_after[0]['min_date'])
  	expect(db_settings[0]['max_date']).toEqual(db_settings_after[0]['max_date'])

  	expect(db_calls.length).not.toBe(0)
  	expect(db_calls.length).toEqual(db_calls_after.length)

  	expect(response.type).toEqual('application/json')
	  expect(response.status).toEqual(200);
	})

	test('[2]', async () => {
		const startDate = "2019-11-07"
		const endDate = "2019-11-11"
		const stD = new Date(startDate)
		const endD = new Date(endDate)

		const response = await request(server)
      .post('/api/ctm-list-of-calls-duration')
      .send({
        startDate: startDate,
        endDate: endDate
  		})
  		.set('Authorization', token)

  		await new Promise(resolve => setTimeout(resolve, 1000));

  		const db_calls = await knex('accounts_calls')
	  		.where('account_id', ct_accaunt)
	  		.orderBy('date')
	  	const db_settings = await knex('accounts_settings')
	  		.where('account_id', ct_accaunt)

	  	let testDb = 0
		  if (new Date(db_calls[0]['date']) < new Date('2019-11-10')){
		  	testDb += 1
		  }

		  expect(testDb).toEqual(1)

		  expect(db_settings[0]['min_date']).toEqual("2019-11-07")
	  	expect(db_settings[0]['max_date']).toEqual("2019-11-14")

		 	expect(response.type).toEqual('application/json')
		  expect(response.status).toEqual(200);
	})

	test('[3]', async () => {
		const startDate = "2019-11-12"
		const endDate = "2019-11-21"
		const stD = new Date(startDate)
		const endD = new Date(endDate)

		const response = await request(server)
      .post('/api/ctm-list-of-calls-duration')
      .send({
        startDate: startDate,
        endDate: endDate
  		})
  		.set('Authorization', token)

  		await new Promise(resolve => setTimeout(resolve, 1000));

  		const db_calls = await knex('accounts_calls')
	  		.where('account_id', ct_accaunt)
	  		.orderBy('date')
	  	const db_settings = await knex('accounts_settings')
	  		.where('account_id', ct_accaunt)

	  	let testDb = 0
		  if (new Date(db_calls[db_calls.length - 1]['date']) > new Date('2019-11-14')){
		  	testDb += 1
		  }

		  expect(testDb).toEqual(1)

		  expect(db_settings[0]['min_date']).toEqual("2019-11-07")
	  	expect(db_settings[0]['max_date']).toEqual("2019-11-21")

		 	expect(response.type).toEqual('application/json')
		  expect(response.status).toEqual(200);
	})

	test('[4]', async () => {
		const startDate = "2019-11-05"
		const endDate = "2019-11-25"
		const stD = new Date(startDate)
		const endD = new Date(endDate)

		const response = await request(server)
      .post('/api/ctm-list-of-calls-duration')
      .send({
        startDate: startDate,
        endDate: endDate
  		})
  		.set('Authorization', token)

  	await new Promise(resolve => setTimeout(resolve, 1000));

  	const db_calls = await knex('accounts_calls')
  		.where('account_id', ct_accaunt)
  		.orderBy('date')
  	const db_settings = await knex('accounts_settings')
  		.where('account_id', ct_accaunt)

  	let testDb = 0
	  if (new Date(db_calls[db_calls.length - 1]['date']) > new Date('2019-11-21')){
	  	testDb += 1
	  }

	  if (new Date(db_calls[0]['date']) < new Date('2019-11-07')){
	  	testDb += 1
	  }

	  expect(testDb).toEqual(2)

	  expect(db_settings[0]['min_date']).toEqual("2019-11-05")
  	expect(db_settings[0]['max_date']).toEqual("2019-11-25")

	 	expect(response.type).toEqual('application/json')
	  expect(response.status).toEqual(200);
	})

	test('[5]', async () => {
		const startDate = "2019-11-01"
		const endDate = "2019-11-04"
		const stD = new Date(startDate)
		const endD = new Date(endDate)

		const response = await request(server)
      .post('/api/ctm-list-of-calls-duration')
      .send({
        startDate: startDate,
        endDate: endDate
  		})
  		.set('Authorization', token)

  	await new Promise(resolve => setTimeout(resolve, 1000));

  	const db_calls = await knex('accounts_calls')
  		.where('account_id', ct_accaunt)
  		.orderBy('date')
  	const db_settings = await knex('accounts_settings')
  		.where('account_id', ct_accaunt)

  	let testDb = 0
	  if (new Date(db_calls[0]['date']) < new Date('2019-11-05')){
	  	testDb += 1
	  }

	   if (new Date(db_calls[db_calls.length - 1]['date']) < new Date('2019-11-25')){
	  	testDb += 1
	  }

	  expect(testDb).toEqual(2)

	  expect(db_settings[0]['min_date']).toEqual("2019-11-01")
  	expect(db_settings[0]['max_date']).toEqual("2019-11-25")

	 	expect(response.type).toEqual('application/json')
	  expect(response.status).toEqual(200);
	})

	test('[6]', async () => {
		const startDate = "2019-11-26"
		const endDate = "2019-11-28"
		const stD = new Date(startDate)
		const endD = new Date(endDate)

		const response = await request(server)
      .post('/api/ctm-list-of-calls-duration')
      .send({
        startDate: startDate,
        endDate: endDate
  		})
  		.set('Authorization', token)

  	await new Promise(resolve => setTimeout(resolve, 1000));

  	//===============
  	const db_calls = await knex('accounts_calls')
  		.where('account_id', ct_accaunt)
  		.orderBy('date')
  	const db_settings = await knex('accounts_settings')
  		.where('account_id', ct_accaunt)

  	let testDb = 0
	   if (new Date(db_calls[db_calls.length - 1]['date']) > new Date('2019-11-25')){
	  	testDb += 1
	  }

	  expect(testDb).toEqual(1)

	  expect(db_settings[0]['min_date']).toEqual("2019-11-01")
  	expect(db_settings[0]['max_date']).toEqual("2019-11-28")

	 	expect(response.type).toEqual('application/json')
	  expect(response.status).toEqual(200);
	})

})