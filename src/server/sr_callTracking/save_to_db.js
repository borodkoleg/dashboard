const {asyncArrayEach, asyncObjectEach} = require('../../libs/asyncIterators.js')
const {formatDayFromFormat, formatDay, dayMinusDays} = require('../../components/data/date')
const knex = require('../../libs/knexConfig');

function saveToDb(result, startDate, endDate, minToDb, maxSelectToDb){
	let objForSave = {}
	let maxToDb

	//если maxSelectToDb >= new Date() тогда выводить звонки за время 
	//с startDate до вчера.. минус - сегодня не включается в ответ
	//плюс - лишний раз не дергается апиха

	if (maxSelectToDb >= formatDay(new Date())){
		console.log('set max date')
		maxToDb = formatDay(dayMinusDays(new Date(), 1))
	} else {
		maxToDb = maxSelectToDb
	}

	//console.log('maxToDb', maxToDb)
	//console.log('minToDb', minToDb)
	//console.log('result', result);

	asyncArrayEach(result, (item, i) => {
		if (parseInt(item.duration, 10) > 119){
		  const date = formatDayFromFormat(item.called_at, "YYYY-MM-DD", "YYYY-MM-DD")
			if (!objForSave[date]){
				objForSave[date] = []
			}
			objForSave[date].push(item)
		}

		if (i === result.length - 1){
			//save to db accounts_settings
			let objSetting = {}
			if (minToDb && maxToDb){
				objSetting = {
					account_id: process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT,
					min_date: minToDb,
					max_date: maxToDb
				}
			}

			if (minToDb && !maxToDb){
				objSetting = {
					account_id: process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT,
					min_date: minToDb
				}
			}

			if (!minToDb && maxToDb){
				objSetting = {
					account_id: process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT,
					max_date: maxToDb
				}
			}

			if (!minToDb && !maxToDb){
				return
			}

			knex('accounts_settings')
				.where('account_id', process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT).then((res) =>
				{
					if (res.length > 0){
						knex('accounts_settings')
							.where({account_id: res[0]['account_id']})
							.update(objSetting)
							.then()
					} else {
						knex('accounts_settings').insert(
							objSetting
						).then()
					}
				}
			)

			let arrayKnex = []
			let	lengthObjSave = Object.keys(objForSave).length

			asyncObjectEach(objForSave, (item, i) => {
				arrayKnex.push({
					account_id: process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT,
					date: item,
					calls: JSON.stringify(objForSave[item])
				})

				if (i === lengthObjSave - 1){
					//save to db accounts_calls
					knex('accounts_calls').insert(arrayKnex).then()
				}
			})
		}
	});
}

module.exports.saveToDb = saveToDb