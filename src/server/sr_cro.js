require('dotenv-flow').config({silent: true});
const Router = require('koa-router');
const router = new Router();
const {formatDay, dayMinusDays, diffBetweenDate, DeltaDays} = require('../components/data/date');

const {google} = require('googleapis');
const service_account = require('../settings/keys/googleAnalyticsKey.json');
const reporting = google.analyticsreporting('v4');

const scopes = [
  'https://www.googleapis.com/auth/analytics.readonly',
  'https://www.googleapis.com/auth/analytics.edit'
];

let jwt = new google.auth.JWT(
  service_account.client_email,
  null,
  service_account.private_key,
  scopes
);

const analytics = google.analyticsreporting({
  version: "v4",
  auth: jwt
});

//=======

router.post('/api/cro-get-landing-pages', async (ctx, next) => {
  try {
    let dateFrom = ctx.request.body.dateFrom;
    let dateTo = ctx.request.body.dateTo;
    //let filters = ctx.request.body.filters;
    const view_id = ctx.request.body.view_id;

    dateFrom = formatDay(dateFrom);
    dateTo = formatDay(dateTo);

    const res = await analytics.reports.batchGet({
      requestBody: {
        reportRequests: [
          {
            viewId: view_id,
            "dateRanges": [
              {"startDate": dateFrom, "endDate": dateTo}
            ],
            dimensions: [
              {"name": "ga:landingPagePath"}
            ],
            metrics: [
              {expression: 'ga:entrances'},
              {expression: 'ga:bounces'}
            ],
            orderBys: [
              {
                fieldName: 'ga:entrances',
                sortOrder: 'DESCENDING'
              }
            ],
            'pageSize': 10
            //filtersExpression: filters
          }
        ],
      },
    });

    ctx.response.status = 200;
    ctx.body = res.data;
  } catch(err){
    console.log(err);
    ctx.response.status = 400;
    ctx.body = false;
  }
});

router.post('/api/cro-get-channels-grouped', async (ctx, next) => {
  try {
    let dateFrom = ctx.request.body.dateFrom;
    let dateTo = ctx.request.body.dateTo;
    //let filters = ctx.request.body.filters;
    const view_id = ctx.request.body.view_id;

    dateFrom = formatDay(dateFrom);
    dateTo = formatDay(dateTo);

    const res = await analytics.reports.batchGet({
      requestBody: {
        reportRequests: [
          {
            viewId: view_id,
            "dateRanges": [
              {"startDate": dateFrom, "endDate": dateTo}
            ],
            dimensions: [
              {"name": "ga:channelGrouping"}
            ],
            metrics: [
              {expression: 'ga:entrances'},
              {expression: 'ga:goalConversionRateAll'}
            ],
            orderBys: [
              {
                fieldName: 'ga:entrances',
                sortOrder: 'DESCENDING'
              }
            ]
            //'pageSize': 10
            //filtersExpression: filters
          }
        ],
      },
    });

    //console.log(JSON.stringify(res.data, null, 2));

    ctx.response.status = 200;
    ctx.body = res.data;
  } catch(err){
    console.log(err);
    ctx.response.status = 400;
    ctx.body = false;
  }
});

module.exports = router;