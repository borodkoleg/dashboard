require('dotenv-flow').config({silent: true});
const Router = require('koa-router');
const router = new Router();
const knex = require('../libs/knexConfig');

router.post('/api/get-monitors', async (ctx, next) => {
	try {
		
		let result = await knex.raw(`
			SELECT 
			mon.*
			FROM monitors mon
		`);

		ctx.response.status = 200;
		ctx.body = result[0];
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/add-monitor', async (ctx, next) => {
	try {
		const params = ctx.request.body.data;

		let result = await knex('monitors').insert(params);
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.body = false;
			ctx.response.status = 200;
		} else {
			console.log(err);
			ctx.response.status = 400;
		}
	}
});

router.post('/api/delete-monitor', async (ctx, next) => {
	try {
		const id = ctx.request.body.id;

		await knex('monitors')
			.where('id', id)
			.del();
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
			console.log(err);
			ctx.response.status = 400;
	}
});

router.post('/api/edit-monitor', async (ctx, next) => {
	try {
		const data = ctx.request.body.data;
		const id = data['id'];
		if (data.hasOwnProperty('created_at')){
			delete data['created_at'];
		}

		await knex('monitors')
			.where('id', id)
			.update(data)
		
		ctx.response.status = 200;
		ctx.body = true;
	} catch(err){
		if (err.code === 'ER_DUP_ENTRY'){
			ctx.response.status = 200;
			ctx.body = false;
			return;
		}
		console.log(err);
		ctx.response.status = 400;
	}
});

module.exports = router;