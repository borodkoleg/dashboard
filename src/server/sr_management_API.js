require('dotenv-flow').config({silent: true});
const Router = require('koa-router');
const router = new Router();
const knex = require('../libs/knexConfig');
const Axios = require('axios');
const {formatDay, dayMinusDays, diffBetweenDate, DeltaDays} = require('../components/data/date')

const {google} = require('googleapis');
const service_account = require('../settings/keys/googleAnalyticsKey.json');
//const reporting = google.analyticsreporting('v4');

const scopes = [
	'https://www.googleapis.com/auth/analytics.readonly',
	'https://www.googleapis.com/auth/analytics.edit'
	];

let jwt = new google.auth.JWT(
    service_account.client_email,
    null,
    service_account.private_key,
    scopes
);

const analytics = google.analytics({
  version: "v3",
  auth: jwt
});

//=======
async function getProfiles(){
	const res = await analytics.management.accountSummaries.list()

	if (res &&
		res.data &&
		res.data.items &&
		res.data.items.length > 0){
		return {
			accountId: res.data.items[0].id,
			webPropertyId: res.data.items[0].webProperties[0].id,
			profiles: res.data.items[0].webProperties[0].profiles //arr
		}
	} else {
		return false
	}
}

async function getGoals(accountId, webPropertyId, profileId){
	const goalsList = await analytics.management.goals.list({
		'accountId': accountId,
		'webPropertyId': webPropertyId,
		'profileId': profileId
	})

	if (goalsList &&
		goalsList.data &&
		goalsList.data.items){
		return {
			profileId: profileId,
			goals: goalsList.data.items
		}
	} else {
		return {
			profileId: profileId,
			goals: []
		}
	}
}

async function getProfileFilterLinks(accountId, webPropertyId, profileId){
	const res = await analytics.management.profileFilterLinks.list({
		'accountId': accountId,
		'webPropertyId': webPropertyId,
		'profileId': profileId
	})

	if (res &&
		res.data &&
		res.data.items){
		return {
			profileId: profileId,
			filters: res.data.items
		}
	} else {
		return {
			profileId: profileId,
			filters: []
		}
	}
}

async function getFilters(accountId){
	const filtersList = await analytics.management.filters.list({
		'accountId': accountId
	});

	if (filtersList &&
		filtersList.data &&
		filtersList.data.items){
		return filtersList.data.items
	} else {
		return []
	}
}

router.post('/api/google-management', async (ctx, next) => {
	try {
		// let dateFrom = ctx.request.body.dateFrom;

		const ac = await getProfiles();

		const accountId = ac.accountId; //account id
		const webPropertyId = ac.webPropertyId;	//webProperty
		const profiles = ac.profiles; //profiles arr

		let prArr = []

		profiles.forEach((ob) => {
			//console.log(JSON.stringify(ob));
			prArr.push(getGoals(accountId, webPropertyId, ob.id));
			prArr.push(getProfileFilterLinks(accountId, webPropertyId, ob.id));
		})

		let resObj = {}
		await Promise.all(prArr).then(function(arr) {
			arr.forEach((ob) => {
				if (!resObj[ob.profileId]){
					resObj[ob.profileId] = {}
				}
				if (ob.goals) {
					resObj[ob.profileId]['goals'] = ob.goals
				}

				if (ob.filters) {
					resObj[ob.profileId]['filters'] = ob.filters
				}
			})
		});

		resObj['filtersAll'] = await getFilters(accountId)
		resObj['profiles'] = profiles
		resObj['accountId'] = accountId
		resObj['webPropertyId'] = webPropertyId
		resObj['currentProfile'] = profiles[0]['id']

		//console.log('res', JSON.stringify(resObj, null, 2))

		ctx.response.status = 200;
		ctx.body = resObj;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;
