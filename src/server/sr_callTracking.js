require('dotenv-flow').config({silent: true});
const Router = require('koa-router');
const router = new Router();
const knex = require('../libs/knexConfig');
const Axios = require('axios');
const {formatDay, dayMinusDays, diffBetweenDate, DeltaDays, formatDayFromFormat} = require('../components/data/date')
const timeout = ms => new Promise(res => setTimeout(res, ms))
const {asyncArrayEach, asyncObjectEach} = require('../libs/asyncIterators.js')
const {saveToDb} = require('./sr_callTracking/save_to_db')
const {logger} = require('../libs/log4js.js');

router.post('/api/ctm-list-of-calls', async (ctx, next) => {
	try {
		let page = ctx.request.body.page;
		let startDate = ctx.request.body.startDate;
		let endDate = ctx.request.body.endDate;
		let pageSize = ctx.request.body.pageSize;

		startDate = formatDay(startDate, 'YYYY-MM-DD');
		endDate = formatDay(endDate, 'YYYY-MM-DD');

		const result = await Axios({
		  method: 'get',
		  url: `https://api.calltrackingmetrics.com/api/v1/accounts/${process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT}/calls`,
		  headers: {
      		Authorization: `Basic ${process.env.REACT_APP_CALLTRACKINGMETRICS_TOKEN}`
      },
      params: {
      	page: page,
      	start_date: startDate,
      	end_date: endDate,
      	with_time: 1,
      	per_page: 150,
      	limit_fields: ['id', 'duration', 'source', 'called_at']
      }
		})
    .then((result) => {
    	return result
    })
    .catch(err => {
    	console.log(err)
      return false
    })

    if (!result) {
    	ctx.response.status = 400;
    } else {
    	//console.log(result);
    	ctx.body = result['data'];
    	ctx.response.status = 200;
    }
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

//=====================================

async function getCallsFromAPI(page, startDate, endDate, rezArray){
	return await Axios({
	  method: 'get',
	  url: `https://api.calltrackingmetrics.com/api/v1/accounts/${process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT}/calls`,
	  headers: {
    		Authorization: `Basic ${process.env.REACT_APP_CALLTRACKINGMETRICS_TOKEN}`
    },
    params: {
    	page: page,
    	start_date: startDate,
    	end_date: endDate,
    	with_time: 1,
    	per_page: 150,
    	limit_fields: [
    		'id',
    		'duration', 
    		'source', 
    		'called_at',
    		'tracking_number'
    	]
    }
	})
  .then((result) => {
  	//rezArray.push(page)
  	//console.log(rezArray.length);
  	rezArray = rezArray.concat(result.data.calls)

  	if (result.hasOwnProperty('data') &&
  			result.data.hasOwnProperty('next_page')){
  		//console.log(result.data.calls)
  		//rezArray.concat(result.data.calls);
  		//timeout(2000)
  		
  		return getCallsFromAPI((parseInt(page,10) + 1).toString(), startDate, endDate, rezArray).then()
  		
  	} else {
  		//console.log('rezArray=>',rezArray)
  		return rezArray
  	}
  })
  .catch(err => {
  	console.log(err)
    return false
  })
}


router.post('/api/ctm-list-of-calls-duration', async (ctx, next) => {
	try {
		let startDate = ctx.request.body.startDate;
		let endDate = ctx.request.body.endDate;

		startDate = formatDay(startDate, 'YYYY-MM-DD');
		endDate = formatDay(endDate, 'YYYY-MM-DD');

		if (startDate >= formatDay(new Date())){
			startDate = formatDay(dayMinusDays(new Date(), 1))
		}

		if (endDate >= formatDay(new Date())){
			endDate = formatDay(dayMinusDays(new Date(), 1))
		}

		//console.log('startDate', startDate)
		//console.log('endDate', endDate)
		
		//logger.info(`endDate calls server - ${endDate}`);

		//1 вытягиваю значения max_date и min_date и 
		//сравниваю с startDate, на основе этого строю запрос

		let settings = await knex('accounts_settings')
	 		  .where('account_id', process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT)

	  let result = []

	  //await knex('accounts_calls').del()		//temp
	  //await knex('accounts_settings').del()
	  //console.log('==================')

	 	if (settings.length === 0) {
	 		console.log('(0)')
	 		//значит данных в базе нет, подтягиваем их и записываем,
	 		//запрос не меняется никак
	 		//ответ сразу отображается юзеру, а затем записывается в 
	 		//бд в фоновом режиме
	 		result = await getCallsFromAPI(1, startDate, endDate, [])

	 		saveToDb(result, startDate, endDate, startDate, endDate)

	 		if (!result) {
    		ctx.response.status = 400;
	    } else {
	    	ctx.body = result;
	    	ctx.response.status = 200;
	    }
	    return
	 	} 

	 	
 		//если startDate и endDate входят в диапазон с базы,
 		//тогда просто выводим все с базы
 		const dbMin = new Date(settings[0]['min_date'])
 		const dbMax = new Date(settings[0]['max_date'])

 		const dbMinMOne = dayMinusDays(dbMin, 1)
 		const dbMaxPOne = dayMinusDays(dbMax, -1)

 		const stD = new Date(startDate)
		const endD = new Date(endDate)


		//logger.info(`dbMin - ${dbMin}`);
		//logger.info(`dbMax - ${dbMax}`);

		//logger.info(`stD - ${stD}`);
		//logger.info(`endD - ${endD}`);

		//[1] когда диапазон полностью входит в диапазон в бд,
		//данные берем с бд, бд не меняем
 		if (stD >= dbMin && endD <= dbMax){
 			console.log('(1)')
 			let resultArray = await knex('accounts_calls')
 				.whereBetween('date', [stD, endD])
 				.orderBy('date', 'desc')

 			resultArray.forEach((obj) => {
 				result = result.concat(JSON.parse(obj.calls))
 			})
 		}

 		//[2]
 		if (stD < dbMin && endD >= dbMin && endD <= dbMax){
 			console.log('(2)')
 			//делаем запрос от stD до dbMin к API
 			//тяним с базы от dbMin до endD 
 			//плюсуем выдаем 
 			//меняем базу
 			const resultApi = await getCallsFromAPI(1, stD, dbMinMOne, [])
 			result = [...resultApi]

 			resultArray = await knex('accounts_calls')
	 		  .where({
	 		  	account_id: process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT})
	 		  .andWhere(function() {
	 		  	this.whereBetween('date', [dbMin, endD])
	 		  })
	 		  .orderBy('date', 'desc')	  

	 		let arrFromDb = []
	 		resultArray.forEach((obj) => {
	 			arrFromDb = arrFromDb.concat(JSON.parse(obj.calls))
	 		})

	 		result = arrFromDb.concat(result)

	 		saveToDb(resultApi, stD, dbMinMOne, stD, false)
 		}

 		//[3]
 		if (stD >= dbMin && stD <= dbMax && endD > dbMax){
 			console.log('(3)')
 			const resultApi = await getCallsFromAPI(1, dbMaxPOne, endD, [])
 			result = [...resultApi]

 			resultArray = await knex('accounts_calls')
	 		  .where({
	 		  	account_id: process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT})
	 		  .andWhere(function() {
	 		  	this.whereBetween('date', [stD, dbMax])
	 		  })
	 		  .orderBy('date', 'desc')

	 		let arrFromDb = []
	 		resultArray.forEach((obj) => {
	 			arrFromDb = arrFromDb.concat(JSON.parse(obj.calls))
	 		})

	 		result = result.concat(arrFromDb) 

	 		saveToDb(resultApi, dbMaxPOne, endD, false, endD)
 		}

 		//[4]
 		if (stD < dbMin && endD > dbMax){
 			console.log('(4)')
 			const resultApi = await getCallsFromAPI(1, stD, dbMinMOne, [])
 			const resultApi2 = await getCallsFromAPI(1, dbMaxPOne, endD, [])
			result = [...resultApi]

			resultArray = await knex('accounts_calls')
	 		  .where({
	 		  	account_id: process.env.REACT_APP_CALLTRACKINGMETRICS_ACCOUNT})
	 		  .andWhere(function() {
	 		  	this.whereBetween('date', [dbMin, dbMax])
	 		  })
	 		  .orderBy('date', 'desc')

	 		let arrFromDb = []
	 		resultArray.forEach((obj) => {
	 			arrFromDb = arrFromDb.concat(JSON.parse(obj.calls))
	 		})

	 		result = arrFromDb.concat(result)
	 		result = resultApi2.concat(result)

	 		saveToDb(resultApi, stD, dbMinMOne, stD, false)
	 		saveToDb(resultApi2, dbMaxPOne, endD, false, endD)
 		}
		
		//[5]
 		if (stD < dbMin && endD < dbMin){
 			console.log('(5)')
 			const resultApi = await getCallsFromAPI(1, stD, dbMinMOne, [])

 			result = []
 			resultApi.forEach((obj) => {
 				if (new Date(obj.called_at) <= new Date(endD)){
 					result.push(obj)
 				}
	 		})

			saveToDb(resultApi, stD, dbMinMOne, stD, false)
 		}

 		//[6]
 		if (stD > dbMax && endD > dbMax){
 			console.log('(6)')
 			const resultApi = await getCallsFromAPI(1, dbMaxPOne, endD, [])
			result = []
 			resultApi.forEach((obj) => {
 				if (new Date(obj.called_at) >= new Date(stD)){
 					result.push(obj)
 				}
	 		})

			saveToDb(resultApi, dbMaxPOne, endD, false, endD)
 		}

    if (!result) {
    	ctx.response.status = 400;
    } else {
    	ctx.body = result;
    	ctx.response.status = 200;
    }

	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;