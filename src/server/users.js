require('dotenv-flow').config({silent: true});
const Router = require('koa-router');
const router = new Router();
const knex = require('../libs/knexConfig');
const {comparePassword, cryptPassword} = require('../libs/bcrypt');
const {jwtEncode, jwtDecode} = require('../libs/jwt');
const jwt = require('jsonwebtoken');
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.REACT_APP_GOOGLE_LOGIN);
const {randomString} = require('../components/data/random');


router.post('/api/user_check', async (ctx) => {
	try {
		const params = ctx.request.body;
		
		let hash = await knex('users').where({
		  login: params.login
		}).select('password', 'id');

		if (hash.length === 0){
			ctx.response.status = 401;
			ctx.response.body = false;
			return;
		}

		let correctPassword = await comparePassword(params.password, hash[0].password);

		if (correctPassword){
			let token = await jwtEncode({
				id: hash[0].id
			});

			let jwtKey = await cryptPassword((hash[0].id).toString());

			await knex('users')
				.update('jwt', jwtKey)
				.where('id', hash[0].id)
			
			ctx.response.status = 200;
			ctx.response.body = token;
		} else {
			ctx.response.status = 401;
			ctx.response.body = false;
		}
		
		
	} catch(err){
		console.log(err);
		ctx.response.status = 401;
		ctx.body = false;
	}
});

router.post('/api/user_logout', async (ctx) => {

	const token = ctx.request.header.authorization
	const resultToken = token.substr(7);

	const jwtRes = await jwtDecode(resultToken);

	let dbJwt = await knex('users')
		.update('jwt', null)
		.where('id', jwtRes.id)

	ctx.response.body = true;
	ctx.response.status = 200;
});

router.post('/api/user-check-google', async (ctx) => {
	try {
		const tokenId = ctx.request.body.obj.tokenId;
		const googleId = ctx.request.body.obj.googleId;

		const ticket = await client.verifyIdToken({
	    idToken: tokenId,
	    audience: process.env.REACT_APP_GOOGLE_LOGIN
		});
		
	  const payload = ticket.getPayload();
	  const envEmails = process.env.REACT_APP_ADMIN_EMAILS.split(",");
	  
	  for (i = 0; i < envEmails.length; i++) {
	  	if (payload['email'] === envEmails[i]){

	  		const dateNow = new Date();
	  		const randString = randomString(20);
				const key = await cryptPassword(randString);

				const token = await jwtEncode({
					userId: googleId,
					time: dateNow.getTime() + parseInt(process.env.REACT_APP_JWT_UPDATE_KEY, 10)*60000,
					key: randString
				});

	  		let array = await knex.select('userId')
					.from('users')
					.where('userId', googleId)

				if (array.length > 0){
					//refresh
					await knex('users')
						.where('userId', googleId)
					  .update({ 'key': key })
				} else {
					//add user
					await knex('users').insert(
						{
							userId: googleId,
							key: key
						}
					)
				}

	  	ctx.response.status = 200;
			ctx.response.body = token;
			return;
	  	}
		}
		
	} catch(err){
		console.log(err);
		ctx.response.status = 404;
		ctx.body = false;
	}
});

router.post('/api/user-google-logout', async (ctx) => {

	const token = ctx.request.header.authorization;
	const resultToken = token.substr(7);

	const randString = randomString(20);
	const key = await cryptPassword(randString);

	const jwtRes = await jwtDecode(resultToken);
	
	await knex('users')
		.update('key', key)
	 	.where('userId', jwtRes['userId'])

	ctx.response.body = true;
	ctx.response.status = 200;
});

router.post('/api/user_get_profile', async (ctx) => {
	try {

		const token = ctx.request.header.authorization;
		const resultToken = token.substr(7);
		const jwtRes = await jwtDecode(resultToken);

		const getProfile = await knex('users')
			.where('userId', jwtRes['userId'])
			.select('def_profile');

		if (getProfile.length === 0 || !getProfile[0]['def_profile']){
			ctx.response.status = 200;
			ctx.response.body = false;
			return;
		}

		ctx.response.status = 200;
		ctx.response.body = getProfile[0]['def_profile'];

	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/user_set_profile', async (ctx) => {
	try {
		const profile = ctx.request.body.profile;

		const token = ctx.request.header.authorization;
		const resultToken = token.substr(7);
		const jwtRes = await jwtDecode(resultToken);

		knex('users')
			.where('userId', jwtRes['userId'])
			.update('def_profile', profile)
			.then();

		ctx.response.status = 200;
		ctx.response.body = true;

	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});


module.exports = router;