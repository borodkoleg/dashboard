require('dotenv-flow').config({silent: true});
const Router = require('koa-router');
const router = new Router();
//const knex = require('../libs/knexConfig');
//const Axios = require('axios');
const {formatDay, dayMinusDays, diffBetweenDate, DeltaDays} = require('../components/data/date');
//const path = require('path');

const {google} = require('googleapis');
const service_account = require('../settings/keys/googleAnalyticsKey.json');
//const reporting = google.analyticsreporting('v4');

const scopes = [
  'https://www.googleapis.com/auth/webmasters',
  'https://www.googleapis.com/auth/webmasters.readonly'
];

let jwt = new google.auth.JWT(
  service_account.client_email,
  null,
  service_account.private_key,
  scopes
);

const webmasters = google.webmasters({
  version: 'v3',
  auth: jwt
});

router.post('/api/google-sc-get-sites', async (ctx, next) => {
  try {
    const getSites = await webmasters.sites.list();

    if (getSites &&
      getSites.data &&
      getSites.data.siteEntry){
      ctx.response.status = 200;
      ctx.body = getSites.data.siteEntry;
    } else {
      //console.log('error', getSites.data);
      ctx.response.status = 400;
      ctx.body = false;
    }
  } catch(err){
    console.log('error', err);
    ctx.response.status = 400;
    ctx.body = false;
  }
});

router.post('/api/google-sc-get-data', async (ctx, next) => {
  try {
    let site = ctx.request.body.site;
    let dateFrom = ctx.request.body.dateFrom;
    let dateTo = ctx.request.body.dateTo;

    dateFrom = formatDay(dateFrom, 'YYYY-MM-DD');
    dateTo = formatDay(dateTo, 'YYYY-MM-DD');

    const result = await webmasters.searchanalytics.query({
      siteUrl: site,
      resource : {
        "startDate": dateFrom,
        "endDate": dateTo,
        "dimensions": ["query"]
      }
    });

    if (result &&
        result['data'] &&
        result['data']['rows']){
      //console.log(result['data']['rows']);
      ctx.response.status = 200;
      ctx.body = result['data']['rows'];
    } else {
      //console.log(result);
      ctx.response.status = 200;
      ctx.body = false;
    }
  } catch(err){
    console.log(err);
    ctx.response.status = 400;
    ctx.body = false;
  }
});

router.post('/api/google-sc-get-l-pages', async (ctx, next) => {
  try {
    let site = ctx.request.body.site;
    let dateFrom = ctx.request.body.dateFrom;
    let dateTo = ctx.request.body.dateTo;

    dateFrom = formatDay(dateFrom, 'YYYY-MM-DD');
    dateTo = formatDay(dateTo, 'YYYY-MM-DD');

    //dimensions: date, query, page, country, device, searchAppearance,
    const result = await webmasters.searchanalytics.query({
      siteUrl: site,
      resource : {
        "startDate": dateFrom,
        "endDate": dateTo,
        "dimensions": ["page"]
      }
    });

    if (result &&
      result['data'] &&
      result['data']['rows']){
      //console.log(result['data']['rows']);
      ctx.response.status = 200;
      ctx.body = result['data']['rows'];
    } else {
      //console.log(result);
      ctx.response.status = 200;
      ctx.body = false;
    }
  } catch(err){
    console.log(err);
    ctx.response.status = 400;
    ctx.body = false;
  }
});

module.exports = router;
