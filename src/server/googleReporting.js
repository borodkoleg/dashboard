require('dotenv-flow').config({silent: true});
const Router = require('koa-router');
const router = new Router();
const knex = require('../libs/knexConfig');
const Axios = require('axios');
const {formatDay, dayMinusDays, diffBetweenDate, DeltaDays} = require('../components/data/date')

const {google} = require('googleapis');
const service_account = require('../settings/keys/googleAnalyticsKey.json');
const reporting = google.analyticsreporting('v4');

const scopes = [
	'https://www.googleapis.com/auth/analytics.readonly',
	'https://www.googleapis.com/auth/analytics.edit'
	];

let jwt = new google.auth.JWT(
    service_account.client_email, 
    null, 
    service_account.private_key,
    scopes
);

const analytics = google.analyticsreporting({
  version: "v4",
  auth: jwt
});

//=======

router.post('/api/google-get-4-metrics', async (ctx, next) => {
	try {
		let dateFrom = ctx.request.body.dateFrom;
		let dateTo = ctx.request.body.dateTo;
		let filters = ctx.request.body.filters;
		const view_id = ctx.request.body.view_id;

		//console.log('filters', filters);

		let daysForDelta = DeltaDays(dateFrom, dateTo)

		dateFrom = formatDay(dateFrom);
		dateTo = formatDay(dateTo);

		//=================
		//const authorize = await jwt.authorize();

		// console.log('analytics', analytics);
		// console.log('authorize', authorize);

		const res = await analytics.reports.batchGet({
    requestBody: {
      reportRequests: [
        {
	        viewId: view_id,
	        "dateRanges": [
		        {"startDate": dateFrom, "endDate": dateTo},
		        {"startDate": daysForDelta[0], "endDate": daysForDelta[1]}
		      ],
		      // dimensions: [ it is hard
		      //   {"name": "ga:source"},
		      //   {"name": "ga:medium"},
		      //   {"name": "ga:deviceCategory"},
		      //   {"name": "ga:channelGrouping"},
		      //   {"name": "ga:country"}
		      // ]
          metrics: [
          	{expression: 'ga:sessions'},
          	{expression: 'ga:goalCompletionsAll'},
          	{expression: 'ga:bounces'},
          	{expression: 'ga:avgSessionDuration'}
          	//{expression: 'ga:users'},
            //{expression: 'ga:bounceRate'},
            //{expression: 'ga:avgSessionDuration'}
          ],
          //dimensionFilterClauses: filters
          //filtersExpression: 'ga:source==google;ga:source==google'
          filtersExpression: filters
	      }
	      ],
	    },
	  });
	  //console.log(JSON.stringify(res.data, null, 2));
		//=================

		ctx.response.status = 200;
		ctx.body = res.data;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});
	
// await Axios.get('https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties/~all/profiles?access_token=ya29.c.Kl-0B...
// let result = await Axios.get('https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties' , {},
 

router.post('/api/google-get-filters', async (ctx, next) => {
	try {
		let dateFrom = ctx.request.body.dateFrom;
		let dateTo = ctx.request.body.dateTo;
		let filters = ctx.request.body.filters;
		const view_id = ctx.request.body.view_id;

		dateFrom = formatDay(dateFrom);
		dateTo = formatDay(dateTo);

		const res = await analytics.reports.batchGet({
    requestBody: {
      reportRequests: [
        {
	        viewId: view_id,
	        "dateRanges": [
		        {"startDate": dateFrom, "endDate": dateTo}
		      ],
          metrics: [
          	{expression: 'ga:users'},
          ],
          dimensions: [
		        {"name": "ga:source"},
		        {"name": "ga:medium"},
		        {"name": "ga:deviceCategory"},
		        {"name": "ga:channelGrouping"},
		        {"name": "ga:country"}
		      ],
		      filtersExpression: filters,
		      orderBys: [
		        {fieldName: "ga:users", "sortOrder": "DESCENDING"},
      		]
	    }]
	  }});
	  //console.log(JSON.stringify(res.data, null, 2));
		//=================

		ctx.response.status = 200;
		ctx.body = res.data;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/google-get-sessions', async (ctx, next) => {
	try {
		let dateFrom = ctx.request.body.dateFrom;
		let dateTo = ctx.request.body.dateTo;
		let filters = ctx.request.body.filters;
		const view_id = ctx.request.body.view_id;

		dateFrom = formatDay(dateFrom);
		dateTo = formatDay(dateTo);

		//=================

		const res = await analytics.reports.batchGet({
    requestBody: {
      reportRequests: [
        {
	        viewId: view_id,
	        "dateRanges": [
		        {"startDate": dateFrom, "endDate": dateTo}
		      ],
		      dimensions: [
		        {name: 'ga:date'}
		      ],
          metrics: [
          	{expression: 'ga:sessions'}
          ],
          filtersExpression: filters
	      }
	      ],
	    },
	  });
	  //console.log(JSON.stringify(res.data, null, 2));
		//=================

		ctx.response.status = 200;
		ctx.body = res.data;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/google-get-goal-conversion-rate', async (ctx, next) => {
	try {
		let dateFrom = ctx.request.body.dateFrom;
		let dateTo = ctx.request.body.dateTo;
		let filters = ctx.request.body.filters;
		const view_id = ctx.request.body.view_id;

		dateFrom = formatDay(dateFrom);
		dateTo = formatDay(dateTo);

		const res = await analytics.reports.batchGet({
    requestBody: {
      reportRequests: [
        {
	        viewId: view_id,
	        "dateRanges": [
		        {"startDate": dateFrom, "endDate": dateTo}
		      ],
		      dimensions: [
		        {name: 'ga:date'}
		      ],
          metrics: [
          	{expression: 'ga:goalConversionRateAll'}
          ],
          filtersExpression: filters
	      }
	      ],
	    },
	  });
		//=================

		ctx.response.status = 200;
		ctx.body = res.data;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/google-sessions-by-sources', async (ctx, next) => {
	try {
		let dateFrom = ctx.request.body.dateFrom;
		let dateTo = ctx.request.body.dateTo;
		let filters = ctx.request.body.filters;
		const view_id = ctx.request.body.view_id;

		dateFrom = formatDay(dateFrom);
		dateTo = formatDay(dateTo);

		const res = await analytics.reports.batchGet({
    requestBody: {
      reportRequests: [
        {
	        viewId: view_id,
	        "dateRanges": [
		        {"startDate": dateFrom, "endDate": dateTo}
		      ],
		      dimensions: [
		        {name: 'ga:source'}
		      ],
          metrics: [
          	{expression: 'ga:sessions'}
          ],
          filtersExpression: filters,
          orderBys: [
          	{
				      fieldName: 'ga:sessions',
				      sortOrder: 'DESCENDING'
				    }
          ]
	      }
	      ],
	    },
	  });
		//=================

		ctx.response.status = 200;
		ctx.body = res.data;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

router.post('/api/google-sessions-by-user-type', async (ctx, next) => {
	try {
		let dateFrom = ctx.request.body.dateFrom;
		let dateTo = ctx.request.body.dateTo;
		let filters = ctx.request.body.filters;
		const view_id = ctx.request.body.view_id;

		dateFrom = formatDay(dateFrom);
		dateTo = formatDay(dateTo);

		const res = await analytics.reports.batchGet({
    requestBody: {
      reportRequests: [
        {
	        viewId: view_id,
	        "dateRanges": [
		        {"startDate": dateFrom, "endDate": dateTo}
		      ],
		      dimensions: [
		        {name: 'ga:userType'}
		      ],
          metrics: [
          	{expression: 'ga:sessions'}
          ],
          filtersExpression: filters,
	      }
	      ],
	    },
	  });
		//=================

		ctx.response.status = 200;
		ctx.body = res.data;
	} catch(err){
		console.log(err);
		ctx.response.status = 400;
		ctx.body = false;
	}
});

module.exports = router;