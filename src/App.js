import React, {Component} from 'react';
import { Route, Switch } from "react-router-dom";

//import Login from './components/Login'
import Home from './components/Home'
import Page404 from './components/Page404'
import Dashboard from './components/Dashboard'
import AddMonitor from './components/dashboard-parts/AddMonitor'
import EditMonitor from './components/dashboard-parts/EditMonitor'
import Graphic from './components/Graphic'
import GraphicPart2 from './components/GraphicPart2'
import GraphicPart3 from './components/GraphicPart3'
import Forms from './components/Forms'
import ValidationPage from './components/ValidationPage'
import Images from './components/Images'
import Table2 from './components/Table2'
import Others from './components/Others'
import Maps from './components/Maps'

import AdminLayout from './layouts/AdminLayout'
import LoginGoogle from './components/1Prod/LoginGoogle'
import ReportingAPI from './components/1Prod/pages/ReportingAPI'
import ReportingAPIPage2 from './components/1Prod/pages/ReportingAPIPage2'
import Start from './components/1Prod/pages/Start'
//import CallTracking from './components/1Prod/pages/CallTracking'
import P_ChooseElements from './components/1Prod/pages/P_ChooseElements' 
import P_Management from './components/1Prod/pages/P_Management'
import P_GoogleSearchConsole from './components/1Prod/pages/P_GoogleSearchConsole'
import P_Cro from "./components/1Prod/pages/P_Cro";

const jwt = require('jsonwebtoken');

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout history={props.history}>
      <Component {...props} />
    </Layout>
  )} />
);

class App extends Component {
	state = {
		accessToken: false
  };

  UNSAFE_componentWillMount() {
    this.unlisten = this.props.history.listen((location, action) => {
      //on route change
      this.tokenCheck();
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

	componentDidMount(){
		this.tokenCheck();
	}

	tokenCheck() {
  	let this_ = this;
  	let token = false;

  	if (!localStorage.getItem('token')){
			this_.setState({
			  accessToken: false
			})
			return
  	}

  	token = localStorage.getItem('token');
  	const decodedToken = jwt.decode(token, {complete: true});

		if (decodedToken && 
			decodedToken.hasOwnProperty('payload') &&
			decodedToken.payload.exp > Date.now() / 1000
			//decodedToken.payload.time > new Date().getTime()
			){
		  	this_.setState({
    			accessToken: true
  			})
  			return
		}
		//if (window.gapi) {
    //  const auth2 = window.gapi.auth2.getAuthInstance();
    //  if (auth2 != null && auth2.isSignedIn.get()) {
    //  	this_.setState({
    //			accessToken: true
  	//		})
  	//		return;
    //  }
    //}

  	localStorage.removeItem('token');
  	this_.setState({
    	accessToken: false
  	})
  	this.props.history.push({
		  pathname: '/login'
		})
  }

	render() {
		const {accessToken} =  this.state

	  return (
	    <Switch>
	      <Route exact path="/" component={Home} />
	      <Route exact path="/login" component={LoginGoogle} />
	      
	      {accessToken && <AppRoute exact path="/dashboard" component={Dashboard} layout={AdminLayout} />}
	      {accessToken && <AppRoute exact path="/dashboard/add-monitor" component={AddMonitor} layout={AdminLayout} />}
	      {accessToken && <AppRoute exact path="/dashboard/edit-monitor" component={EditMonitor} layout={AdminLayout} />}

	      {accessToken && <AppRoute exact path="/charts" component={Graphic} layout={AdminLayout} />}
	      {accessToken && <AppRoute exact path="/charts-part2" component={GraphicPart2} layout={AdminLayout} />}
	      {accessToken && <AppRoute exact path="/charts-part3" component={GraphicPart3} layout={AdminLayout} />}
	      {accessToken && <AppRoute exact path="/charts-part3" component={GraphicPart3} layout={AdminLayout} />}

	      {accessToken && <AppRoute exact path="/forms" component={Forms} layout={AdminLayout} />}
	      {accessToken && <AppRoute exact path="/images" component={Images} layout={AdminLayout} />}
	      {accessToken && <AppRoute exact path="/validation" component={ValidationPage} layout={AdminLayout} />}   
	      {accessToken && <AppRoute exact path="/table2" component={Table2} layout={AdminLayout} />}
				{accessToken && <AppRoute exact path="/others" component={Others} layout={AdminLayout} />}
				{accessToken && <AppRoute exact path="/maps" component={Maps} layout={AdminLayout} />}

				{accessToken && <AppRoute exact path="/reporting-API" component={ReportingAPI} layout={AdminLayout} />}
				{accessToken && <AppRoute exact path="/reporting-API-page2" component={ReportingAPIPage2} layout={AdminLayout} />}
				{accessToken && <AppRoute exact path="/start" component={Start} layout={AdminLayout} />}
				{accessToken && <AppRoute exact path="/choose-elements" component={P_ChooseElements} layout={AdminLayout} />}
				{accessToken && <AppRoute exact path="/management" component={P_Management} layout={AdminLayout} />}
				{accessToken && <AppRoute exact path="/google-search-console" component={P_GoogleSearchConsole} layout={AdminLayout} />}
				{accessToken && <AppRoute exact path="/cro" component={P_Cro} layout={AdminLayout} />}

	      <Route component={Page404} />
	    </Switch>
	  );
	}
}

export default App;
