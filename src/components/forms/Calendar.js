import React, {Component} from 'react';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

class Calendar extends Component {
  state = {
    calendarOpen: false
  };
  
  componentDidMount() {
  }

  handleDateOpen = () => {
  	this.setState({
  		calendarOpen: true
  	})
  }

  handleDateClose = () => {
  	this.setState({
  		calendarOpen: false
  	})
  }

  dataChange = (date) => {
  	this.setState({
  		calendarOpen: false
  	})
  	this.props.change(date)
  }

  render() {
    //const {} = this.props;
    const {calendarOpen} = this.state;

    return (
    	<MuiPickersUtilsProvider utils={DateFnsUtils}> 
    		<KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="MM-dd-yyyy"
          margin="normal"
          style={{width: '100%'}}
          open={calendarOpen}
          label={this.props.label}
          value={this.props.date}
          onChange={this.dataChange}
          onOpen={this.handleDateOpen}
          onClose={this.handleDateClose}
          className="datePickerBasis"
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
	    </MuiPickersUtilsProvider>
    )
  }
}

export default Calendar;