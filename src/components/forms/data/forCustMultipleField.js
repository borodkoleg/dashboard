export default [
  {
    name: '1',
    color: '#7057ff',
    description: 'Good for newcomers',
  },
  {
    name: '2',
    color: '#008672',
    description: 'Extra attention is needed',
  },
  {
    name: '3',
    color: '#b60205',
    description: '',
  },
  {
    name: '4',
    color: '#d93f0b',
    description: '',
  },
  {
    name: '5',
    color: '#0e8a16',
    description: '',
  },
  {
    name: '6',
    color: '#fbca04',
    description: '',
  },
  {
    name: "7",
    color: '#fec1c1',
    description: '',
  },
  {
    name: '8',
    color: '#215cea',
    description: '',
  },
  {
    name: '9',
    color: '#cfd3d7',
    description: 'This issue or pull request already exists',
  },
  {
    name: '10',
    color: '#fef2c0',
    description: '',
  }
];