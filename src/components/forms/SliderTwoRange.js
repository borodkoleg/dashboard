import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';

const useStyles = makeStyles({
  root: {
    
  },
});

function valuetext(value) {
  return `${value}`;
}

export default function SliderTwoRange() {
  const classes = useStyles();
  const [value, setValue] = React.useState([20, 75]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const marks = [
	  {
	    value: 0,
	    label: '0',
	  },
	  {
	    value: 20,
	    label: '20',
	  },
	  {
	    value: 40,
	    label: '40',
	  },
	  {
	    value: 60,
	    label: '60',
	  },
	  {
	    value: 80,
	    label: '80',
	  },
	  {
	    value: 100,
	    label: '100',
	  },
	];

  return (
    <div className={classes.root} style={{marginRight: '16px'}}>
      <Typography id="range-slider" gutterBottom>
        Test2
      </Typography>
      <Slider
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        getAriaValueText={valuetext}
        marks={marks}
        //step={5}
      />
    </div>
  );
}