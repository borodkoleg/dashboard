import React, {Component} from 'react';
import { Picky } from 'react-picky';
import "react-picky/dist/picky.css";

class MultiSelect extends Component {
	static defaultProps = {
		data: [
	  	{key: 1, value: '1ww'},
	  	{key: 2, value: '2ww'}
    ]
	}

	state = {
	  value: null,
	  arrayValue: []
	};
  
  componentDidMount() {
  
  }

  selectOption = (value) => {
    this.setState({value: value});
  }

  selectMultipleOption = (value) => {
    this.setState({ 
    	arrayValue: value 
    });

    this.props.onChange(value)
  }

  render() {
    const {data, placeholder, style} = this.props;
    const {arrayValue} = this.state;

    return (
    	<div style={style}>
    	 <Picky
        value={arrayValue}
        options={data}
        onChange={this.selectMultipleOption}
        //open={false}
        valueKey="key"
        labelKey="value"
        multiple={true}
        includeSelectAll={true}
        includeFilter={true}
        className='multi-select-picky'
        placeholder={placeholder}
        //allSelectedPlaceholder={"All items selected"}
        //manySelectedPlaceholder={"You have selected %s items"}
        //dropdownHeight={600}
        render={({
          //index,
          style,
          item,
          isSelected,
          selectValue,
          labelKey,
          valueKey,
          multiple
        }) => {
          return (
            <li
              style={{ ...style }} // required
              className={isSelected ? "selected" : ""} // required to indicate is selected
              key={item[valueKey]} // required
              onClick={() => selectValue(item)}
            >
              
              <input type="checkbox" checked={isSelected} readOnly />
              <span class="checkmark"></span>
        			<span>
        				{item[labelKey]}
        				<span style={{
        					float: 'right',
									position: 'relative',
									top: '9px',
									right: '10px'
        				}}>
        					{item.number ? item.number : ''}
        				</span>
        			</span>
            </li>
          );
        }}
      />
      </div>
    )
  }
}

export default MultiSelect;