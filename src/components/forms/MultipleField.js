import React, {Component} from 'react';
import {Autocomplete} from '@material-ui/lab';
import TextField from '@material-ui/core/TextField';
import data from './data/forMultipleField';

class MultipleField extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<Autocomplete
        multiple
        options={data}
        getOptionLabel={option => option.title}
        defaultValue={[data[1]]}
        filterSelectedOptions
        renderInput={params => (
          <TextField
            {...params}
            variant="outlined"
            label="Filter Selected Options"
            placeholder="Favorites"
            margin="normal"
            fullWidth
          />
        )}
      />
    )
  }
}

export default MultipleField;