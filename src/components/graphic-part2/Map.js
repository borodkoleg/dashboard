import React, {Component} from 'react';
import { ResponsiveChoropleth } from '@nivo/geo';
import countries from "./data-map/countries.json";
import data from "./data-map/data";

const theme = {
  fontSize: 12
}

class Map extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{height: '250px'}} className={'maps'}>
    	<ResponsiveChoropleth
        data={data}
        theme={theme}
        features={countries.features}
        margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
        colors={this.props.colors} //greys !
        domain={[ 0, 1000000 ]}
        unknownColor="#666666"
        label="properties.name"
        valueFormat=".2s"
        projectionTranslation={[ 0.49, 0.50 ]}
        projectionRotation={[ 0, 0, 0 ]}
        projectionScale = {75} //!
        enableGraticule={true}
        graticuleLineColor="#dddddd"
        borderWidth={0.5}
        borderColor="#152538"
        legends={[
            {
                anchor: 'bottom-left',
                direction: 'column',
                justify: false,
                translateX: 0, //!
                translateY: 0, //!
                itemsSpacing: 0,
                itemWidth: 94,
                itemHeight: 22, //!
                itemDirection: 'left-to-right',
                itemTextColor: '#444444',
                itemOpacity: 0.9, //!
                symbolSize: 20, //!
                effects: [
                    {
                        on: 'hover',
                        style: {
                            itemTextColor: '#000000',
                            itemOpacity: 1
                        }
                    }
                ]
            }
        ]}
    	/>
    	</div>
    )
  }
}

export default Map;