import React, {Component} from 'react';
import { PieChart, Pie, Sector, ResponsiveContainer, Cell } from 'recharts';

const data = [
  { name: 'Group A', value: 78 }
];

const COLORS = ['#0088FE'];

const renderActiveShape = (props) => {
	//midAngle, payload, percent, value
  const {
    cx, cy, innerRadius, outerRadius, startAngle, endAngle,
    fill
  } = props;

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{Math.round(endAngle / 3.6)} %</text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
    </g>
  );
};

class PieChartPartProcent extends Component {
  state = {
   
  };
  
  componentDidMount() {
  }

  render() {

    return (
    	<div style={{ width: '100%', height: 250 }}>
        <ResponsiveContainer>
		    	<PieChart width={400} height={400}>
		        <Pie
		          activeIndex={0}
		          activeShape={renderActiveShape}
		          data={data}
		          innerRadius={60}
		          outerRadius={80}
		          dataKey="value"
		          startAngle={0}
        			endAngle={3.6 * data[0].value}
		        >
		        {
            data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
          	}
          	</Pie>
      		</PieChart>
		    </ResponsiveContainer>  
		  </div>  
    )
  }
}

export default PieChartPartProcent;