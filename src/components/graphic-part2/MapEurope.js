import React, {Component} from 'react';
import { ResponsiveChoropleth } from '@nivo/geo';
import countries from "./data-map/countries.json";
import data from "./data-map/data";

const theme = {
  fontSize: 12
}

class Map extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{height: '250px'}} className={'maps'}>
    	<ResponsiveChoropleth
        data={data}
        theme={theme}
        features={countries.features}
        margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
        colors="nivo"
        domain={[ 0, 1000000 ]}
        unknownColor="#666666"
        label="properties.name"
        valueFormat=".2s"
        projectionType="orthographic"
        projectionScale={375}
        projectionTranslation={[ 0.55, 0.5 ]}
        projectionRotation={[ -27, -40, 0 ]}
        enableGraticule={true}
        graticuleLineWidth={1}
        graticuleLineColor="#dddddd"
        borderColor="#152538"
        legends={[
            {
                anchor: 'bottom-left',
                direction: 'column',
                justify: true,
                translateX: 0,
                translateY: -130,
                itemsSpacing: 2,
                itemWidth: 94,
                itemHeight: 18,
                itemDirection: 'left-to-right',
                itemTextColor: '#444444',
                itemOpacity: 0.85,
                symbolSize: 18,
                effects: [
                    {
                        on: 'hover',
                        style: {
                            itemTextColor: '#000000',
                            itemOpacity: 1
                        }
                    }
                ]
            }
        ]}
    />
    	</div>
    )
  }
}

export default Map;