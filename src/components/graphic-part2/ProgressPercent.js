import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { LinearProgress } from '@material-ui/core';

class ProgressPercent extends Component {
	constructor(props){
    super(props)
    this.linearRef = React.createRef();
    this.labelRef = React.createRef();
  }

  state = {
    labelPosition: 0
  };

	componentDidMount() {
		let labelWidth = this.labelRef.current.offsetWidth;

		let linearWidth = (this.linearRef.current.offsetWidth / 100) * this.props.value;

		let position = 0;
		if (labelWidth < linearWidth - 2){
			position = linearWidth - labelWidth - 2; 
		} else {
			position = 2;
		}

		this.setState({
			labelPosition: position
		})
	
  }

  componentWillUpdate(){
		this.linearRef.current.children[0].style = `transform: translateX(${this.props.value - 100}%);`;
  }

  render() {
  	const { labelPosition } = this.state;
    const { classes, value } = this.props;

    return (
    	<div style={{position: 'relative'}}>
	    	<LinearProgress {...this.props} 
	    		classes={{colorPrimary: classes.colorPrimary, barColorPrimary: classes.barColorPrimary}} 
	    		value={0} //value
	    		variant={'determinate'} 
	    		ref={this.linearRef}
	    		/>
	    	<div
	    		ref={this.labelRef}
	    	  style={{
	    		position: 'absolute',
    			top: '0',
    			color: '#FFFFFF',
    			transitionProperty: 'left',
				  transitionDuration: '1s',
				  WebkitTransitionProperty: 'left',
  				WebkitTransitionDuration: '1s',
    			left: `${labelPosition}px`
	    	}}>{value}%</div>
    	</div>
    );
  }
}

//breakpoints,direction,mixins,overrides,palette,props,shadows,typography,spacing,shape,transitions,zIndex
const styles = props => ({
  colorPrimary: {
    backgroundColor: '#B2DFDB',
    height: '16px',
    padding: '2px 0 1px',
    position: 'relative',
   //  '&::after': {
   //  	display: 'block',
   //  	position: 'absolute',
   //  	content: `${props.zIndex}`,
   //  	top: '0',
   //  	left: '10px',
   //  	color: '#FFFFFF'
  	// }
  },
  barColorPrimary: {
    backgroundColor: '#00695C',
  }
});

export default  withStyles(styles)(ProgressPercent);