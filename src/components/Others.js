import React, {Component} from 'react';
import Tree from './others/Tree';
import Tree2 from './others/Tree2';
import Grid from '@material-ui/core/Grid';

class Others extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<Grid container>
    		<Grid item xs={12} sm={12} md={4} lg={4}>
    			<Tree/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={4} lg={4}>
    			<Tree2/>
    		</Grid>
    	</Grid>
    )
  }
}

export default Others;