import React, {Component} from 'react';
import SimpleLine from './graphic-part/SimpleLine';
import Grid from '@material-ui/core/Grid';
import PieChartGraph from './graphic-part/PieChartGraph';
import PieChartPart from './graphic-part/PieChartPart';
import RadialBarChartEl from './graphic-part/RadialBarChartEl';
import LineWithLabels from './graphic-part/LineWithLabels';
import AreaChartFillByValue from './graphic-part/AreaChartFillByValue';

class Graphic extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<Grid container>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<SimpleLine/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    		 <PieChartGraph/>
    		</Grid>
				<Grid item xs={12} sm={12} md={6} lg={6}>
    		 <PieChartPart/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<RadialBarChartEl/>
    		</Grid>
				<Grid item xs={12} sm={12} md={6} lg={6}>
    			<LineWithLabels/>
    		</Grid>
				<Grid item xs={12} sm={12} md={6} lg={6}>
    			<AreaChartFillByValue/>
    		</Grid>
    	</Grid>
    )
  }
}

export default Graphic;