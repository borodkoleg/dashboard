import React, {Component} from 'react';
import SimpleMap from './maps/SimpleMap';
import Polygon from './maps/Polygon';
import ActiveMap from './maps/ActiveMap';
import Grid from '@material-ui/core/Grid';

class Maps extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
			<Grid container>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<SimpleMap/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<Polygon/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<ActiveMap/>
    		</Grid>
    	</Grid>
    )
  }
}

export default Maps;