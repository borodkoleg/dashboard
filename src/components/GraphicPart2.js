import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import PieChartPartCusomize from './graphic-part2/PieChartPartCusomize';
import PieChartPartProcent from './graphic-part2/PieChartPartProcent';
import ProgressPercent from './graphic-part2/ProgressPercent';
import Map from './graphic-part2/Map';
import MapEurope from './graphic-part2/MapEurope';


class GraphicPart2 extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<Grid container>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<PieChartPartCusomize/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    		 	<PieChartPartProcent/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<ProgressPercent
    				value={1}
    			/>
    			<br />

    			<ProgressPercent
    				value={4}
    			/>
    			<br />

    			<ProgressPercent
    				value={13}
    			/>
    			<br />

    			<ProgressPercent
    				value={27}
    			/>
    			<br />

    			<ProgressPercent
    				value={50}
    			/>
    			<br />

    			<ProgressPercent
    				value={97}
    			/>
    			<br />

    			<ProgressPercent
    				value={100}
    			/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<Map
    				colors = {'nivo'}
    			/>
    		</Grid>
				<Grid item xs={12} sm={12} md={6} lg={6}>
					<Map
    				colors = {'greys'}
    			/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
					<MapEurope
    				colors = {'nivo'}
    			/>
    		</Grid>
    	</Grid>
    )
  }
}

export default GraphicPart2;