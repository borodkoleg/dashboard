import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import MultipleField from './forms/MultipleField';
import CustMultipleField from './forms/CustMultipleField';
import SliderRange from './forms/SliderRange';
import SliderTwoRange from './forms/SliderTwoRange';
import Calendar from './forms/Calendar';
import MultiSelect from './forms/MultiSelect';

const { getDatesBetween } = require('./data/date');

class Forms extends Component {
  state = {
    calendar1Date: new Date(),
    calendar2Date: new Date()
  };
  
  componentDidMount() {
  }

  onChangeCommitted = (data, value) => {
  	console.log(value);
  }

  calendar1Change = (date) => {
  	this.setState({
  		calendar1Date: date
  	})
  }

  calendar2Change = (date) => {
  	this.setState({
  		calendar2Date: date
  	})
  }

  render() {
    //const {} = this.props;
    const {calendar1Date, calendar2Date} = this.state;
    console.log(getDatesBetween(calendar1Date, calendar2Date));

    return (
    	<Grid container>
    		<Grid item xs={12} sm={12} md={4} lg={4}>
    			<MultipleField/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={4} lg={4}>
    			<CustMultipleField/>
    		</Grid>
				<Grid item xs={12} sm={12} md={4} lg={4}>
    			<SliderRange
    				onChangeCommitted={(data, value)=>this.onChangeCommitted(data, value)}
    			/>
    		</Grid>
				<Grid item xs={12} sm={12} md={4} lg={4}>
    			<SliderTwoRange/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={4} lg={4}>
    			<Calendar
    				date={calendar1Date}
    				change={(data) => this.calendar1Change(data)}
    				label='Calendar 1'
    			/>
    			<Calendar
    				date={calendar2Date}
    				change={(data) => this.calendar2Change(data)}
    				label='Calendar 2'
    			/>
    		</Grid>
				<Grid item xs={12} sm={12} md={4} lg={4}>
		      {getDatesBetween(calendar1Date, calendar2Date).map((value, index) => {
		        return <div style={{display: 'inline-block',
    marginRight: '20px'}} key={index}>{value}</div>
		      })}
    		</Grid>
    		<Grid item xs={12} sm={12} md={4} lg={4}>
    			<MultiSelect
    				onChange={(data) => {console.log(data)}}
    			/>
    		</Grid>
    	</Grid>
    )
  }
}

export default Forms;