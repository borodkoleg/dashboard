import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const { basis } = require('./data/themes');
const theme1 = createMuiTheme(basis);

const valid = require('./data/validation');

let startInputsValue = {
	email: '',
	integer: '0',
	integerPositive: '0',
	float: '1',
	string: '',
	password: '',
	login: '',
	date: '',
	year: '',
};
class ValidationPage extends Component {
  state = {
    inputsValue: startInputsValue
  };
  
  componentDidMount() {
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);

		  if (key === 'password'){
		  	obj[key] = newData;
			} else {
				obj[key] = newData.toUpperCase();
			}

		  return { inputsValue: obj };
		})
  }

  render() {
    //const {} = this.props;
    const {inputsValue} = this.state;

    return (
    	<div>
    	<MuiThemeProvider theme={theme1}>
    		<TextField
	        placeholder='Email'
	        variant='outlined'
	        label='Email'
	        helperText={!valid.isEmail(inputsValue['email'], true) ? 'Incorrect Email' : ''}
	        value={inputsValue['email']}
	        name="email"
	        onChange={(value, key) => this.inputChange(value, 'email')}
	        error={!valid.isEmail(inputsValue['email'], true)}
	        style={{marginRight: '10px'}}
		    />

		    <TextField
	        placeholder='Integer'
	        variant='outlined'
	        label='Integer'
	        helperText={!valid.isInteger(inputsValue['integer'], true) ? 'Incorrect Integer' : ''}
	        value={inputsValue['integer']}
	        name="integer"
	        onChange={(value, key) => this.inputChange(value, 'integer')}
	        error={!valid.isInteger(inputsValue['integer'], true)}
	        style={{marginRight: '10px'}}
		    />

		    <TextField
	        placeholder='Integer Positive'
	        variant='outlined'
	        label='Integer Positive'
	        helperText={!valid.isIntegerPositive(inputsValue['integerPositive'], true) ? 'Incorrect Integer Positive' : ''}
	        value={inputsValue['integerPositive']}
	        name="integerPositive"
	        onChange={(value, key) => this.inputChange(value, 'integerPositive')}
	        error={!valid.isIntegerPositive(inputsValue['integerPositive'], true)}
	        style={{marginRight: '10px'}}
		    />

		    <TextField
	        placeholder='Float'
	        variant='outlined'
	        label='Float'
	        helperText={!valid.isFloat(inputsValue['float'], true) ? 'Incorrect Float' : ''}
	        value={inputsValue['float']}
	        name="float"
	        onChange={(value, key) => this.inputChange(value, 'float')}
	        error={!valid.isFloat(inputsValue['float'], true)}
	        style={{marginRight: '10px'}}
		    />

		    <TextField
	        placeholder='String Eng.'
	        variant='outlined'
	        label='String Eng.'
	        helperText={!valid.isEngString(inputsValue['string'], true) ? 'Incorrect Eng. String' : ''}
	        value={inputsValue['string']}
	        name="string"
	        onChange={(value, key) => this.inputChange(value, 'string')}
	        error={!valid.isEngString(inputsValue['string'], true)}
	        style={{marginRight: '10px'}}
		    />

		     <TextField
	        placeholder='password'
	        variant='outlined'
	        label='password'
	        type='password'
	        helperText={!valid.isPassword(inputsValue['password'], true) ? 'Password must has a-z, 0-9, A-Z, len. 6' : ''}
	        value={inputsValue['password']}
	        name="password"
	        onChange={(value, key) => this.inputChange(value, 'password')}
	        error={!valid.isPassword(inputsValue['password'], true)}
	        style={{marginRight: '10px'}}
		    />

		    <TextField
	        placeholder='Login'
	        variant='outlined'
	        label='Login'
	        helperText={!valid.isLogin(inputsValue['login'], true) ? 'Incorrect Login' : ''}
	        value={inputsValue['login']}
	        name="login"
	        onChange={(value, key) => this.inputChange(value, 'login')}
	        error={!valid.isLogin(inputsValue['login'], true)}
	        style={{marginRight: '10px'}}
		    />

		    <TextField
	        placeholder='Date'
	        variant='outlined'
	        label='Date'
	        helperText={!valid.isDate(inputsValue['date'], true) ? 'Format MM-DD-YYYY' : ''}
	        value={inputsValue['date']}
	        name="date"
	        onChange={(value, key) => this.inputChange(value, 'date')}
	        error={!valid.isDate(inputsValue['date'], true)}
	        style={{marginRight: '10px'}}
		    />

		    <TextField
	        placeholder='Year'
	        variant='outlined'
	        label='Year'
	        helperText={!valid.isYear(inputsValue['year'], true) ? 'Format YYYY' : ''}
	        value={inputsValue['year']}
	        name="year"
	        onChange={(value, key) => this.inputChange(value, 'year')}
	        error={!valid.isYear(inputsValue['year'], true)}
	        style={{marginRight: '10px'}}
		    />

		  </MuiThemeProvider>  
    	</div>
    )
  }
}

export default ValidationPage;