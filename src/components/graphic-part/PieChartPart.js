import React, {Component} from 'react';
import {
  PieChart, Pie, Cell, ResponsiveContainer, Tooltip
} from 'recharts';

const data = [
  { name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 }, { name: 'Group D', value: 200 },
  { name: 'Group E', value: 278 }, { name: 'Group F', value: 189 },
];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

class PieChartPart extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{ width: '100%', height: 250 }}>
        <ResponsiveContainer>
		    	<PieChart>
		         <Pie
          data={data}
          //cx={120}
          //cy={200}
          innerRadius={30}
          outerRadius={80}
          //fill="#8884d8"
          //paddingAngle={5}
          dataKey="value"
          label
        >
          {
            data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
          }
        </Pie>
		        <Tooltip />
		      </PieChart>
		    </ResponsiveContainer>  
		  </div>  
    )
  }
}

export default PieChartPart;