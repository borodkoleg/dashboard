import React, {Component} from 'react';
import {
  Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Area, ComposedChart, Bar, Brush
} from 'recharts';

const data = [
  {
    name: '1', uv: 8000, pv: 2400, amt: 6400,
  },
  {
    name: '2', uv: 8000, pv: 1398, amt: 6210,
  },
  {
    name: '3', uv: 8000, pv: 9800, amt: 6290,
  },
  {
    name: '4', uv: 8780, pv: 3908, amt: 6000,
  },
  {
    name: '5', uv: 8890, pv: 6800, amt: 6181,
  },
  {
    name: '6', uv: 8390, pv: 3800, amt: 6500,
  },
  {
    name: '7', uv: 8490, pv: 7300, amt: 6100,
  },
    {
    name: '8', uv: 8000, pv: 2400, amt: 6400,
  },
  {
    name: '9', uv: 8000, pv: 1398, amt: 6210,
  },
  {
    name: '10', uv: 8000, pv: 9800, amt: 6290,
  },
  {
    name: '11', uv: 8780, pv: 3908, amt: 6000,
  },
  {
    name: '12', uv: 8890, pv: 6800, amt: 6181,
  },
  {
    name: '13', uv: 8390, pv: 3800, amt: 6500,
  },
  {
    name: '14', uv: 8490, pv: 7300, amt: 6100,
  },
];

class SimpleLine extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{ width: '100%', height: 250 }}>
        <ResponsiveContainer>
          <ComposedChart
            data={data}
          >
            <CartesianGrid stroke="#f5f5f5" strokeDasharray="3 3" />
            <XAxis dataKey="name" scale="point" /> {/*auto */}
            <YAxis />
            <Tooltip />
            <Legend />
            <Area type="monotone" dataKey="amt" fill="#0088FE" stroke="#8884d8" />
            <Bar dataKey="pv" barSize={10} fill="#00C49F" />
            <Line type="monotone" dataKey="uv" stroke="#FF8042" />    
       
        		<Brush dataKey="name" height={30} stroke="#FFBB28" />
      
          </ComposedChart>
        </ResponsiveContainer>
      </div>
    )
  }
}

export default SimpleLine;