import React, {Component} from 'react';
import { RadialBarChart, RadialBar, Legend, ResponsiveContainer } from 'recharts';

const data = [
  {
    name: '18-24', uv: 31.47, pv: 2400, fill: '#8884d8',
  },
  {
    name: '25-29', uv: 26.69, pv: 4567, fill: '#83a6ed',
  },
  {
    name: '30-34', uv: 15.69, pv: 1398, fill: '#8dd1e1',
  },
  {
    name: '35-39', uv: 8.22, pv: 9800, fill: '#82ca9d',
  },
  {
    name: '40-49', uv: 8.63, pv: 3908, fill: '#a4de6c',
  }
];

const style = {
  top: 30,
  left: 330,
  lineHeight: '24px',
};

class RadialBarChartEl extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{ width: '100%', height: 250 }}>
	    	<ResponsiveContainer>
		    	<RadialBarChart startAngle={180}
	          endAngle={-180} cx={200} innerRadius={10} outerRadius={110} barSize={30} data={data}>
		        <RadialBar minAngle={15} label={{ position: 'insideStart', fill: '#fff', fontSize: '12px' }} background clockWise dataKey="uv" />
		        <Legend iconSize={10} width={120} height={140} layout="vertical" verticalAlign="middle" wrapperStyle={style} />
		      </RadialBarChart>
	      </ResponsiveContainer>
      </div>
    )
  }
}

export default RadialBarChartEl;