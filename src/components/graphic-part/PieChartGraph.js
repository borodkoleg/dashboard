import React, {Component} from 'react';
import {
  ResponsiveContainer, PieChart, Pie, Cell, Tooltip
} from 'recharts';

class PieChartGraph extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;
    const data = [
		  { name: 'Group A', value: 300 },
		  { name: 'Group B', value: 300 },
		  { name: 'Group C', value: 300 },
		  { name: 'Group D', value: 300 },
		];

		const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FFFFFF']; //#FF8042

		const RADIAN = Math.PI / 180;
		const renderCustomizedLabel = ({
		  cx, cy, midAngle, innerRadius, outerRadius, percent, index,
		}) => {
		  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
		  const x = cx + radius * Math.cos(-midAngle * RADIAN);
		  const y = cy + radius * Math.sin(-midAngle * RADIAN);

		  return (
		    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
		      {`${(percent * 100).toFixed(0)}%`}
		    </text>
		  );
		};


    return (
    	<div style={{ width: '100%', height: 250 }}>
        <ResponsiveContainer>
          <PieChart>
            <Pie
	          data={data}
	          //cx={200}
	          //cy={200}
	          labelLine={false}
	          label={renderCustomizedLabel}
	          outerRadius={100}
	          fill="#8884d8"
	          dataKey="value"
	          //startAngle={270}
	          //endAngle={0}
        		>
		          {
		            data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
		          }
        		</Pie>
        		<Tooltip />
          </PieChart>
        </ResponsiveContainer>
      </div>
    )
  }
}

export default PieChartGraph;