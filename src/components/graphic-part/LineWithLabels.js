import React, {Component} from 'react';
import {
  XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, ComposedChart, Bar
} from 'recharts';

const data = [
  {
    name: '1', uv: 8000, pv: 2400, amt: 6400,
  },
  {
    name: '2', uv: 8000, pv: 1398, amt: 6210,
  },
  {
    name: '3', uv: 8000, pv: 9800, amt: 6290,
  },
  {
    name: '4', uv: 8780, pv: 3908, amt: 6000,
  },
  {
    name: '5', uv: 8890, pv: 6800, amt: 6181,
  },
  {
    name: '6', uv: 8390, pv: 3800, amt: 6500,
  },
  {
    name: '7', uv: 8490, pv: 7300, amt: 6100,
  },
    {
    name: '8', uv: 8000, pv: 2400, amt: 6400,
  },
  {
    name: '9', uv: 8000, pv: 1398, amt: 6210,
  },
  {
    name: '10', uv: 8000, pv: 9800, amt: 6290,
  },
  {
    name: '11', uv: 8780, pv: 3908, amt: 6000,
  },
  {
    name: '12', uv: 8890, pv: 6800, amt: 6181,
  },
  {
    name: '13', uv: 8390, pv: 3800, amt: 6500,
  },
  {
    name: '14', uv: 8490, pv: 7300, amt: 6100,
  },
];

class CustomizedLabel extends Component {
  render() {
    const {
      x, y, value,
    } = this.props;

    let xChange = x + 14;
    let yChange = y + 10;

    return <text x={xChange} y={yChange} dy={0} fill={'#FFFFFF'} fontSize={10} textAnchor="middle">{value}</text>;
  }
}

class CustomizedAxisTick extends Component {
  render() {
    const {
      x, y, payload,
    } = this.props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text style={{fontWeight: '700'}} x={0} y={0} dy={16} textAnchor="end" fill="#666">{payload.value}</text> 
      {/* transform="rotate(-35)" */}
      </g>
    );
  }
}

class LineWithLabels extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{ width: '100%', height: 250 }}>
        <ResponsiveContainer>
          <ComposedChart
            data={data}
          >
            <CartesianGrid stroke="#f5f5f5" strokeDasharray="3 3" />
            <XAxis tick={<CustomizedAxisTick />} dataKey="name" />
            <YAxis tick={<CustomizedAxisTick />} />
            <Tooltip />
            
            <Bar label={<CustomizedLabel />} dataKey="pv" barSize={30} fill="#00C49F" />
            
       
        		
      
          </ComposedChart>
        </ResponsiveContainer>
      </div>
    )
  }
}

export default LineWithLabels;