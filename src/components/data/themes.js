exports.basis = {
 overrides: {
    MuiButton: {
    	contained: {
    		'&:hover':{
    			backgroundColor: '#ffd300',
    			color:'#ffffff'
    		},
    		backgroundColor: '#ffd300',
    		borderColor: '#4cae4c',
    		color: '#000000',
    		textTransform: 'none',
    		padding: '4px 8px'
    	}
    },
    MuiDialogContent: {
    	root: {
    		overflowY: 'initial'
    	}
    },
    MuiOutlinedInput: {
    	input: {
    		padding: '7px 10px 9px 10px',
    		marginRight: '5px'
    	}
    },
    MuiFormControl: {
    	root: {
    		//minWidth:'280px'
    		marginBottom: '20px'
    	}
    },
    MuiInputLabel: {
    	formControl: {
    		top: '-10px'
    	}
  	},
    MuiInputBase: {
    	root: {
    		//marginRight:'10px'
    	},
    	input:{
    		color: 'black'
    	}
    },
    MuiTypography: {
    	h6: {
    		fontSize: '15px'
    	}
    },
    MuiTextField: {
    	root: {
    		minWidth: '240px'
    	}
    },
    MuiFormHelperText: {
    	contained: {
    		margin: '6px 0 0 2px'
    	}
    }
  }
}