const moment = require('moment');

module.exports.isEmail = function(value, require) { //почтовые ящики могут содержать апостраф, и это нужно учитывать при сохранении в бд
	if (value === '' && !require){
		return true;
	}

	return /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value);
}

module.exports.isInteger = function(value, require) {
	if (value === '' && !require){
		return true;
	}

	if (value && value.length > 0 && value[0] === '0' && value[1] === '0'){
		return false;
	}

	if (value && value.length > 1 && value[0] === '0'){
		return false;	
	}

	const number = value * 1; // +value;
	console.log(number);
	
	return /^[-]?\d+$/.test(value);
}

module.exports.isIntegerPositive = function(value, require) {
	if (value === '' && !require){
		return true;
	}

	const number = value * 1;
	if (number <= 0){
		return false;	
	}

	return /^\d+$/.test(value);
}

module.exports.isFloat = function(value, require) {
	if (value === '' && !require){
		return true;
	}

	if (value[0] === '0' && value[1] && value[1] !== '.'){
		return false;
	}

	return /^[-]?\d+(\.\d+)?$/.test(value);
}

module.exports.isEngString = function(value, require) {
	if (value === '' && !require){
		return true;
	}

	return /^[a-zA-Z0-9$@$!%*-?&#^-_. ,+]+$/.test(value);
}

module.exports.isPassword = function(value, require) {
	if (value === '' && !require){
		return true;
	}
	
	return /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/.test(value);
}

module.exports.isLogin = function(value, require) {
	if (value === '' && !require){
		return true;
	}
	
	return /^[a-zA-Z0-9_/.-]{3,16}$/.test(value);
}

module.exports.isDate = function(value, require) {
	if (value === '' && !require){
		return true;
	}

	if (value.length !== 10) {
	  return false;
	}

	let test1 = /^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$/.test(value);

	if (!test1){
		return false;
	}

	try {
		if (moment(value,'MM-DD-YYYY').isValid()) {
	    return true;
	  }
	  return false;
	} catch (e) {
	  return false;
	}

}

module.exports.isYear = function(value, require) {
	if (value === '' && !require){
		return true;
	}

	if (!/^\d{4}$/.test(value)){
		return false;
	}

	const number = value * 1; // +value;

	if (number <= 0 || number < 1000){
		return false;	
	}

	return (typeof number === 'number') && (number % 1 === 0);
};

