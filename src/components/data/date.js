const moment = require('moment');

function getDatesBetween(startDate, stopDate){
	let start = new Date(startDate);
	let end = new Date(stopDate);
	let newend = end.setDate(end.getDate()+1);
	let endNew = new Date(newend);
	let result = [];
	while(start < endNew){
	   result.push(moment(start).format('YYYY-MM-DD'));
	   let newDate = start.setDate(start.getDate() + 1);
	   start = new Date(newDate);
	}

	return result;
}

const today = moment().startOf('day')

let todayFix = new Date(moment().startOf('day'))
let todayMinus7 = todayFix.setDate(todayFix.getDate() - 7)
module.exports.todayMinus7 = moment(todayMinus7)

function diffBetweenDate(date1, date2){ //dayFrom, dayTo
	const date1D = new Date(date1);
	const date2D = new Date(date2);
	let diffTime = Math.abs(date2D - date1D);
	let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
	return diffDays;
}

function dayMinusDays(dayFrom, diffDays){
	let dayFromFormat = new Date(dayFrom);
	let result = dayFromFormat.setDate(dayFromFormat.getDate() - diffDays)
	return new Date(result)
}

function formatDay(day, pFormat){
	if (pFormat){
		return moment(day).format(pFormat)
	} else {
		return moment(day).format('YYYY-MM-DD')
	}
}

function formatDayFromFormat(value, formatTemplateStart, formatTemplateEnd){
	try {
		if (moment(value, formatTemplateStart).format(formatTemplateEnd) === 'Invalid date') {
			return value;
		} else {
			return moment(value, formatTemplateStart).format(formatTemplateEnd)
		}
	} catch (e) {
		return value;
	}
}

function DeltaDays(dayFrom, dayTo){
	const minusOneDay = dayMinusDays(dayFrom, 1);

	const differentBetweenDays = diffBetweenDate(dayFrom, dayTo)
	const secondDay = dayMinusDays(minusOneDay, differentBetweenDays);

	return [formatDay(secondDay), formatDay(minusOneDay)]
}

module.exports.dayMinusDays = dayMinusDays;
module.exports.DeltaDays = DeltaDays;
module.exports.formatDay = formatDay;
module.exports.getDatesBetween = getDatesBetween;
module.exports.today = today;
module.exports.diffBetweenDate = diffBetweenDate;
module.exports.formatDayFromFormat = formatDayFromFormat;