export const monitors = [
	{
        headerName: "Code Unique",
        field: "code",
        sortable:true,
        resizable:true,
        filter: true,
        width: 140
  },
  {
        headerName: "Brand",
        field: "brand",
        sortable:true,
        resizable:true,
        filter: true,
        width: 140
  },
  {
        headerName: "Color",
        field: "color",
        sortable:true,
        resizable:true,
        filter: true,
        width: 140
  },
  {
        headerName: "Type",
        field: "type",
        sortable:true,
        resizable:true,
        filter: true,
        width: 140
  },
  {
        headerName: "Diagonal",
        field: "diagonal",
        sortable:true,
        resizable:true,
        filter: true,
        width: 140
  },
  {
        headerName: "Year",
        field: "year",
        sortable:true,
        resizable:true,
        filter: true,
        width: 140
  }
]