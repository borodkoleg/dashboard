import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import Grid from '@material-ui/core/Grid';

class Marker extends Component {
	constructor(props) {
    super(props)

    this.markerRef = React.createRef()
  }

	componentDidMount() {
		const width = this.markerRef.current.offsetWidth;
		const height = this.markerRef.current.offsetHeight;
		const top = this.markerRef.current.offsetTop;
		const left = this.markerRef.current.offsetLeft;
		this.markerRef.current.style.top = `${top - height}px`; 
		this.markerRef.current.style.left = `${left - width/2}px`;
	}

	render() {
  	const {text, $hover, hoverTitle, active} = this.props;

  	let defaultDisplay = true;

  	if (hoverTitle){
  		defaultDisplay = false;
  	}

  	if (hoverTitle && $hover){
  		defaultDisplay = true;
  	}

  	let zIndex = '0';
  	if (active){
  		zIndex = '1000'
  	}
  	if ($hover && defaultDisplay){
  		zIndex = '2000'
  	}

  	let opacity = '0'
  	if (defaultDisplay){
			opacity = '1'
  	}

  	let background = '#FFFFFF'
  	let color = '#000000'
  	if (active){
  		background = '#079fe4'
  		color = '#FFFFFF'
  	}

		return (
			<div 
				className='map-marker'
				style={{zIndex: zIndex}}
				ref={this.markerRef}
			>
				<div
					className='map-marker-text'
					style={{
						opacity: opacity, 
						background: background,
						color: color
					}}
				>
					{text}
				</div>
				<img
				  src={$hover ? window.location.origin + '/marker2_hover.png': window.location.origin + '/marker2.png'}
				  alt='marker'
				  className='map-marker-img'
				/>
			</div>
		)
	}
}

class ListItem extends Component {
	render() {
		const {obj, className} = this.props
		return(
			<div
        key={obj.index}
        className={className}
        onClick={() => this.props.onClick(obj)}
      >
      	{obj.text}
	    </div>
		)
	}
}

class ActiveMap extends Component {
	state = {
		start: true,
		index: '1',
		center: {},
		markers: []
	}

	map;
	maps;

  static defaultProps = {
  	markers: [
  	{
  		lat: 34.0522,
  		lng: -118.2437,
  		text: 'My Marker',
  		index: 1
  	},
  	{
  		lat: 34.0572,
  		lng: -118.2777,
  		text: 'My Marker 2',
  		index: 2
  	},
  	{
  		lat: 34.0622,
  		lng: -118.2077,
  		text: 'My Marker 3',
  		index: 3
  	},
  	{
  		lat: 34.0722,
  		lng: -118.1877,
  		text: 'My Marker 4',
  		index: 4
  	}
  	],
  	height: '300px',
  	width: '100%',
    centerDefault: {
      lat: 34.0522,
      lng: -118.2437
    },
    zoom: 11
  };

  static getDerivedStateFromProps(props, state) {
    if (state.start) {
      return {
        center: props.centerDefault,
        markers: props.markers,
        start: false
      };
    }
    return null;
  }

  allMarkers(array){
  	let items = [];

  	array.forEach((obj) => {
  		items.push(
  			<Marker
          lat={obj.lat}
          lng={obj.lng}
          text={obj.text}
          hoverTitle={obj.hoverTitle}
          key={obj.index}
          active={this.state.index.toString() === obj.index.toString()}
        />
  		)
  	})

  	return items;
  }

  listItemClick(obj){
  	let newObj = {}
  	newObj.lat = obj.lat
  	newObj.lng = obj.lng
  	this.setState({
  		center: newObj,
  		index: obj.index.toString()
  	})
  }

  listCreate(array){
  	let indexState = this.state.index;
  	let result = [];

  	array.forEach((obj) => {
  		if (obj.index.toString() === indexState){
  			result.push(
  				<ListItem
  					key={obj.index}
  					obj={obj}
  					className='map-list-item map-list-item-active'
  					onClick={() => this.listItemClick(obj)}
  				/>
  			)
  		} else {
  			result.push(
	  			<ListItem
  					key={obj.index}
  					obj={obj}
  					className='map-list-item'
  					onClick={() => this.listItemClick(obj)}
  				/>
  			)
  		}
  	});

  	return result;
  }

  markerClick = (e) => {
  	const markers = this.props.markers;
  	let center = {};

  	markers.forEach((obj) => {
  		if (obj.index.toString() === e.toString()){
  			center = {
  				lat: obj.lat,
  				lng: obj.lng
  			}
  		}
  	});

  	this.setState({
  		index: e.toString(),
  		center: center
  	})
  }

  onGoogleApiLoaded = (map, maps) => {
  	this.maps = maps;
  	this.map = map;
  }

  mapChange = (obj) => {
  	if (!this.map || !this.maps){
  		return;
  	}
  	const markers = this.props.markers;
  	//console.log('ne.lat=>' + obj.bounds.ne.lat);
		//console.log('sw.lat=>' + obj.bounds.sw.lat);

		//console.log('sw.lng=>' + obj.bounds.sw.lng);
		//console.log('ne.lng=>' + obj.bounds.ne.lng);

		let foundMarkers = [];

		markers.forEach((marker) => {
			//console.log('lng=>' + marker.lng);
    	//console.log('lat=>' + marker.lat);
	    if (
  			marker.lng > obj.bounds.sw.lng &&
  			marker.lng < obj.bounds.ne.lng &&
	      marker.lat > obj.bounds.sw.lat && 
	      marker.lat < obj.bounds.ne.lat
	    ) {
	      foundMarkers.push(marker)
	    }
	  });

		this.setState({
			markers: foundMarkers
		});
  }

  showAllMarkers = () => {
  	const markers = this.props.markers;
  	const bounds = new this.maps.LatLngBounds();
	  markers.forEach((place) => {
	    bounds.extend(new this.maps.LatLng(
	      place.lat,
	      place.lng
	    ));
	  });
	  this.map.fitBounds(bounds);
  }

  render() {
  	const {center, markers} = this.state;
  	const {height, zoom, centerDefault} = this.props;

  	const markersJsx = this.allMarkers(markers);

  	const markerList = this.listCreate(markers);

    return (
      <Grid container>
  			<Grid item xs={12} sm={12} md={8} lg={8}>

    			<div style={{height: height}}>
		        <GoogleMapReact
		        	yesIWantToUseGoogleMapApiInternals
		          bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAP_KEY, language: 'en' }}
		          defaultCenter={centerDefault}
		          center={center}
		          defaultZoom={zoom}
		          hoverDistance={12}
		          onChildClick={this.markerClick}
		          onChange={this.mapChange} //показывает границу
		          //onBoundsChange={this.Test} //показывает точку
		          onGoogleApiLoaded={({ map, maps }) => this.onGoogleApiLoaded(map, maps)}
		        >
		          {markersJsx}
		        </GoogleMapReact>
	      	</div>

      	</Grid>
      	<Grid item xs={12} sm={12} md={4} lg={4}>
      		<div 
      			className='map-list-items-all-show'
      			onClick={this.showAllMarkers}
      		>
      			Show All Markers
      		</div>
      		{markerList}
      	</Grid>
      </Grid>	
    );
  }
}

export default ActiveMap;