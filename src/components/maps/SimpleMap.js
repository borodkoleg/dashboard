import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

class Marker extends Component {
	constructor(props) {
    super(props)

    this.markerRef = React.createRef()
  }

	componentDidMount() {
		const width = this.markerRef.current.offsetWidth;
		const height = this.markerRef.current.offsetHeight;
		const top = this.markerRef.current.offsetTop;
		const left = this.markerRef.current.offsetLeft;
		this.markerRef.current.style.top = `${top - height}px`; 
		this.markerRef.current.style.left = `${left - width/2}px`;
	}

	render() {
  	const {text, $hover, hoverTitle } = this.props;

  	let defaultDisplay = true;

  	if (hoverTitle){
  		defaultDisplay = false;
  	}

  	if (hoverTitle && $hover){
  		defaultDisplay = true;
  	}

		return (
			<div 
				className='map-marker'
				style={($hover && defaultDisplay) ? {zIndex: '1000'} : {}}
				ref={this.markerRef}
			>
				<div
					className='map-marker-text'
					style={defaultDisplay ? {opacity: '1'} : {opacity: '0'}}
				>
					{text}
				</div>
				<img
				  src={window.location.origin + '/marker.png'}
				  className='map-marker-img'
				  alt='marker'
				/>
			</div>
		)
	}
}

class SimpleMap extends Component {
  static defaultProps = {
  	markers: [
  	{
  		lat: 34.0522,
  		lng: -118.2437,
  		text: 'My Marker'
  	},
  	{
  		lat: 34.0572,
  		lng: -118.2477,
  		text: 'My Marker 1'
  	},
  	{
  		lat: 34.0622,
  		lng: -118.2077,
  		text: 'My Marker 2',
  		hoverTitle: true
  	}
  	],
  	height: '300px',
  	width: '100%',
    center: {
      lat: 34.0522,
      lng: -118.2437
    },
    zoom: 11
  };

  allMarkers(array){
  	const items = []

  	array.forEach((obj, index) => {
  		items.push(
  			<Marker
          lat={obj.lat}
          lng={obj.lng}
          text={obj.text}
          hoverTitle={obj.hoverTitle}
          key={index}
          //title="Alex"
          //icon="https://reactjs.org/favicon.ico"
          //onClick={() => alert("o")}
        />
  		)
  	})

  	return items;
  }

  render() {
  	const {height, width, center, zoom, markers} = this.props;

  	const markersJsx = this.allMarkers(markers);

    return (
      <div style={{ height: height, width: width }}>
        <GoogleMapReact
        	yesIWantToUseGoogleMapApiInternals
          bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAP_KEY, language: 'en' }}
          defaultCenter={center}
          defaultZoom={zoom}
          hoverDistance={10}
        >
          {markersJsx}
        </GoogleMapReact>
      </div>
    );
  }
}

export default SimpleMap;