import React, { Component } from 'react';
import GoogleMapReact from "google-map-react";

const GoogleApiPolygon = ({center, zoom, polygons}) => {
	const polygonsCopy = polygons;

	const onGoogleApiLoaded = (map, maps, polygons) => {
		//create polygon
		polygons.forEach((obj) => {
			let draw = new maps.Polygon(obj);
	  	draw.setMap(map);

	  	const infoWindow = new maps.InfoWindow();
    	maps.event.addListener(draw, 'mouseover', function (e) {
	      infoWindow.setContent(obj.label);
	      const latLng = e.latLng;
	      infoWindow.setPosition(latLng);
	      infoWindow.open(map);
    	});

    	maps.event.addListener(draw, 'mouseout', function (e){
    		infoWindow.close(map);
    	});

    	maps.event.addListener(draw, 'click', function (clickEvent) {
	  	//думаю здесь можно поменять redux переменную
	  	});
		});
	};

  return <GoogleMapReact
    defaultCenter={center}
    defaultZoom={zoom}
    yesIWantToUseGoogleMapApiInternals
    onGoogleApiLoaded={({ map, maps, polygons }) => onGoogleApiLoaded(map, maps, polygonsCopy)}
  />
};

class Polygon extends Component {

	static defaultProps = {
		height: '300px',
		width: '100%',
		center: {
      lat: 34.0522,
      lng: -118.2437
    },
    zoom: 11,
    polygons: [
	    {
	    	paths: [
		    	{ lat: 34.0522, lng: -118.2437 },
		    	{ lat: 34, lng: -118.2437 },
		    	{ lat: 33.9999, lng: -118.3437 }
	    	],
	    	strokeColor: "#FF0000",
		    strokeOpacity: 0.8,
		    strokeWeight: 2,
		    fillColor: "#FF0000",
		    fillOpacity: 0.35,
		    label: 'Test label'
	    },
	    {
	    	paths: [
		    	{ lat: 34.0622, lng: -118.2537 },
		    	{ lat: 33.9950, lng: -118.4437 },
		    	{ lat: 34.1010, lng: -118.4000 }
	    	],
	    	strokeColor: "#FF0000",
		    strokeOpacity: 0.8,
		    strokeWeight: 2,
		    fillColor: "#FF0000",
		    fillOpacity: 0.35,
		    label: 'Test label 2'
	    }
    ]
	}

	render() {
		const {height, width, center, zoom, polygons} = this.props

		return (
      <div style={{ height: height, width: width }}>
        <GoogleApiPolygon 
        	center={center}
        	zoom={zoom}
        	polygons={polygons}
        />
      </div>
	  );
	}

}

export default Polygon;