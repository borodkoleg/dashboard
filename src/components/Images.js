import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
//import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Notify from './part/Notify';
import DialogConfirm from './part/DialogConfirm';

const {Axios, checkToken} = require('./config/axios');
const valid = require('./data/validation');

const { basis } = require('./data/themes');
const theme1 = createMuiTheme(basis);
const dirPublic = window.location.origin;

class Images extends Component {
  state = {
    rowData: [],
    inputsValue: {
    	image_title: '',
    	valid_file: false
    },
    fileRemove: '',
    indexRemove: '',
    dialogConfirmOpen: false
  };
  
  componentDidMount() {
  	this.filesRead();
  }

  filesRead(){
  	const thisHistory = this.props.history
		Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-images-in-directory' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	    	checkToken(result, thisHistory)

	      if (result['data'] && result['data']['body']){

	      	let allPromises = [];
	      	result['data']['body'].forEach((obj, index) => {
	      		//console.log(obj);
	      		allPromises.push(this.getBase64(obj, index))
	      	});

	      	Promise.all(allPromises).then((array) => {
	      		const rowData = [...this.state.rowData];

	      		array.forEach((obj)=> {
	      			rowData[obj.index] = obj;
	      		})

	      		this.setState({
					  	rowData: rowData,
					  	inputsValue: {
					  		image_title: '',
    						valid_file: false
					  	}
					  })

					  this.refs.uploadFile.value = null;

	      	});
	    	}
	  });
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);

		  if (key === 'password'){
		  	obj[key] = newData;
			} else {
				obj[key] = newData.toUpperCase();
			}

		  return { inputsValue: obj };
		})
  }

  handleSubmit = (event) => {
  	event.preventDefault();

  	const data = new FormData();

  	data.append('title', this.state.inputsValue['image_title']);
  	data.append('file', this.refs.uploadFile.files[0]);

  	const thisHistory = this.props.history

  	Axios({
			method: 'post',
			processData: false,
			contentType: 'multipart/form-data',
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			cache: false,
			url: process.env.REACT_APP_HOST_PORT + '/api/save-image', 
			data: data
		})
    .then((result) => {
    	checkToken(result, thisHistory)

      if (result['data'] && result['data']['body']){
      	this.filesRead();
				this.refs.notifyC.notify('success','Success');
			}
		})
		.catch((err) => {
			if (err.response) {
				if (err.response.status === 403){
					this.setState({
				  	inputsValue: {
				  		image_title: '',
  						valid_file: false
				  	}
				  })

					this.refs.uploadFile.value = null;

					this.refs.notifyC.notify('error', err.response.data);
				}
			}
		})
  }

  fileValid(data){
  	let inputsValue = {...this.state.inputsValue};

  	inputsValue['valid_file'] = data;

  	if (!data){
  		this.refs.uploadFile.value = null;  	
  	}
  	
  	this.setState({
			inputsValue: inputsValue
		})
  }

  readFile = (event, index) => {

  	if (!event.target.files[0]){
  		this.fileValid(false);
  		return;	
  	}

  	if (event.target.files[0]['size'] > 5000000){
  		this.fileValid(false);
			this.refs.notifyC.notify('error','File size must be < 5mb');
  		return;
  	}
  
  	if (
  		event.target.files[0]['type'] !== 'image/jpeg' &&
  		event.target.files[0]['type'] !== 'image/jpg' &&
  		event.target.files[0]['type'] !== 'image/png' &&
  		event.target.files[0]['type'] !== 'image/gif' &&
  		event.target.files[0]['type'] !== 'image/bmp'
  	) {
  		this.refs.notifyC.notify('error','File type must be \'jpeg, png, gif, bmp \'');
  		this.fileValid(false);
  		return;
	  }

	  this.fileValid(true);
	}

	async getBase64(obj, index) {
  	
  	let url = `${dirPublic}/uploads/${obj.file}`;
  	
  	return await Axios.get(url, {
      responseType: 'arraybuffer'
    })
    .then((response) => {
    	let buff = new Buffer(response.data, 'binary').toString('base64');
    	let result = 'data:image/jpeg;base64,' + buff;
    	
  		return {file: result, index: index, id: obj.id, title: obj.file};
    })
	}

	imageRemove = (fileName) => {
		let rowData = this.state.rowData;
		let index = 0;
		rowData.forEach((obj, i) => {
			if (obj.title === fileName){
				index = i;
			}
		});

		this.setState({
			fileRemove: fileName,
			dialogConfirmOpen: true,
			indexRemove: index
		})
	}

	confirmRemoveClick = (data) => {
		if (!data){
			this.setState({
				fileRemove: '',
				dialogConfirmOpen: false,
				indexRemove: ''
			})
			return;
		} 

		const thisHistory = this.props.history

		Axios.post(process.env.REACT_APP_HOST_PORT + '/api/remove-image' , {file: this.state.fileRemove},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
	    })
	    .then((result) => {
	    	checkToken(result, thisHistory)
	    	
	      if (result['data'] && result['data']['body']){
	      	let rowData = [...this.state.rowData];
	      	rowData.splice(this.state.indexRemove, 1);

	      	this.setState({
						fileRemove: '',
						dialogConfirmOpen: false,
						rowData: rowData,
						indexRemove: ''
					})

					this.refs.notifyC.notify('success','Success');

	    	}
	  	});
	}

  render() {
    //const {} = this.props;
    const {
    	rowData, 
    	inputsValue, 
    	fileRemove, 
    	dialogConfirmOpen
    } = this.state;

    return (
    	<div>
    		<div style={{
    			display: 'inline-block',
			    padding: '20px 14px',
			    border: '1px dashed silver',
			    background: 'rgb(250, 250, 251)'
    		}}>
    		<form ref='form' onSubmit={this.handleSubmit}>
	    		<MuiThemeProvider theme={theme1}>
		    		<TextField
			        placeholder='Image Title'
			        variant='outlined'
			        label='Image Title'
			        value={inputsValue['image_title']}
			        name="image_title"
			        helperText={!valid.isEngString(inputsValue['image_title'], true) ? 'Incorrect Image Title' : ''}
			        onChange={(value, key) => this.inputChange(value, 'image_title')}
			        error={!valid.isEngString(inputsValue['image_title'], true)}
				    />
				    <br/>
				    <input ref="uploadFile" type="file" accept="image/*"
		        	onChange={(event)=> { 
		             this.readFile(event, 0) 
		        	}}
						/>
						<br/>
						<br/>
		    		<Button 
				    	variant="contained" 
				    	size="small"
				    	style={{
		      			marginRight: '20px'
		      		}}
		      		type="submit"
		      		disabled={
		      			(!valid.isEngString(inputsValue['image_title'], true) || 
		      			!inputsValue['valid_file']) 
		      			? true : false 
		      		}
						> 
		    			Add Image
		   			</Button>
	   			</MuiThemeProvider>
   			</form>
   			</div>
   			<br/>
   			<br/>

   			{rowData.length>0 && rowData.map((obj) => {
   				return <div key={obj.id} style={{
   					display: 'inline-block',
   					padding: '10px',
   					border: '1px dashed silver',
			    	background: 'rgb(250, 250, 251)'
   				}}>
   				
   				<div>{`${obj.title}`}</div>
   					<img src={`${obj.file}`} alt={`${obj.title}`} style={{height: '100px'}} />
   					<br/>
   					<div 
   						onClick={(file) => this.imageRemove(obj.title)}
   						style={{
   							cursor: 'pointer', 
   							display: 'inline-block', 
   							textDecoration: 'underline'
   						}}>
   						remove
   					</div>
   				</div>
   			})}

   			<DialogConfirm
    			title = 'Do you want remove this item?'
    			text = {fileRemove}
    			open = {dialogConfirmOpen}
    			click = {(data) => this.confirmRemoveClick(data)}
    		/>

	      <Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default Images;