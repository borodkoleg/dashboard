import React, {Component} from 'react';
//import TextField from '@material-ui/core/TextField';
//import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
//import Select from '../../part/Select';
import { g_getCall } from '../../../actions/a_callTracking';
import CallAjaxTable from './callTracking/CallAjaxTable'
import Filters from './reportingApi/Filters'
import { g_getFilters } from '../../../actions/a_reportingAPI';

//const Axios = require('axios');


class CallTracking extends Component {
  state = {

  };

  updateCalls(){
  	this.props.g_getCall(this.history, this.props.st_callTracking, this.props.st_storeFilter)
  }
  
  componentDidMount() {
  	this.props.g_getFilters(this.history, this.props.globalState.storeFilter)
  	this.updateCalls()
  }

  changeCallsTable = (data) => {
		this.props.g_st_callChange_calls(['currentPage', data])
		this.updateCalls()
  }

  render() {
    //const {loading} = this.props;
    //const {} = this.state;
    console.log('globalState', this.props.st_callTracking);
    return (
    	<div>
    		<Grid container style={{marginTop: '40px'}}>
	    		<Grid item xs={12} sm={8} md={8} className='padding-for-blocks'>
	    			<CallAjaxTable
	    				title='Calls'
							columns={[
								{ name: 'id', title: 'Id' },
							  { name: 'source', title: 'Source' },
							  { name: 'duration', title: 'Duration' },
								{ name: 'called_at', title: 'Called at' }
							]}
							columnExtensions={[
								{ columnName: 'id', width: 120 },
								// { columnName: 'source', width: 100 },
								{ columnName: 'duration', width: 110 },
								{ columnName: 'called_at', width: 170 }
							]}
							rows={this.props.st_callTracking.calls.rows}
							currentPage={this.props.st_callTracking.calls.currentPage}
							totalCount={this.props.st_callTracking.calls.totalCount}
							change={this.changeCallsTable}
							loading={this.props.st_storeFilter.loading.calls}
							pageSize={this.props.st_callTracking.calls.pageSize}
	    			/>
					</Grid>
					<Grid item xs={12} sm={4} md={4} className='padding-for-blocks'>
						<Filters
							history={this.props.history}
							//disabled = {}
							loading={this.props.st_storeFilter.loading.filters}
						/>
					</Grid>
				</Grid>
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    st_callTracking: state.st_callTracking,
    st_storeFilter: state.storeFilter
  }),
  dispatch => ({
    g_getCall: (history, store) => {
  		dispatch(g_getCall(history, store))
    },
    g_st_callTracking: (data) => {
    	dispatch({type: 'CALL_CHANGE_ALL', payload: data})
    },
		g_st_callChange_calls: (data) => {
    	dispatch({type: 'CALL_CHANGE_CALLS', payload: data})
    },
    g_getFilters: (history, storeFilter) => {
    	dispatch(g_getFilters(history, storeFilter))
    },
  })
)(CallTracking);