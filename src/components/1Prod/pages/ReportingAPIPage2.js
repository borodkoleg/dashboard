import React, {Component} from 'react';
//import TextField from '@material-ui/core/TextField';
//import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Select from '../../part/Select';
import TableSessions from '../../part/TableSessions';
import ChartLine from '../../part/ChartLine';
import MultiSelect from '../../part/MultiSelect';
import SessionsBySources from './reportingApi/TableCheckboxPagination'
import SessionsByUserPie from './reportingApi/SessionsByUserPie'
import Button from '@material-ui/core/Button';

const {Axios, checkToken} = require('../../config/axios');

class ReportingAPI extends Component {
  state = {
    dateFrom: new Date(),
    dateTo: new Date()
  };
  
  componentDidMount() {
  	const this_ = this;
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-reporting' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, this_.props.history)
      if (result && result['data'] && result['data']['body']){
      }
    })
    .catch(err => {
      
    });
  }

  selectAccountName = (event) => {
  	console.log(event.target.textContent)
  }

  selectPropertyName = (event) => {
  	console.log(event.target.textContent)
  }

  selectViewName = (event) => {
  	console.log(event.target.textContent)
  }

  changeFrom = (date) => {
  	this.setState({
  		dateFrom: date
  	})
  }

  changeTo = (date) => {
  	this.setState({
  		dateTo: date
  	})
  }

  multiSelect = (index, data) => {
  	console.log(index)
  	console.log(data)
  }

  sessionsTableChange = (data) => {
  	console.log(data)
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div className='reporting-page'>
    		<Grid container>
	    		<Grid item xs={12} sm={12} md={12} className='padding-for-blocks'>
		    		<img
						  src={window.location.origin + '/logo.png'}
						  className='reporting-logo'
						  alt='logo'
						/>

						<div className='reporting-title'>
							SITE RELAUNCH MODE
						</div>
					</Grid>

					<Grid item xs={12} sm={12} md={3} lg={3} className='padding-for-blocks'>
						<Select
							placeholder='Account Name'
							onChange={this.selectAccountName}
							data={[
								{title: '1', value: 10},
								{title: '2', value: 20},
								{title: '3', value: 30}
							]}
						/>

						</Grid>
						<Grid item xs={12} sm={12} md={3} lg={3} className='padding-for-blocks'>

						<Select
							placeholder='Property Name'
							onChange={this.selectPropertyName}
							data={[
								{title: '1', value: 10},
								{title: '2', value: 20},
								{title: '3', value: 30}
							]}
						/>

						</Grid>
						<Grid item xs={12} sm={12} md={3} lg={3} className='padding-for-blocks'>

						<Select
							placeholder='View Name'
							onChange={this.selectViewName}
							data={[
								{title: 'view1', value: 1},
								{title: 'view2', value: 2},
								{title: 'view3', value: 3}
							]}
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={3} lg={3} className='padding-for-blocks'>
						<Button
							variant="contained" 
   		    		size="medium"	  
   		    		color="primary"
						>
							Relaunch Options
						</Button>
					</Grid>

					<Grid item xs={12} sm={12} md={12}>
						<Grid container>
							<Grid item xs={12} sm={12} md={3} lg={3} className='padding-for-blocks'>
								<TableSessions/>
							</Grid>
							<Grid item xs={12} sm={12} md={5} lg={5} className='padding-for-blocks'>
								<ChartLine
									YFormat={(value) => `${value/1000}K`}
									YDomain={[
										4000, 9000
									]}
								/>
							</Grid>
							<Grid className='padding-for-blocks' item xs={12} sm={12} md={4} lg={4}>

								<div style={{
									fontWeight: '700',
									marginBottom: '20px'
								}}>FILTERS</div>

								<MultiSelect
									onChange={(value) => this.multiSelect(1, value)}
									placeholder='Source'
									style={{marginTop: '10px'}}
									data={[
										{key: 1, value: 'google', number: '195.6K'},
										{key: 2, value: '(direct)', number: '16.4K'},
										{key: 3, value: 'bing', number: '8.9K'}
									]}
								/>

								<MultiSelect
									onChange={(value) => this.multiSelect(2, value)}
									placeholder='Medium'
									style={{marginTop: '10px'}}
									data={[
										{key: 1, value: 'organic', number: '195.6K'},
										{key: 2, value: '(none)', number: '16.4K'},
										{key: 3, value: 'referral', number: '8.9K'}
									]}
								/>

								<MultiSelect
									onChange={(value) => this.multiSelect(3, value)}
									placeholder='Device Category'
									style={{marginTop: '10px'}}
									data={[
										{key: 1, value: 'mobile', number: '195.6K'},
										{key: 2, value: 'desctop', number: '16.4K'},
										{key: 3, value: 'tablet', number: '8.9K'}
									]}
								/>

								<MultiSelect
									onChange={(value) => this.multiSelect(4, value)}
									placeholder='Default Channel Grouping'
									style={{marginTop: '10px'}}
									data={[
										{key: 1, value: 'Organic Search', number: '195.6K'},
										{key: 2, value: 'Direct', number: '16.4K'},
										{key: 3, value: 'Social', number: '8.9K'}
									]}
								/>

								<MultiSelect
									onChange={(value) => this.multiSelect(5, value)}
									placeholder='Country'
									style={{marginTop: '10px'}}
									data={[
										{key: 1, value: 'United States', number: '195.6K'},
										{key: 2, value: 'United Kingdom', number: '16.4K'},
										{key: 3, value: 'Canada', number: '8.9K'}
									]}
								/>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} sm={12} md={4} lg={4} className='padding-for-blocks'>
						<ChartLine
							title='Goal Conversion Rate'
							data={[
								{
							    Name: 'Sep 1', Value: 0.5
							  },
							  {
							    Name: 'Sep 6', Value: 0.8
							  },
							  {
							    Name: 'Sep 11', Value: 0.6
							  },
							  {
							    Name: 'Sep 16', Value: 1.1
							  },
							  {
							    Name: 'Sep 21', Value: 0.8
							  },
							  {
							    Name: 'Sep 26', Value: 1.3
							  },
							]}
							YDomain={[
								0, 1.5
							]}
							YFormat={(value) => `${value}%`}
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={4} lg={4} className='padding-for-blocks'>
						<SessionsBySources
							change={this.sessionsTableChange}
							title='Sessions by Sources'
							columns={[
								{ name: 'id', title: 'Id' },
							  { name: 'source', title: 'Source' },
							  { name: 'sessions', title: 'Sessions' }
							]}
							rows={[
							  {
							  	id: 1,
							  	source: 'google', 
							  	sessions: '195,566'
							  },
							  {
							  	id: 2,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 3,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 4,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 5,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 6,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 7,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 8,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 9,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 10,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 11,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  },
							  {
							  	id: 12,
							  	source: '(direct)', 
							  	sessions: '16,426'
							  }
							]}
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={4} lg={4} className='padding-for-blocks'>
						<SessionsByUserPie/>
					</Grid>
				</Grid>
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  })
)(ReportingAPI);