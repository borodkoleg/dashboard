import React, {Component} from 'react';
//import TextField from '@material-ui/core/TextField';
//import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
//import Select from '../../part/Select';
import TableSessions from '../../part/TableSessions';
import ChartLine from '../../part/ChartLine';
import SessionsBySources from './reportingApi/TableCheckboxPagination'
import SessionsByUserPie from './reportingApi/SessionsByUserPie'
import { g_updateData, g_getFilters, g_updateDataForSessions, g_updateDataSessionsByUserType } from '../../../actions/a_reportingAPI';
import Filters from './reportingApi/Filters'
//const moment = require('moment');


class ReportingAPI extends Component {
  state = {

  };
  
  componentDidMount() {
  	this.props.g_updateData(this.history, this.props.globalState.storeFilter)
  	this.props.g_getFilters(this.history, this.props.globalState.storeFilter)

  	//https://www.googleapis.com/analytics/v3/management/accounts?access_token=ya29.c.Kl-0B_B7fxvYoITh69CWMsxdd68bCBubw3itBPh84mlfi1T21FOYA1a-SHGxT9X4wq1YYgrtRIpMVM2-_OJRHa_P9fzsAi5SdPYtuK6VUwLxqE-Qh7chJt9yzCUjlUSRJw accaunts

  	//https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties/~all/profiles?access_token=ya29.c.Kl-0B_B7fxvYoITh69CWMsxdd68bCBubw3itBPh84mlfi1T21FOYA1a-SHGxT9X4wq1YYgrtRIpMVM2-_OJRHa_P9fzsAi5SdPYtuK6VUwLxqE-Qh7chJt9yzCUjlUSRJw views

  	//https://www.googleapis.com/analytics/v3/management/accounts/78975985/webproperties?access_token=ya29.c.Kl-0B_B7fxvYoITh69CWMsxdd68bCBubw3itBPh84mlfi1T21FOYA1a-SHGxT9X4wq1YYgrtRIpMVM2-_OJRHa_P9fzsAi5SdPYtuK6VUwLxqE-Qh7chJt9yzCUjlUSRJw

  	// Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-accounts' , {},
   //  	{
   //      headers: {
   //         "Content-Type": "application/json",
   //          "Authorization": `Bearer ${localStorage.getItem('token')}`
   //      },
   //      params: {}
   //  })
   //  .then((result) => {
   //  	console.log('yes');
   //  	console.log(result);
   //  })
   //  .catch(err => {
   //    console.log(err);
   //  });

    //==============
  }

  selectAccountName = (event) => {
  	console.log(event.target.textContent)
  }

  selectPropertyName = (event) => {
  	console.log(event.target.textContent)
  }

  sessionsBySourcesChange = (data) => {
		this.props.g_updateDataForSessions(this.history, this.props.globalState.storeFilter, data)
  }

  sessionsByUserPieClick = (data, index) => {
  	this.props.g_updateDataSessionsByUserType(this.history, this.props.globalState.storeFilter, data)
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;
    //console.log(JSON.stringify(this.props.storeFilter, null, 2))
  //   let showLoading = false;
  //   Object.values(loading).forEach(function(value) {
		// 	if (value){
		// 		showLoading = true;
		// 		return;
		// 	}
		// });
		// {showLoading ? <div className="loader-block">
  //   				<div className="loader">Loading...</div>
  //   			</div> : ''
		// }

    return (
    	<div className='reporting-page'>
    		<Grid container>
	    		<Grid item xs={12} sm={12} md={12} className='padding-for-blocks'>
		    		<img
						  src={window.location.origin + '/logo.png'}
						  className='reporting-logo'
						  alt='logo'
						/>

						<div className='reporting-title'>
							MAIN DASHBOARD
						</div>
					</Grid>

					<Grid className='padding-for-blocks' item xs={12} sm={12} md={4} lg={4}>
						{/*<Select
							placeholder='Account Name'
							onChange={this.selectAccountName}
							data={[
								{title: '1', value: 10},
								{title: '2', value: 20},
								{title: '3', value: 30}
							]}
						/> */}
					</Grid>
					<Grid className='padding-for-blocks' item xs={12} sm={12} md={4} lg={4}>

						{/*<Select
							placeholder='Property Name'
							onChange={this.selectPropertyName}
							data={[
								{title: '1', value: 10},
								{title: '2', value: 20},
								{title: '3', value: 30}
							]}
						/> */}
					</Grid>
					<Grid item xs={12} sm={12} md={12}>
						<Grid container>
							<Grid item xs={12} sm={12} md={3} lg={3} className='padding-for-blocks'>
								<TableSessions
									data={[
										{
											title: 'SESSIONS', 
											value: this.props.tableSessions.Sessions,
											increase: this.props.tableSessions.SessionsDelta > 0 ? true : false,
											percent: (this.props.tableSessions.SessionsDelta).toFixed(2)
										},
										{
											title: 'GOAL COMPLETIONS', 
											value: this.props.tableSessions.Goal, 
											increase: this.props.tableSessions.GoalDelta > 0 ? true : false,
											percent: (this.props.tableSessions.GoalDelta).toFixed(2)
										},
										{
											title: 'BOUNCES', 
											value: this.props.tableSessions.Bounces, 
											increase: this.props.tableSessions.BouncesDelta > 0 ? true : false,
											percent: (this.props.tableSessions.BouncesDelta).toFixed(2)
										},
										{
											title: 'AVG. TIME ON SITE', 
											value: Number(this.props.tableSessions.Avg).toFixed(2),
											increase: this.props.tableSessions.AvgDelta > 0 ? true : false,
											percent: (this.props.tableSessions.AvgDelta).toFixed(2)
										}
									]}
									loading={this.props.storeFilter.loading.tableSessions}
								/>
							</Grid>
							<Grid item xs={12} sm={12} md={5} lg={5} className='padding-for-blocks'>
								<ChartLine
									// YFormat={(value) => `${value/1000}K`}
									// YDomain={[
									// 	4000, 9000
									// ]}
									data={this.props.storeFilter.sessionsChart}
									loading={this.props.storeFilter.loading.sessionsChart}
								/>
							</Grid>
							<Grid className='padding-for-blocks' item xs={12} sm={12} md={4} lg={4}>
								<Filters
									history = {this.props.history}
									disabled = {this.props.storeFilter.filterStringBackend.sessionsBySourcesF !== ''}
									loading={this.props.storeFilter.loading.filters}
								/>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} sm={12} md={4} lg={4} className='padding-for-blocks'>
						<ChartLine
							title='Goal Conversion Rate'
							data={this.props.storeFilter.goalConversionRate}
							YFormat={(value) => `${value}%`}
							tooltipFormat={(value, name, props) => {
					  		return `${value}%`
					  	}}
					  	loading={this.props.storeFilter.loading.goalConversionChart}
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={4} lg={4} className='padding-for-blocks'>
						<SessionsBySources
							title='Sessions by Sources'
							columns={[
								{ name: 'id', title: 'Id' },
							  { name: 'source', title: 'Source' },
							  { name: 'sessions', title: 'Sessions' }
							]}
							change={this.sessionsBySourcesChange}
							rows={this.props.storeFilter.sessionsBySources}
							loading={this.props.storeFilter.loading.sessionsBySources}
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={4} lg={4} className='padding-for-blocks'>
						<SessionsByUserPie
							select={(this.props.storeFilter.filterStringBackend.userType).substr(13)}
							data={this.props.storeFilter.sessionsByUserType}
							click={(a, b) => this.sessionsByUserPieClick(a, b)}
							loading={this.props.storeFilter.loading.sessionsByUserType}
						/>
					</Grid>
				</Grid>
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    tableSessions: state.storeFilter.TableSessions,
    s_filters: state.storeFilter.filtersList,
    storeFilter: state.storeFilter,
    s_filtersListSelect: state.storeFilter.filtersListSelect,
    loading: state.storeFilter.loading
  }),
  dispatch => ({
    g_filterChangeAllPromice: async (data) => {
  		dispatch({ type: 'FILTER_CHANGE_ALL', payload: data })
    },
    g_filterChangeAll: (data) => {
  		dispatch({ type: 'FILTER_CHANGE_ALL', payload: data })
    },
    g_updateData: (history, storeFilter, storeFilterSelect) => {
    	dispatch(g_updateData(history, storeFilter, storeFilterSelect))
    },
    g_getFilters: (history, storeFilter, storeFilterSelect) => {
    	dispatch(g_getFilters(history, storeFilter, storeFilterSelect))
    },
    g_updateDataForSessions: (history, storeFilter, storeFilterSelect, data) => {
    	dispatch(g_updateDataForSessions(history, storeFilter, storeFilterSelect,data))
    },
    g_updateDataSessionsByUserType: (history, storeFilter, storeFilterSelect, data) => {
    	dispatch(g_updateDataSessionsByUserType(history, storeFilter, storeFilterSelect, data))
    }
  })
)(ReportingAPI);