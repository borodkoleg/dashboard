import React, { useState } from 'react';
//import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { 
	SortingState,
  IntegratedSorting,
  PagingState,
  CustomPaging,
  SelectionState,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  PagingPanel,
  TableSelection
} from '@devexpress/dx-react-grid-material-ui';

const Root = props => <Grid.Root {...props} style={{ height: '100%' }} />;

export default ({
	columns, 
	rows, 
	title, 
	loading,
	currentPage, //!
	change, //
	totalCount,
	pageSize,
	columnExtensions //
}) => {

	const [selection, setSelection] = useState(
		[]
	);
	//const pageSize = 10;

	const onSelectionChange = (data) => {
		setSelection(data);
	}

	const setCurrentPage = (data) => {
		change(data + 1)
	}

  return (
  	<div className={loading ? 'loading' : ''}>
  		<div style={{
  			fontWeight: '700',
  			fontSize: '15px',
  			marginBottom: '10px'
  		}}>
  			{title}
  		</div>
    <Paper style={{ height: '300px' }} className='table-source-reporting-api'>
      <Grid
        rows={rows}
        columns={columns}
        rootComponent={Root}
      >
      	<SelectionState
          selection={selection}
          onSelectionChange={onSelectionChange}
        />
      	<PagingState
          currentPage={currentPage - 1}
          onCurrentPageChange={setCurrentPage}
          pageSize={pageSize}
        />
        <CustomPaging
          totalCount={totalCount}
        />
      	<SortingState/>
      	<IntegratedSorting />
        <VirtualTable
          height="auto"
          columnExtensions={columnExtensions}
        />
        <TableHeaderRow showSortingControls/>
        <TableSelection
          selectByRowClick
          highlightRow
          showSelectionColumn={false}
        />
        <PagingPanel />
      </Grid>
    </Paper>
    </div>
  );
};