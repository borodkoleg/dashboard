import React, {useState, useEffect} from 'react';
import Paper from '@material-ui/core/Paper';
import MultiSelect from '../../../part/MultiSelect';
import Button from '@material-ui/core/Button';

import { 
	SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging,
  //SelectionState
} from '@devexpress/dx-react-grid';
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  PagingPanel,
  //TableSelection
} from '@devexpress/dx-react-grid-material-ui';

//const {formatDayFromFormat} = require('../../../data/date.js')

const Root = props => <Grid.Root {...props} style={{ height: '100%' }} />;

export default ({columns, rows, title, loading, columnExtensions}) => {

	// const [selection, setSelection] = useState(
	// 	//[...Array(rows.length)].map((_,i) => i)
	// 	[]
	// );

	// const onSelectionChange = (data) => {
	// 	setSelection(data);
	// }

	const [rowsFilter, setRowsFilter] = useState(
		[]
	);

	const [rowsAndTrNumber, setRowsAndTrNumber] = useState(
		[]
	);

	const [trackingNumbers, setTrackingNumbers] = useState(
		[]
	);

	const [selectTrN, setSelectTrN] = useState(
		[]
	);	

	const multiSelect = (data) => {
		setSelectTrN(data)
	}

	const btnClick = () => {
		let filterRow = []

		selectTrN.forEach((el) => {
			rowsFilter.forEach((rEl) => {
				if (el.value === rEl.tracking_number){
					filterRow.push(rEl)
				}
			})
		})

		setRowsAndTrNumber(filterRow)
	}

	useEffect(() => {
		const rowsResult = () => {
			let rowsDuration = []
			let trackingNumbersObj = {}
			let trackingNumbers = []
			let count = 1

			rows.forEach((el) => {
	    	//el.called_at = formatDayFromFormat(el.called_at, 'YYYY-MM-DD hh:mm A', 'MM-DD-YYYY hh:mm A')
	    	if (parseInt(el.duration, 10) > 119){
	    		const date = el.called_at
	    		if (date[2]!=='-'){
	    			el.called_at = `${date.slice(5, 10)}-${date.slice(0, 4)} ${date.slice(11, 16)} ${date.slice(17)}`
	    		}
	    			rowsDuration.push(el);
	    			if (!trackingNumbersObj[el.tracking_number]){
	    				trackingNumbersObj[el.tracking_number] = true
	    				trackingNumbers.push({
	    					key: count,
	    					value: el.tracking_number
	    				})
	    				count += 1
	    			}
	    	}
	    })
	    return [rowsDuration, trackingNumbers]
		}

		const rowsF = rowsResult()
		setRowsFilter(rowsF[0])
		setRowsAndTrNumber(rowsF[0])
		setTrackingNumbers(rowsF[1])
		setSelectTrN(rowsF[1])

		//console.log('rowsF[1]', rowsF[1])
		//console.log('rowsF[0]', rowsF[0])
	},[rows])

  return (
  	<div style={{height: '80%'}} className={loading ? 'loading' : ''}>
  		<div
  		className={'drag-me'}
  		style={{
  			fontWeight: '700',
  			fontSize: '15px',
  			marginBottom: '10px'
  		}}>
  			{title}
  		</div>
  		<div className={'calls-filter-block'}>
	  		<MultiSelect
					onChange={(value) => multiSelect(value)}
					placeholder='Tracking numbers'
					data={trackingNumbers}
					className={'multi-select-picky multi-select-calls'}
					valuesSelect={selectTrN}
					//disabled={disabled}
				/> 
				<Button 
	    		variant="contained" 
	    		size="small"	 
	    		//style={{marginTop: '10px'}}
	    		color="primary"
	    		onClick={btnClick}
	    		className={'multi-select-calls-button'}
			  > 
	    		Filter
	  		</Button>
  		</div>
    <Paper style={{ height: '100%' }} className='table-source-reporting-api'>
      <Grid
        rows={rowsAndTrNumber}
        columns={columns}
        rootComponent={Root}
      >
      	{/*<SelectionState
          selection={selection}
          onSelectionChange={onSelectionChange}
        />*/}
      	<PagingState
          defaultCurrentPage={0}
          pageSize={20}
        />
        <IntegratedPaging />
      	<SortingState/>
      	<IntegratedSorting />
        <VirtualTable
          height="auto"
          columnExtensions={columnExtensions}
        />
        <TableHeaderRow showSortingControls/>
        {/*<TableSelection
          selectByRowClick
          highlightRow
          showSelectionColumn={false}
        />*/}
        <PagingPanel />
      </Grid>
    </Paper>
    </div>
  );
};