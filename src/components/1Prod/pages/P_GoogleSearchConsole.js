import React, {Component} from 'react';
import { connect } from 'react-redux';
import TableComponentOneSelect from '../../part/TableComponentOneSelect';
import TableComponent from "../../part/TableComponent";

import Grid from '@material-ui/core/Grid';
import FiltersGoogleSearch from "./reportingApi/FiltersGoogleSearch";

//actions
//const g_getSites = require("../../../actions/googleSearch/getSites");
//const g_getQueryImpressions = require("../../../actions/googleSearch/getQueryImpressions");
import g_getSites from "../../../actions/googleSearch/getSites";
import g_getQueryImpressions from "../../../actions/googleSearch/getQueryImpressions";
import g_getLandingPages from "../../../actions/googleSearch/getLandingPages"

//const {Axios, checkToken} = require('../../../components/config/axios');


class P_GoogleSearchConsole extends Component {

  componentDidMount() {
    const history = this.props.history;

    //state is full
    if (this.props.st_googleSearch &&
      this.props.st_googleSearch.sites &&
      this.props.st_googleSearch.sites.length > 0){
      return;
    }

    // Error
    // this.props.g_getSites(history)().then(
    //   this.props.g_getQueryImpressions(history)
    // )
    this.props.g_general(history);

  }

  siteChange = (data) => {
    console.log(data);
  }

  filterClick = () => {
    const history = this.props.history;
    this.props.g_general(history);
  }

  render() {
    const {st_googleSearch, history} = this.props;
    //console.log('dateFrom',st_googleSearch.dateFrom);
    //console.log('dateTo',st_googleSearch.dateTo);

    return (
      <Grid container style={{marginTop: '20px'}}>
        <Grid item xs={12} sm={6} md={6} className='padding-for-blocks'>
          <TableComponentOneSelect
            title='Sites'
            columns={[
              { name: 'siteUrl', title: 'Site Url' },
              { name: 'permissionLevel', title: 'Permission' },
            ]}
            columnExtensions={[
              { columnName: 'permissionLevel', width: 120 }
            ]}
            rows={st_googleSearch.sites}
            history={history}
            loading={st_googleSearch.loading.sites}
            select={st_googleSearch.currentSiteIndex}
            onSelect={(data) => this.siteChange(data)}
            styleP={{height:'200px'}}
          />
        </Grid>

        <Grid item xs={12} sm={4} md={4} className='padding-for-blocks'>
          <FiltersGoogleSearch
            history={this.props.history}
            disabled={false}
            loading={false}
            click={this.filterClick}
          />
        </Grid>
        <Grid item xs={12} sm={11} md={11} className='padding-for-blocks'>
          <TableComponent
            title='Queries'
            columns={[
              { name: 'queries', title: 'Queries' },
              { name: 'impressions', title: 'Impressions' },
              { name: 'clicks', title: 'Clicks' },
              { name: 'ctr', title: 'Ctr' },
              { name: 'position', title: 'Position' },
            ]}
            columnExtensions={[
              { columnName: 'impressions', width: 130 },
              { columnName: 'clicks', width: 130 },
              { columnName: 'ctr', width: 130 },
              { columnName: 'position', width: 130 }
            ]}
            rows={st_googleSearch.queryImpressions.map((el) => {
              el.queries = el.keys[0];
              return el;
            })}
            history={history}
            loading={st_googleSearch.loading.queryImpressions}
          />
        </Grid>
        <Grid item xs={12} sm={11} md={11} className='padding-for-blocks'>
          <TableComponent
            title='Landing Pages'
            columns={[
              { name: 'pages', title: 'Pages' },
              { name: 'impressions', title: 'Impressions' },
              { name: 'clicks', title: 'Clicks' },
              { name: 'ctr', title: 'Ctr' },
              { name: 'position', title: 'Position' },
            ]}
            columnExtensions={[
              { columnName: 'impressions', width: 100 },
              { columnName: 'clicks', width: 100 },
              { columnName: 'ctr', width: 80 },
              { columnName: 'position', width: 80 }
            ]}
            rows={st_googleSearch.landingPages.map((el) => {
              el.pages = el.keys[0].replace(st_googleSearch.currentSiteUrl, '/');
              return el;
            })}
            history={history}
            loading={st_googleSearch.loading.landingPages}
          />
        </Grid>
      </Grid>
    )
  }

}

export default connect(
  state => ({
    globalState: state,
    st_googleSearch: state.st_googleSearch
  }),
  dispatch => ({
    g_general: (history) => {
      dispatch(g_getSites(history))().then(() => {
        dispatch(g_getQueryImpressions(history));
        dispatch(g_getLandingPages(history));
      })
    },
    // g_updateGoogleSearch: (data) => {
    //   dispatch({ type: 'GOOGLE_SEARCH_CHANGE_ALL', payload: data })
    // }
  })
)(P_GoogleSearchConsole);
