import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import DateBetween from '../../../part/DateBetween';
import MultiSelect from '../../../part/MultiSelect';
import { g_updateData, g_getFilters, g_beforeFilter } from '../../../../actions/a_reportingAPI';
const {diffBetweenDate, DeltaDays} = require('../../../data/date')


class Filters extends Component {
	static defaultProps = {
    disabled: false,
    loading: false
	}
	
	componentDidMount(){

	}

	changeFrom = (date) => {
  	//const this_ = this;
  	this.props.g_filterChangeAll({
  		dateFrom: date,
  		diffDate: diffBetweenDate(date, this.props.globalState.storeFilter.dateTo)
  	})
    	// this.props.g_updateData(this.history, this.props.globalState.storeFilter)
  }

  changeTo = (date) => {
  	//const this_ = this;
  	this.props.g_filterChangeAll({
  		dateTo: date,
  		diffDate: diffBetweenDate(this.props.globalState.storeFilter.dateFrom, date)
  	})
  }

  multiSelect = (index, data) => {
  	let copyObj = {...this.props.s_filtersListSelect}
  	
  	if (index===1){
  		copyObj['source'] = data;
  	}
  	if (index===2){
  		copyObj['medium'] = data;
  	}
  	if (index===3){
  		copyObj['deviceCategory'] = data;
  	}
  	if (index===4){
  		copyObj['channelGrouping'] = data;
  	}
  	if (index===5){
  		copyObj['country'] = data;
  	}

  	//storeFilter['filtersListSelect'] = copyObj
  	this.props.g_filterChangeAll({
  		filtersListSelect: copyObj
  	})
  }

  filterUpdate(){
  	const history = this.props.history;
		const storeFilter = this.props.globalState.storeFilter;

  	this.props.g_updateData(history, storeFilter);
		this.props.g_getFilters(history, storeFilter);
  }

  filterClick = () => {
  	const history = this.props.history;
		const storeFilter = this.props.globalState.storeFilter;

		this.props.g_beforeFilter(history, storeFilter);
		this.filterUpdate();
  }

  filterReset = () => {
  	this.props.g_filterReset()
  	this.filterUpdate();
  }

  render() {
    const {disabled, loading} = this.props;
    //const {} = this.state;
    //console.log(JSON.stringify(this.props.s_filtersListSelect, null, 2));
    let daysForDelta = DeltaDays(this.props.globalState.storeFilter.dateFrom, this.props.globalState.storeFilter.dateTo)
    let errorDate = new Date('2005-01-01')
    return (
    	<div className={loading ? 'loading' : ''}>
    		<div
    		className={'drag-me'}
    		style={{
					fontWeight: '700',
					marginBottom: '10px'
				}}>FILTERS</div>
				<DateBetween
					changeFrom={(date)=>this.changeFrom(date)}
					changeTo={(date)=>this.changeTo(date)}
					dateFrom={this.props.globalState.storeFilter.dateFrom}
					dateTo={this.props.globalState.storeFilter.dateTo}
					disabled={disabled}
					error={(
						new Date(daysForDelta[0]) < errorDate ||
						new Date(daysForDelta[1]) < errorDate ||
						new Date(this.props.globalState.storeFilter.dateTo) < errorDate ||
						new Date(this.props.globalState.storeFilter.dateFrom) < errorDate) ? true : false
					}
				/>

				<MultiSelect
					onChange={(value) => this.multiSelect(1, value)}
					placeholder='Source'
					data={this.props.s_filters['source']}
					valuesSelect={this.props.s_filtersListSelect['source']}
					disabled={disabled}
				/>

				<MultiSelect
					onChange={(value) => this.multiSelect(2, value)}
					placeholder='Medium'
					style={{marginTop: '5px'}}
					data={this.props.s_filters['medium']}
					valuesSelect={this.props.s_filtersListSelect['medium']}
					disabled={disabled}
				/>

				<MultiSelect
					onChange={(value) => this.multiSelect(3, value)}
					placeholder='Device Category'
					style={{marginTop: '5px'}}
					data={this.props.s_filters['deviceCategory']}
					valuesSelect={this.props.s_filtersListSelect['deviceCategory']}
					disabled={disabled}
				/>

				<MultiSelect
					onChange={(value) => this.multiSelect(4, value)}
					placeholder='Default Channel Grouping'
					style={{marginTop: '5px'}}
					data={this.props.s_filters['channelGrouping']}
					valuesSelect={this.props.s_filtersListSelect['channelGrouping']}
					disabled={disabled}
				/>

				<MultiSelect
					onChange={(value) => this.multiSelect(5, value)}
					placeholder='Country'
					style={{marginTop: '5px'}}
					data={this.props.s_filters['country']}
					valuesSelect={this.props.s_filtersListSelect['country']}
					disabled={disabled}
				/>

				<Button 
	    		variant="contained" 
	    		size="small"	 
	    		style={{marginTop: '10px'}}
	    		color="primary"
	    		onClick={this.filterClick}
	    		disabled={disabled}
			  > 
      		Filter
    		</Button>

    		<Button 
	    		variant="contained" 
	    		size="small"	 
	    		color="primary"   
	    		style={{marginLeft: '20px', marginTop: '10px'}}
	    		onClick = {this.filterReset}	
	    		disabled={(this.props.storeFilter.filterStringBackend.result === '' || disabled) ? true : false}
			  > 
      		Reset
    		</Button>
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    tableSessions: state.storeFilter.TableSessions,
    s_filters: state.storeFilter.filtersList,
    storeFilter: state.storeFilter,
    s_filtersListSelect: state.storeFilter.filtersListSelect
  }),
  dispatch => ({
  	g_filterReset: () => {
  		dispatch({ type: 'FILTER_RESET' })
  	},
    g_beforeFilter: (history, storeFilter) => {
    	dispatch(g_beforeFilter(history, storeFilter))
    },
    g_filterChangeAll: (data) => {
  		dispatch({ type: 'FILTER_CHANGE_ALL', payload: data })
    },
    g_updateData: (history, storeFilter) => {
    	dispatch(g_updateData(history, storeFilter))
    },
    g_getFilters: (history, storeFilter) => {
    	dispatch(g_getFilters(history, storeFilter))
    }
  })
)(Filters);