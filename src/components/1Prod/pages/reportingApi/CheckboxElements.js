import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { connect } from 'react-redux';

class CheckboxElements extends Component {
	state = {
    elements: {
    	tableSessions: this.props.st_selected.tableSessions,
			sessionsChart: this.props.st_selected.sessionsChart,
			goalConversionRate: this.props.st_selected.goalConversionRate,
			sessionsBySources: this.props.st_selected.sessionsBySources,
			sessionsByUserType: this.props.st_selected.sessionsByUserType,
			tableCalls: this.props.st_selected.tableCalls
    }
  };

  componentDidMount() {
  }

  handleChange = (data) => {
  	let elementsNew = {...this.state.elements}
  	if (elementsNew[data]){
  		elementsNew[data] = false
  	} else {
  		elementsNew[data] = true
  	}

  	this.setState({
  		elements: elementsNew
  	})
  }

  render() {
  	const {elements} = this.state

    return (
    	<div style={{
    		borderBottom: '1px solid rgb(211, 211, 211)',
    		padding: '0px 4px'
    	}}>
    		<FormControlLabel
    			className='form-control-checkbox'
	        control={
	          <Checkbox
	            checked={elements['tableSessions']}
	            onChange={() => this.handleChange('tableSessions')}
	            color="primary"
	          />
	        }
	        label="4 parameters"
      	/>

      	<FormControlLabel
      		className='form-control-checkbox'
	        control={
	          <Checkbox
	            checked={elements['sessionsChart']}
	            onChange={() => this.handleChange('sessionsChart')}
	            color="primary"
	          />
	        }
	        label="Sessions (chart)"
      	/>

      	<FormControlLabel
      		className='form-control-checkbox'
	        control={
	          <Checkbox
	            checked={elements['goalConversionRate']}
	            onChange={() => this.handleChange('goalConversionRate')}
	            color="primary"
	          />
	        }
	        label="Goal Conversion Rate (chart)"
      	/>

      	<FormControlLabel
      		className='form-control-checkbox'
	        control={
	          <Checkbox
	            checked={elements['sessionsBySources']}
	            onChange={() => this.handleChange('sessionsBySources')}
	            color="primary"
	          />
	        }
	        label="Sessions By Sources"
      	/>

      	<FormControlLabel
      		className='form-control-checkbox'
	        control={
	          <Checkbox
	            checked={elements['sessionsByUserType']}
	            onChange={() => this.handleChange('sessionsByUserType')}
	            color="primary"
	          />
	        }
	        label="Sessions By User Type"
      	/>

      	<FormControlLabel
      		className='form-control-checkbox'
	        control={
	          <Checkbox
	            checked={elements['tableCalls']}
	            onChange={() => this.handleChange('tableCalls')}
	            color="primary"
	          />
	        }
	        label="table Calls"
      	/>

      	<Button 
	    		variant="contained"
	    		size="small"
	    		color="primary"
	    		onClick={() => this.props.apply(this.state.elements)}
    		> 
    	    APPLY
   	    </Button>
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    st_selected: state.st_selected_elements
  }),
  dispatch => ({
  	g_updateSelectedEl: (data) => {
   		dispatch({ type: 'SELECTED_EL_CHANGE', payload: data })
   	}
  })
)(CheckboxElements);