import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { 
	SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging,
  SelectionState
} from '@devexpress/dx-react-grid';
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  PagingPanel,
  TableSelection
} from '@devexpress/dx-react-grid-material-ui';

// const styles = theme => ({
//   tableStriped: {
//     '& tbody tr': {
//       backgroundColor: 'red'
//     },
//   },
// });

// const TableComponentBase = ({ classes, ...restProps }) => (
//   <Table.Table
//     {...restProps}
//     className={classes.tableStriped}
//   />
// );

// export const TableComponent = withStyles(styles, { name: 'TableComponent' })(TableComponentBase);

const Root = props => <Grid.Root {...props} style={{ height: '100%' }} />;

export default ({columns, rows, change, title, loading}) => {

	const [selection, setSelection] = useState(
		//[...Array(rows.length)].map((_,i) => i)
		[]
	);

	const onSelectionChange = (data) => {
		setSelection(data);
	}

	const filterClick = (data) => {
		let newArray = [];
		selection.forEach((el, index) => {
			newArray.push(rows[el]);
		});
		change(newArray)
	}

	const filterReset = (data) => {
		setSelection([]);
		change([])
	}

  return (
  	<div className={loading ? 'loading' : ''}>
  		<div style={{
  			fontWeight: '700',
  			fontSize: '15px',
  			marginBottom: '10px'
  		}}>
  			{title}
  		</div>
    <Paper style={{ height: '300px' }} className='table-source-reporting-api'>
      <Grid
        rows={rows}
        columns={columns}
        rootComponent={Root}
      >
      	<SelectionState
          selection={selection}
          onSelectionChange={onSelectionChange}
        />
      	<PagingState
          defaultCurrentPage={0}
          pageSize={20}
        />
        <IntegratedPaging />
      	<SortingState/>
      	<IntegratedSorting />
        <VirtualTable
          height="auto"
      //     cellComponent={(props) =>   
      //       <Table.Cell 
      //       	{...props}
      //       	//style={{ height: "150px", maxHeight: "150px" }}
      //       	onClick={()=> {console.log('1234')}}
      //       >
      //       {/* console.log(props) */}
					 //  </Table.Cell>
 					// }
 					//tableComponent={TableComponent}
        />
        <TableHeaderRow showSortingControls/>
        <TableSelection
          selectByRowClick
          highlightRow
          //showSelectAll
          showSelectionColumn={false}
      //     rowComponent={(props) => 
 					// 	<Table.Row
 					// 		{...props}
 					// 		className={props.selected ? 'select-table-reporting-api' : ''}
 					// 	>
 					// 	</Table.Row>
 					// }
        />
        <PagingPanel />
      </Grid>
    </Paper>
	    <Button 
	  		variant="contained" 
	  		size="small"	 
	  		style={{marginTop: '10px'}}
	  		color="primary"
	  		onClick={filterClick}
	  		disabled={selection && selection.length === 0 ? true : false}
		  > 
	  		Filter
			</Button>

			<Button 
	  		variant="contained" 
	  		size="small"	 
	  		color="primary"   
	  		style={{marginLeft: '20px', marginTop: '10px'}}
	  		onClick = {filterReset}
	  		disabled={selection && selection.length === 0 ? true : false}
		  > 
	  		Reset
			</Button>
    </div>
  );
};