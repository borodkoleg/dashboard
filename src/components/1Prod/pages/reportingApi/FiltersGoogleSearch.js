import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import DateBetween from '../../../part/DateBetween';
//import MultiSelect from '../../../part/MultiSelect';
//import { g_updateData, g_beforeFilter } from '../../../../actions/a_reportingAPI';
//const {diffBetweenDate, DeltaDays} = require('../../../data/date')


class FiltersGoogleSearch extends Component {
	static defaultProps = {
    disabled: false,
    loading: false
	}
	
	componentDidMount(){

	}

	changeFrom = (date) => {
  	//const this_ = this;
  	this.props.g_filterChange({
  		dateFrom: date
  	})
  }

  changeTo = (date) => {
  	this.props.g_filterChange({
  		dateTo: date
  	})
  }

  filterUpdate(){
  	// const history = this.props.history;
		//
  	// this.props.g_updateData(history, storeFilter);
		// this.props.g_getFilters(history, storeFilter);
  }

  filterClick = () => {
		this.props.click();
  }

  filterReset = () => {
  	// this.props.g_filterReset()
  	// this.filterUpdate();
  }

  render() {
    const {disabled, loading} = this.props;

    //console.log(JSON.stringify(this.props.s_filtersListSelect, null, 2));
    //let daysForDelta = DeltaDays(this.props.globalState.storeFilter.dateFrom, this.props.globalState.storeFilter.dateTo)
    let errorDate = new Date('2005-01-01');

    return (
    	<div className={loading ? 'loading' : ''}>
    		<div
    		//className={'drag-me'}
    		style={{
					fontWeight: '700',
					marginBottom: '10px'
				}}>FILTERS</div>
				<DateBetween
					changeFrom={(date)=>this.changeFrom(date)}
					changeTo={(date)=>this.changeTo(date)}

					dateFrom={this.props.st_googleSearch.dateFrom}
					dateTo={this.props.st_googleSearch.dateTo}

					disabled={disabled}
					error={(
						new Date(this.props.st_googleSearch.dateTo) < errorDate ||
						new Date(this.props.st_googleSearch.dateFrom) < errorDate) ? true : false
					}
				/>

				<Button 
	    		variant="contained" 
	    		size="small"	 
	    		style={{marginTop: '10px'}}
	    		color="primary"
	    		onClick={this.filterClick}
	    		disabled={disabled}
			  > 
      		Filter
    		</Button>

    		{/*<Button */}
	    	{/*	variant="contained" */}
	    	{/*	size="small"	 */}
	    	{/*	color="primary"   */}
	    	{/*	style={{marginLeft: '20px', marginTop: '10px'}}*/}
	    	{/*	onClick = {this.filterReset}	*/}
	    	{/*	disabled={(this.props.storeFilter.filterStringBackend.result === '' || disabled) ? true : false}*/}
			  {/*> */}
      	{/*	Reset*/}
    		{/*</Button>*/}
    	</div>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    st_googleSearch: state.st_googleSearch,
  }),
  dispatch => ({
  	g_filterChange: (data) => {
  		dispatch({ type: 'GOOGLE_SEARCH_CHANGE_ALL', payload: data })
  	},
		g_filterReset: () => {
			dispatch({ type: 'GOOGLE_SEARCH_RESET'})
		},

    // g_getFilters: (history, storeFilter) => {
    // 	dispatch(g_getFilters(history, storeFilter))
    // }
  })
)(FiltersGoogleSearch);