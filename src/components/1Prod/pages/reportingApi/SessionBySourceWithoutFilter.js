import React from 'react';
import Paper from '@material-ui/core/Paper';
import { 
	SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging
} from '@devexpress/dx-react-grid';
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  PagingPanel
} from '@devexpress/dx-react-grid-material-ui';

const Root = props => <Grid.Root {...props} style={{ height: '100%' }} />;

export default ({columns, rows, title, loading}) => {
  return (
  	<div className={loading ? 'loading' : ''} style={{ height: '90%' }}>
  		<div 
  		className={'drag-me'}
  		style={{
  			fontWeight: '700',
  			fontSize: '15px',
  			marginBottom: '10px'
  		}}>
  			{title}
  		</div>
    <Paper style={{ height: '100%' }} className='table-source-reporting-api'>
      <Grid
        rows={rows}
        columns={columns}
        rootComponent={Root}
      >
      	<PagingState
          defaultCurrentPage={0}
          pageSize={20}
        />
        <IntegratedPaging />
      	<SortingState/>
      	<IntegratedSorting />
        <VirtualTable
          height="auto"
        />
        <TableHeaderRow showSortingControls/>
        <PagingPanel />
      </Grid>
    </Paper>
    </div>
  );
};