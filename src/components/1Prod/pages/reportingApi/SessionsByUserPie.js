import React, {Component} from 'react';
import { PieChart, Pie, Sector, ResponsiveContainer, Cell, Legend } from 'recharts';

const COLORS = ['#0088FE', '#FFBB28', '#D3D3D3'];

const renderActiveShape = (data) => (props) => {
  const RADIAN = Math.PI / 180;
  const {
    cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
    fill, payload, percent
  } = props;
  const cos = Math.cos(-RADIAN * midAngle);  
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{(percent * 100).toFixed(2)}%</text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
        style={{cursor: data.cursor}}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <text x={cx} y={cy-105} textAnchor={textAnchor} fill="#333">{`${payload.name}`}</text>
      <text x={cx} y={cy-105} dy={18} textAnchor={textAnchor} fill="#999">
        {`${payload.value}`}
      </text>
    </g>
  );
};

class SessionsByUserPie extends Component {
	static defaultProps = {
		data: [
			{ name: 'New Visitor', value: 13 },
  		{ name: 'Returning Visitor', value: 80 }
		],
		select: '',
		loading: false,
		title: 'test',
		cursor: 'pointer',
		className: 'drag-me'
	}

  state = {
    activeIndex: 0
  };

  onPieEnter = (data, index) => {
    this.setState({
      activeIndex: index,
    });
  };

  onPieClick = (data, index) => {
  	this.props.click(data, index)
  }
  
  componentDidMount() {
  	
  }

  render() {
    const {data, select, loading, title, cursor, className} = this.props;
    //const {} = this.state;

    let result = []
    let selectIndex = -1
  	this.props.data.forEach((el, index) => {
  		if (select === el.name){
  			selectIndex = index
  		}
  		result.push({
  			value: `${el.name} (${el.value})`,
  			color: COLORS[index]
  		})		
  	})

  // 	data: [
		// 	{ name: 'New Visitor', value: 13 },
  // 		{ name: 'Returning Visitor', value: 80 }
		// ]
  // 	let test = [
	 //  	{ value: 'item name 1', type: 'circle', id: 'ID01', color: 'red' },
		// 	{ value: 'item name 2', type: 'circle', id: 'ID02', color: 'blue' }
		// ]

    return (
    	<div className={loading ? 'loading' : ''} style={{ width: '100%', height: 270 }}>
    		<div 
    		className={className}
    		style={{
    			fontWeight: '700',
    			marginBottom: '10px'
    		}}>
					{title}
    		</div>
        <ResponsiveContainer className='shadow border-grey'>
		    	<PieChart>
		        <Pie
		          activeIndex={this.state.activeIndex}
		          activeShape={renderActiveShape({cursor: cursor})}
		          data={data}
		          innerRadius={50}
		          outerRadius={70}
		          dataKey="value"
		          onMouseEnter={this.onPieEnter}
		          onClick={this.onPieClick}
		          cx={'50%'}
		          cy={'50%'}
		        >
		        {
            data.map((entry, index) => {
            	if (selectIndex >= 0){
            		if (index === selectIndex){
            			return <Cell 
		            		key={`cell-${index}`} 
		            		fill={
		            			COLORS[index % COLORS.length]} 
		            	/>
            		} else {
            			return <Cell 
		            		key={`cell-${index}`} 
		            		fill={
		            			COLORS[2]} 
		            	/>
            		}
            	} else {
            		return <Cell 
            		key={`cell-${index}`} 
            		fill={
            			COLORS[index % COLORS.length]} 
            		/>
            	}
            })
          	}
          	</Pie>
          	<Legend 
          		layout='vertical' //'horizontal', 'vertical'
          		verticalAlign='bottom' //'top', 'middle', 'bottom'
          		align='center' //'left', 'center', 'right'
          		//height={36}
          		iconSize={12}
          		iconType='circle' //'line' | 'square' | 'rect' | 'circle' | 'cross' | 'diamond' | 'star' | 'triangle' | 'wye'
          		payload={result}
          		wrapperStyle={{bottom: '32px'}}
          	/>
      		</PieChart>
		    </ResponsiveContainer>  
		  </div>  
    )
  }
}

export default SessionsByUserPie;