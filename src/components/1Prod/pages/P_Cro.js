import React, {Component} from 'react';
import { connect } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import SessionsByUserPie from "./reportingApi/SessionsByUserPie";

//actions
import g_newReturningUsers from "../../../actions/cro/newReturningUsers";
import g_landingPages from "../../../actions/cro/landingPages";
import g_channelsGrouped from "../../../actions/cro/channelsGrouped";

import TableComponent from "../../part/TableComponent";

class P_Cro extends Component {

  componentDidMount() {
    const history = this.props.history;
    const st_cro = this.props.st_cro;
    //const st_management = this.props.st_management;

    if (st_cro &&
        st_cro.newReturningUsers &&
        st_cro.newReturningUsers.length > 0 &&
        st_cro.viewIdCro === localStorage.getItem('viewId')){
      return;
    }

    this.props.g_croLoadingStart();
    this.props.g_newReturningUsers(history);
    this.props.g_landingPages(history);
    this.props.g_channelsGrouped(history);

  }

  render() {
    const {st_cro, history} = this.props;

    return (
      <Grid container style={{marginTop: '20px'}}>
        <Grid item xs={12} sm={8} md={8} className='padding-for-blocks'>
          <TableComponent
            title='Top landing pages'
            columns={[
              { name: 'pages', title: 'Pages' },
              { name: 'entrances', title: 'Entrances' },
              { name: 'bounces', title: 'Bounces Rate' }
            ]}
            columnExtensions={[
              { columnName: 'entrances', width: 120 },
              { columnName: 'bounces', width: 120 },
            ]}
            rows={st_cro.landingPages}
            history={history}
            loading={st_cro.loading.landingPages}
          />
        </Grid>
        <Grid item xs={12} sm={4} md={4} className='padding-for-blocks'>
          <SessionsByUserPie
            title={'New and Returning Users'}
            //select={}
            data={st_cro.newReturningUsers}
            click={(a, b) => {}}
            loading={st_cro.loading.newReturningUsers}
            cursor={'initial'}
            className={''}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6} className='padding-for-blocks'>
          <TableComponent
            title='Channels Grouping'
            columns={[
              { name: 'channels', title: 'Channels' },
              { name: 'entrances', title: 'Entrances' },
              { name: 'goalConversionRate', title: 'Goal Conversion Rate' }
            ]}
            columnExtensions={[
              { columnName: 'entrances', width: 120 },
              // { columnName: 'bounces', width: 120 },
            ]}
            rows={st_cro.groupedChannels}
            history={history}
            loading={st_cro.loading.groupedChannels}
          />
        </Grid>
      </Grid>
    )
  }

}

export default connect(
  state => ({
    st_cro: state.st_cro
    //st_management: state.st_management
  }),
  dispatch => ({
    g_croLoadingStart: () => {
      dispatch({ type: 'CRO_LOADING_START' })
    },
    g_newReturningUsers: (history) => {
      dispatch(g_newReturningUsers(history))
    },
    g_landingPages: (history) => {
      dispatch(g_landingPages(history))
    },
    g_channelsGrouped: (history) => {
      dispatch(g_channelsGrouped(history))
    },
  })
)(P_Cro);
