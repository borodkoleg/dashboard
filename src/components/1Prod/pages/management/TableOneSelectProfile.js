import React, {useState} from 'react';
import Paper from '@material-ui/core/Paper';

import {
  SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging,
  SelectionState
} from '@devexpress/dx-react-grid';
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  PagingPanel,
  TableSelection
} from '@devexpress/dx-react-grid-material-ui';

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
const theme = createMuiTheme({
  palette: {
    action: {
      selected: 'rgba(-300, -300, -300, 1)'
    }
  }
});

const Root = props => <Grid.Root {...props} style={{height: '100%'}}/>;

export default ({
                  columns,
                  rows,
                  title,
                  loading,
                  columnExtensions,
                  select,
                  onSelect,
                  styleP = {height: '300px'},
                  defProfile,
                  btnClick
                }) => {
  const [selection, setSelection] = useState([select]);

  const changeSelection = (data) => {
    if (selection === undefined) {
      setSelection(data)
      onSelect(data)
    } else {
      const lastSelected = data.find(selected => selection.indexOf(selected) === -1);

      if (lastSelected !== undefined) {
        setSelection([lastSelected]);
        onSelect([lastSelected])
      }
    }
  }

  return (
    <div style={{height: '80%'}} className={loading ? 'loading' : ''}>
      <div
        // className={'drag-me'}
        style={{
          fontWeight: '700',
          fontSize: '15px',
          marginBottom: '10px'
        }}>
        {title}
      </div>
      <MuiThemeProvider theme={theme}>
        <Paper style={styleP} className='table-source-reporting-api-select'>
          <Grid
            rows={rows}
            columns={columns}
            rootComponent={Root}
          >
            <SelectionState
              selection={selection}
              onSelectionChange={changeSelection}
            />
            <PagingState
              defaultCurrentPage={0}
              pageSize={20}
            />
            <IntegratedPaging/>
            <SortingState/>
            <IntegratedSorting/>
            <VirtualTable
              height="auto"
              columnExtensions={columnExtensions}
            />
            <TableHeaderRow showSortingControls/>
            <TableSelection
              selectByRowClick
              highlightRow
              showSelectionColumn={false}
            />
            <PagingPanel/>
          </Grid>
        </Paper>
      </MuiThemeProvider>
      <div className={'default-profile'}>
        <div className={'title'}>
          Current Profile
        </div>
        <div className={'current'}>
          {rows.length > 0 ? rows[selection]['id'] : ''}
        </div>
        <div className={'title'}>
          Default Profile
        </div>
        <div className={'default'}>
          {defProfile}
        </div>
        <Button
          variant="contained"
          size="small"
          //style={{marginTop: '10px'}}
          color="primary"
          onClick={btnClick}
          className={'prof-button'}
        >
          Change Profile
        </Button>
      </div>
    </div>
  );
};