import React, {Component} from 'react';
import { connect } from 'react-redux';
//import Grid from '@material-ui/core/Grid';
//import GridLayout from 'react-grid-layout';
import { Responsive, WidthProvider } from 'react-grid-layout';
import '../../../../node_modules/react-grid-layout/css/styles.css';
import '../../../../node_modules/react-resizable/css/styles.css';


//data components
import D_TABLE_SESSIONS from '../data_elements/D_TABLE_SESSIONS';

//elements
import ChartLine from '../../part/ChartLine';
import SessionBySourceWithoutFilter from './reportingApi/SessionBySourceWithoutFilter'
import SessionsByUserPie from './reportingApi/SessionsByUserPie'
import Filters from './reportingApi/Filters'
import CallsTable from './callTracking/CallsTable'

import CheckboxElements from './reportingApi/CheckboxElements'

//actions
import { g_updateData, g_getFilters, g_updateDataSessionsByUserType } from '../../../actions/a_reportingAPI';

const ResponsiveGridLayout = WidthProvider(Responsive);

let layouts = {
	md:[
		{i: 'a', x: 0, y: 0, w: 3, h: 9, minW: 3, maxW: 4, minH: 9, maxH: 9},
  	{i: 'b', x: 3, y: 0, w: 5, h: 9, minW: 3, maxW: 5, minH: 5, maxH: 10},
  	{i: 'c', x: 8, y: 0, w: 4, h: 10, minW: 4, maxW: 4, minH: 10, maxH: 10},
  	{i: 'd', x: 0, y: 14, w: 4, h: 9, minW: 3, maxW: 5, minH: 5, maxH: 10},
  	{i: 'e', x: 4, y: 14, w: 4, h: 10, minW: 4, maxW: 6, minH: 10, maxH: 12},
  	{i: 'f', x: 8, y: 14, w: 4, h: 10, minW: 4, maxW: 4, minH: 10, maxH: 10},
  	{i: 'g', x: 0, y: 0, w: 6, h: 12, minW: 6, maxW: 6, minH: 12, maxH: 12}
	],
	sm:[
		{i: 'a', x: 0, y: 0, w: 12, h: 9, minW: 3, maxW: 12, minH: 9, maxH: 9},
		{i: 'b', x: 0, y: 0, w: 12, h: 9, minW: 4, maxW: 12, minH: 7, maxH: 12},
		{i: 'c', x: 0, y: 0, w: 12, h: 12, minW: 4, maxW: 12, minH: 12, maxH: 12},
		{i: 'd', x: 0, y: 0, w: 12, h: 9, minW: 3, maxW: 12, minH: 5, maxH: 10},
		{i: 'e', x: 0, y: 0, w: 12, h: 9, minW: 4, maxW: 12, minH: 5, maxH: 10},
		{i: 'f', x: 0, y: 0, w: 12, h: 10, minW: 4, maxW: 12, minH: 10, maxH: 10},
		{i: 'g', x: 0, y: 0, w: 12, h: 12, minW: 6, maxW: 12, minH: 12, maxH: 12}
	],
	xs:[
		{i: 'a', x: 0, y: 0, w: 12, h: 9, minW: 3, maxW: 12, minH: 9, maxH: 9},
		{i: 'b', x: 0, y: 0, w: 12, h: 9, minW: 4, maxW: 12, minH: 7, maxH: 12},
		{i: 'c', x: 0, y: 0, w: 12, h: 12, minW: 4, maxW: 12, minH: 12, maxH: 12},
		{i: 'd', x: 0, y: 0, w: 12, h: 9, minW: 3, maxW: 12, minH: 5, maxH: 10},
		{i: 'e', x: 0, y: 0, w: 12, h: 9, minW: 4, maxW: 12, minH: 5, maxH: 10},
		{i: 'f', x: 0, y: 0, w: 12, h: 10, minW: 4, maxW: 12, minH: 10, maxH: 10},
		{i: 'g', x: 0, y: 0, w: 12, h: 12, minW: 6, maxW: 12, minH: 12, maxH: 12}
	],
	xxs:[
		{i: 'a', x: 0, y: 0, w: 12, h: 9, minW: 3, maxW: 12, minH: 9, maxH: 9},
  	{i: 'b', x: 0, y: 0, w: 12, h: 9, minW: 4, maxW: 12, minH: 7, maxH: 12},
  	{i: 'c', x: 0, y: 0, w: 12, h: 12, minW: 4, maxW: 12, minH: 12, maxH: 12},
  	{i: 'd', x: 0, y: 0, w: 12, h: 9, minW: 3, maxW: 12, minH: 5, maxH: 10},
  	{i: 'e', x: 0, y: 0, w: 12, h: 9, minW: 4, maxW: 12, minH: 5, maxH: 10},
  	{i: 'f', x: 0, y: 0, w: 12, h: 10, minW: 4, maxW: 12, minH: 10, maxH: 10},
  	{i: 'g', x: 0, y: 0, w: 12, h: 12, minW: 6, maxW: 12, minH: 12, maxH: 12}
	]
};

class P_ChooseElements extends Component {
	state = {
    //items: []
  };

  componentDidMount() {

  }

  sessionsByUserPieClick = (data, index) => {
  	this.props.g_updateDataSessionsByUserType(this.history, this.props.globalState.storeFilter, data)
  }

  checkboxPanelApply = (elements) => {

   	this.props.g_updateSelectedEl(elements)

    this.props.g_getFilters(this.props.history, this.props.st_filter)
		this.props.g_updateData(this.props.history, this.props.st_filter)
  }

  render() {
    const {st_filter, st_selected} = this.props;
    //const {items} = this.state;
		//console.log('render choose');

    //console.log('st_filter',this.props.st_filter)


    let items = []

    if (st_selected.tableCalls){
			items.push(
				<div 
    			key="g"
    			data-grid={{i: 'g', x: 0, y: 0, w: 6, h: 12, minW: 6, maxW: 6, minH: 12, maxH: 12}}
    		>
					<CallsTable
						title='Calls'
						columns={[
							{ name: 'id', title: 'Id' },
						  { name: 'source', title: 'Source' },
						  { name: 'duration', title: 'Duration' },
							{ name: 'called_at', title: 'Called at' }
						]}
						columnExtensions={[
							{ columnName: 'id', width: 120 },
							// { columnName: 'source', width: 100 },
							{ columnName: 'duration', width: 110 },
							{ columnName: 'called_at', width: 170 }
						]}
						rows={this.props.st_call.calls.rows}
						loading={st_filter.loading.tableCalls}
					/>
				</div>
			)
		}

  	if (st_selected.tableSessions){
    	items.push(
    		<div 
    			key="a"
    			data-grid={{i: 'a', x: 0, y: 0, w: 3, h: 9, minW: 3, maxW: 4, minH: 9, maxH: 9}}
    		>
    			<D_TABLE_SESSIONS 
    				st_filter={st_filter}
    			/>
    		</div>
    	)
    } 

    if (st_selected.sessionsChart){
    	items.push(
    		<div 
    			key="b"
    			data-grid={{i: 'b', x: 3, y: 0, w: 5, h: 9, minW: 3, maxW: 5, minH: 5, maxH: 10}}
    		>
        	<ChartLine
						title='Sessions'
						data={st_filter.sessionsChart}
						loading={st_filter.loading.sessionsChart}
					/>
			  </div>
    	)
    }

    if (st_selected.goalConversionRate){
	    items.push(
	    	<div 
	    		key="d"
	    		data-grid={{i: 'd', x: 0, y: 14, w: 4, h: 9, minW: 3, maxW: 5, minH: 5, maxH: 10}}
	    	>
		    	<ChartLine
						title='Goal Conversion Rate'
						data={st_filter.goalConversionRate}
						YFormat={(value) => `${value}%`}
						tooltipFormat={(value, name, props) => {
				  		return `${value}%`
				  	}}
				  	loading={st_filter.loading.goalConversionChart}
					/>
	    	</div>
	    )
  	}

  	if (st_selected.sessionsBySources){
	    items.push(
	    	<div 
	    		key="e"
	    		data-grid={{i: 'e', x: 4, y: 14, w: 4, h: 10, minW: 4, maxW: 6, minH: 10, maxH: 12}}
	    	>
		    	<SessionBySourceWithoutFilter
						title='Sessions by Sources'
						columns={[
							{ name: 'id', title: 'Id' },
						  { name: 'source', title: 'Source' },
						  { name: 'sessions', title: 'Sessions' }
						]}
						rows={st_filter.sessionsBySources}
						loading={st_filter.loading.sessionsBySources}
					/>
	    	</div>
	    )
  	}

  	if (st_selected.sessionsByUserType){
	    items.push(
	    	<div 
	    		key="f"
	    		data-grid={{i: 'f', x: 8, y: 14, w: 4, h: 10, minW: 4, maxW: 4, minH: 10, maxH: 10}}
	    	>
		    	<SessionsByUserPie
						title={'Sessions by User Type'}
						select={(st_filter.filterStringBackend.userType).substr(13)}
						data={st_filter.sessionsByUserType}
						click={(a, b) => this.sessionsByUserPieClick(a, b)}
						loading={st_filter.loading.sessionsByUserType}
					/>
	    	</div>
	    )
  	}

    if (st_selected.ifOneIsTrue){
	  	items.push(
	    	<div 
	    		key="c"
	    		data-grid={{i: 'c', x: 8, y: 0, w: 4, h: 10, minW: 4, maxW: 4, minH: 10, maxH: 10}}
	    	>
		    	<Filters
						history = {this.props.history}
						disabled = {st_filter.filterStringBackend.sessionsBySourcesF !== ''}
						loading={st_filter.loading.filters}
					/>
	    	</div>
	    )
  	}



    // let layouts = {md:[], sm:[]}
    // let x = 0
    // let y = 0
    // 	layouts['md'].push({i: 'a', x: 0, y: 0, w: 3, h: 9, minW: 3, maxW: 4, minH: 9, maxH: 9})
    // 	layouts['sm'].push({i: 'a', x: 0, y: 0, w: 12, h: 9, minW: 3, maxW: 12, minH: 9, maxH: 9})
    // 	x += 
    //lg: 1280, md: 996, sm: 768, xs: 480, xxs: 0

    return (
    			<div>
    			<CheckboxElements 
    				apply={(elements) => this.checkboxPanelApply(elements)}
    			/>

    			<ResponsiveGridLayout 
						className="layout"
						layouts={layouts}
		    		breakpoints={{lg: 1280, md: 900, sm: 768, xs: 480, xxs: 0}}
		    		//cols={{lg: 12, md: 12, sm: 12, xs: 12, xxs: 12}}
		    		rowHeight={25}
		    		//isResizable={true}
		    		//compactType="vertical"
		    		//useCSSTransforms={true}
		    		//onLayoutChange={this.onLayoutChange}
		    		//verticalCompact={true}
		    		containerPadding={[17,15]}
						//draggableHandle=".drag-me"
		    		>

		    			{items}

		        </ResponsiveGridLayout>
    			
		      </div>
    )
  }
}

export default connect(
  state => ({
    globalState: state,
    st_call: state.st_callTracking,
    st_filter: state.storeFilter,
    st_selected: state.st_selected_elements
  }),
  dispatch => ({
  	g_updateData: (history, storeFilter) => {
    	dispatch(g_updateData(history, storeFilter))
    },
    g_getFilters: (history, storeFilter) => {
    	dispatch(g_getFilters(history, storeFilter))
    },
    g_updateDataSessionsByUserType: (history, storeFilter, data) => {
    	dispatch(g_updateDataSessionsByUserType(history, storeFilter, data))
    },
    g_updateSelectedEl: (data) => {
   		dispatch({ type: 'SELECTED_EL_CHANGE_ALL', payload: data })
   	}
  })
)(P_ChooseElements);