import React, {Component} from 'react';
import { connect } from 'react-redux';
import TableComponent from "../../part/TableComponent";

import Grid from '@material-ui/core/Grid';
import TableOneSelectProfile from "./management/TableOneSelectProfile";
import TableComponentSelect from "../../part/TableComponentSelect";
const {Axios, checkToken} = require('../../../components/config/axios');

class P_Management extends Component {

	getProfile = () => {
		return Axios.post(process.env.REACT_APP_HOST_PORT + '/api/user_get_profile' , {
			},
			{
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				params: {}
			})
			.then((result) => {
				checkToken(result, this.props.history);

				if (result &&
					result['data'] &&
					result['data']['body']){

					return result['data']['body'];
				} else {
					return false;
				}
			})
			.catch(err => {
				console.log(err);
				return false;
			})
	};

	setProfile = (profile) => {
		return Axios.post(process.env.REACT_APP_HOST_PORT + '/api/user_set_profile' , {profile},
			{
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				params: {}
			})
			.then((result) => {
				checkToken(result, this.props.history);

				if (result &&
					result['data'] &&
					result['data']['body']){
					return true;
				} else {
					return false;
				}
			})
			.catch(err => {
				console.log(err);
				return false;
			})
	};


  componentDidMount() {
  	//state is full
  	if (this.props.st_management &&
				this.props.st_management.profiles &&
				this.props.st_management.profiles.length > 0 &&
				localStorage.getItem('viewId')){
  		return;
		}

  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-management' , {
  			//dateFrom: storeFilter.dateFrom,
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, this.props.history)
    	//console.log(JSON.stringify(result, null, 2))
    	if (result &&
					result['data'] &&
					result['data']['body']){
    		this.props.g_updateM(result['data']['body']);

    		const currentProfile = result['data']['body']['currentProfile'];

				this.getProfile().then((res) => {
					if (res){
						this.props.g_updateM({
							defProfile: res
						});
						localStorage.setItem('viewId', res);
					} else {
						this.props.g_updateM({
							defProfile: currentProfile
						});
						localStorage.setItem('viewId', currentProfile);
						this.setProfile(currentProfile)
							.then()
					}
				})

				//localStorage.setItem('viewId', result['data']['body']['currentProfile']);
    	} else {
				this.props.g_resetM()
			}
    })
    .catch(err => {
      console.log(err)
    })
  }

	profileChange = (data) => {
  	//console.log(this.props.st_management.profiles)
  	const currentProfile = this.props.st_management.profiles[data[0]].id;

		this.props.g_updateM({
			currentProfile: currentProfile
		})
		//localStorage.setItem('viewId', currentProfile);
	}

	profileBtnClick = () => {
  	const currentProfile = this.props.st_management.currentProfile;

		this.props.g_updateM({
			defProfile: currentProfile
		});
		localStorage.setItem('viewId', currentProfile);
		this.setProfile(currentProfile).then();
	};

  render() {
  	const {st_management, history} = this.props;

  	//console.log(JSON.stringify(st_management.profiles, null, 2));

		let goals = [];
		let filters = [];
		let load = true;
  	if (st_management &&
				st_management[st_management.currentProfile] &&
				st_management[st_management.currentProfile]['goals']) {
			load = false;
			goals = st_management[st_management.currentProfile]['goals'];

			const filtersTemp = st_management[st_management.currentProfile]['filters'];

			let filtersAll = st_management['filtersAll'];

			filtersTemp.forEach((el) => {
				filtersAll.forEach((obj) => {
					if (el.filterRef.id === obj.id){
						filters.push({
							id: obj.id,
							type: obj.type,
							created: obj.created,
							name: obj.name
						})
					}
				})
			})

		}

  	let currentIndex = 0;
  	if (!load){
			currentIndex = st_management.profiles.findIndex((el) => {
					return el.id === st_management.currentProfile
			})
		}
  	// console.log([st_management.profiles.findIndex((el) => {
		// 	return el.id === st_management.currentProfile
		// })])

  	//console.log('st_management', JSON.stringify(st_management, null, 2));
  	return (
  			<Grid container style={{marginTop: '20px'}}>
  				<Grid item xs={12} sm={8} md={8} className='padding-for-blocks'>
		  			<TableOneSelectProfile
		  				title='Profiles'
		  				columns={[
									{ name: 'id', title: 'Id' },
								  { name: 'name', title: 'Name' },
								  { name: 'type', title: 'Type' }
								]}
							columnExtensions={[
								{ columnName: 'id', width: 120 },
								{ columnName: 'type', width: 100 },
							]}
							rows={st_management.profiles}
							history={history}
							loading={load}
							select={currentIndex}
							defProfile={st_management.defProfile}
							onSelect={(data) => this.profileChange(data)}
							btnClick={() => this.profileBtnClick()}
		  			/>
		  		</Grid>

		  		<Grid item xs={12} sm={5} md={5} className='padding-for-blocks'>
		  			<TableComponentSelect
		  				title='Goals'
		  				columns={[
								  { name: 'name', title: 'name' },
								  { name: 'active', title: 'active' },
								]}
							columnExtensions={[

							]}
							rows={goals.map((obj) => {
								if (obj.active){
									obj.active = 'true'
								} else {
									obj.active = 'false'
								}
								return obj
							})}
							history={history}
							loading={load}
		  			/>
		  		</Grid>

					<Grid item xs={12} sm={7} md={7} className='padding-for-blocks'>
						<TableComponent
							title='Filters'
							columns={[
								{ name: 'name', title: 'name' },
								{ name: 'type', title: 'type' },
								{ name: 'created', title: 'created' },
							]}
							columnExtensions={[
								{ columnName: 'type', width: 120 },
								{ columnName: 'created', width: 140 },
							]}
							rows={filters.map((el) => {
								const date = el.created;

								if (date[2]!=='-') {
									el.created =
										`${date.slice(5, 10)}-${date.slice(0, 4)} ${date.slice(11, 16)}`;
								}

								return el;
							})}
							history={history}
							loading={load}
						/>
					</Grid>
  		</Grid>
  	)
  }

}

export default connect(
  state => ({
    globalState: state,
    st_management: state.st_management
  }),
  dispatch => ({
  	g_updateM: (data) => {
			dispatch({ type: 'MANAGEMENT_CHANGE_ALL', payload: data })
    },
		g_resetM: () => {
			dispatch({ type: 'MANAGEMENT_RESET' })
		}
  })
)(P_Management);
