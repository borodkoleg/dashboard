import React, {Component} from 'react';
import GoogleLogin from 'react-google-login';
const Axios = require('axios');

class LoginGoogle extends Component {
  state = {
    
  };
  
  componentDidMount() {

  }

  addToken(obj){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/user-check-google' , {
          obj
    	},
    	{
        headers: {
            "Content-Type": "application/json"
        },
        params: {}
    })
    .then((res) => {
    	if (res['data']){
				localStorage.setItem('token', res['data']);
				this.props.history.push('/management');
			} else {
				localStorage.removeItem('token');
			}
    })
    .catch(err => {
      localStorage.removeItem('token');
    });
  }

  googleSuccess = (response) => {
  	if (response.profileObj && 
  		response.profileObj.email) {

  		let envEmails = process.env.REACT_APP_ADMIN_EMAILS.split(",");

  		for (let i = 0; i < envEmails.length; i++) {
  			if (envEmails[i] === response.profileObj.email){
  				//success
  				this.addToken(response)
  				return;
  			}
  		}		
  	}

  	if (window.gapi) {
      const auth2 = window.gapi.auth2.getAuthInstance()
      if (auth2 != null) {
        auth2.signOut().then(
          auth2.disconnect().then({
          	//success logout
          })
        )
      }
    }

  	localStorage.removeItem('token');
  	this.props.history.push('/login');
  }

  googleError = () => {
  	localStorage.removeItem('token');
    this.props.history.push('/login');
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{
    		textAlign:'center', 
    		marginTop: '20px'
    	}}>
    	<GoogleLogin
		    clientId={process.env.REACT_APP_GOOGLE_LOGIN}
		    buttonText="Login"
		    onSuccess={this.googleSuccess}
		    onFailure={this.googleError}
		    cookiePolicy={'single_host_origin'}
  		/>
  		</div>
    )
  }
}

export default LoginGoogle;