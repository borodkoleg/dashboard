import React, {Component} from 'react';
import TableSessions from '../../part/TableSessions';


class D_TABLE_SESSIONS extends Component {

	render() {
		const {st_filter} = this.props

		return (
				<TableSessions
					data={[
						{
							title: 'SESSIONS', 
							value: st_filter.TableSessions.Sessions,
							increase: st_filter.TableSessions.SessionsDelta > 0 ? true : false,
							percent: (st_filter.TableSessions.SessionsDelta).toFixed(2)
						},
						{
							title: 'GOAL COMPLETIONS', 
							value: st_filter.TableSessions.Goal, 
							increase: st_filter.TableSessions.GoalDelta > 0 ? true : false,
							percent: (st_filter.TableSessions.GoalDelta).toFixed(2)
						},
						{
							title: 'BOUNCES', 
							value: st_filter.TableSessions.Bounces, 
							increase: st_filter.TableSessions.BouncesDelta > 0 ? true : false,
							percent: (st_filter.TableSessions.BouncesDelta).toFixed(2)
						},
						{
							title: 'AVG. TIME ON SITE', 
							value: Number(st_filter.TableSessions.Avg).toFixed(2),
							increase: st_filter.TableSessions.AvgDelta > 0 ? true : false,
							percent: (st_filter.TableSessions.AvgDelta).toFixed(2)
						}
					]}
					loading={st_filter.loading.tableSessions}
				/>
		)
	}
}

export default D_TABLE_SESSIONS;