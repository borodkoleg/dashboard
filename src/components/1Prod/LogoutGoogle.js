import React, {Component} from 'react';
import { GoogleLogout } from 'react-google-login';
const Axios = require('axios');

class LogoutGoogle extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  logout = (response) => {
  	this.clear();
  }

  clear(){
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/user-google-logout' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((res) => {})
    .catch(err => {
      console.log(err);
    });

  	localStorage.removeItem('token');
    localStorage.removeItem('viewId');
    this.props.history.push('/login');
    this.props.logoutClick();
  }

  error = (err) => {
  	if (window.gapi) {
      const auth2 = window.gapi.auth2.getAuthInstance()
      if (auth2 != null) {
        auth2.signOut().then(
          auth2.disconnect().then({
          	//success logout
          })
        )
      }
    }
    this.clear();
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{
		   	margin: '18px 0 20px 8px'
    	}}>
    		<GoogleLogout
	      clientId={process.env.REACT_APP_GOOGLE_LOGIN}
	      buttonText="Logout"
	      onFailure={this.error}
	      onLogoutSuccess={this.logout}
	    	>
	    	</GoogleLogout>
  		</div>
    )
  }
}

export default LogoutGoogle;