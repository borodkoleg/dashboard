// Axios.interceptors.response.use((response) => {
//   if (response['data'] && response['data']['token']){
// 		if (response['data']['token'] === 'error'){
// 			localStorage.removeItem('token');
// 			this.props.history.push({
// 	  		pathname: '/login'
// 			})
// 			return;
// 		}
// 		localStorage.setItem('token', response['data']['token'])
// 	}

//   return response;
// }, error => {
//   // handle the response error
//   return Promise.reject(error);
// });

module.exports.checkToken = (response, thisHistory) => {
	if (response['data'] && response['data']['token']){
		if (response['data']['token'] === 'error'){
			localStorage.removeItem('token');
			thisHistory.push({
	  		pathname: '/login'
			})
			return;
		}
		localStorage.setItem('token', response['data']['token'])
	}
}

module.exports.Axios = require('axios');