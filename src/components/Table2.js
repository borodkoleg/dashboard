import React, {Component} from 'react';
import TableTreeAndSelect from './table2/TableGroupingAndSelect';
import TableEditing from './table2/TableEditing';
import TableDetailAndVisibility from './table2/TableDetailAndVisibility';
import TableTree from './table2/TableTree';
import TableMultiGrouping from './table2/TableMultiGrouping';


import Grid from '@material-ui/core/Grid';
const {columns, rows, defaultColumnWidths} = require('./table2/data/columnsAndRows');

class Table2 extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<Grid container>
    		<Grid item xs={12} sm={12}>
	    		<TableTreeAndSelect
	    			columns={columns}
	    			rows={rows}
	    		/>
    		</Grid>	
    		<Grid item xs={12} sm={12}>
    			<br/>
    			<br/>
    			<TableEditing
    				columns={columns}
	    			rows={rows}
    			/>
    		</Grid>
				<Grid item xs={12} sm={12}>
					<br/>
    			<br/>
    			<TableDetailAndVisibility
    				columns={columns}
	    			rows={rows}
    			/>
    		</Grid>
    		<Grid item xs={12} sm={12}>
					<br/>
    			<br/>
    			<TableTree
    				columns={columns}
    				defaultColumnWidths={defaultColumnWidths}
	    			rows={rows}
    			/>
    		</Grid>
    		<Grid item xs={12} sm={12}>
					<br/>
    			<br/>
    			<TableMultiGrouping
    				columns={columns}
	    			rows={rows}
	    			grouping={[{ columnName: 'city' }]}
    			/>
    		</Grid>
    	</Grid>
    )
  }
}

export default Table2;