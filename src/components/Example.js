import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
const {Axios, checkToken} = require('./config/axios');

class Example extends Component {
  state = {
    
  };
  
  componentDidMount() {
  	const this_ = this;
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-reporting' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, this.props.history)
      if (result && result['data'] && result['data']['body']){
      }
    })
    .catch(err => {
      
    });
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div></div>
    )
  }
}

export default connect(
  state => ({
    globalState: state
  })
)(Example);