import React from 'react';
import Paper from '@material-ui/core/Paper';
import { RowDetailState } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableRowDetail,
  TableColumnVisibility,
  Toolbar,
  ColumnChooser
} from '@devexpress/dx-react-grid-material-ui';

const RowDetail = ({ row }) => (
  <div>
    {row.details}
  </div>
);

export default ({columns, rows}) => {

  return (
    <Paper>
      <Grid
        rows={rows}
        columns={columns}
      >
        <RowDetailState
          //defaultExpandedRowIds={[2, 5]}
        />
        <Table />
        <TableHeaderRow />
        <TableRowDetail
          contentComponent={RowDetail}
        />
        <Toolbar />
        <TableColumnVisibility/>
        <ColumnChooser />
      </Grid>
    </Paper>
  );
};