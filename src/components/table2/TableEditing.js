import React, { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { 
	EditingState, 
	PagingState,
	IntegratedPaging,
	SortingState,
  IntegratedSorting,
  SearchState,
  IntegratedFiltering
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableEditRow,
  TableEditColumn,
	PagingPanel,
	SearchPanel,
	Toolbar
} from '@devexpress/dx-react-grid-material-ui';

const getRowId = row => row.id;

export default (props) => {
 
  const [rows, setRows] = useState(props.rows);

  const commitChanges = ({ added, changed, deleted }) => {
    let changedRows;
    if (added) {
      const startingAddedId = rows.length > 0 ? rows[rows.length - 1].id + 1 : 0;
      changedRows = [
        ...rows,
        ...added.map((row, index) => ({
          id: startingAddedId + index,
          ...row,
        })),
      ];
    }
    if (changed) {
      changedRows = rows.map(row => (changed[row.id] ? { ...row, ...changed[row.id] } : row));
    }
    if (deleted) {
      const deletedSet = new Set(deleted);
      changedRows = rows.filter(row => !deletedSet.has(row.id));
    }
    setRows(changedRows);
  };

  return (
    <Paper>
      <Grid
        rows={rows}
        columns={props.columns}
        getRowId={getRowId}
      >
      	<SearchState /> 
        <IntegratedFiltering />
        <EditingState
          onCommitChanges={commitChanges}
        />
        <SortingState
          defaultSorting={[{ columnName: 'city', direction: 'asc' }]}
        />
        <IntegratedSorting />
        <PagingState
          defaultCurrentPage={0}
          pageSize={5}
        />
        <IntegratedPaging />
        <Table />
        <TableHeaderRow showSortingControls/>
        <TableEditRow />
        <TableEditColumn
          showAddCommand
          showEditCommand
          showDeleteCommand
        />
        <PagingPanel />
        <Toolbar />
        <SearchPanel />
      </Grid>
    </Paper>
  );
};
