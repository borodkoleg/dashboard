import React from 'react';
import Paper from '@material-ui/core/Paper';
import { TableRow, TableCell } from 'material-ui/Table';

import {
  GroupingState,
  IntegratedGrouping,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableGroupRow,
  GroupingPanel,
  DragDropProvider,
  Toolbar,
} from '@devexpress/dx-react-grid-material-ui';

import {
  Plugin,
  Getter,
  Template,
  TemplateConnector,
} from '@devexpress/dx-react-core';

const isSummaryRow = tableRow => tableRow.type === 'summary';
const tableBodyRowsComputed = ({ tableBodyRows }) => {
  return tableBodyRows.reduce((acc, row) => {
    if (row.type === 'group') {
      if (acc.length && acc[acc.length - 1].type === 'data') {
        acc.push({
          type: 'summary',
          key: `summary_${acc.length}`,
        });
      }
    }
    acc.push(row);

    return acc;
  }, []);
};
class SummaryRow extends React.PureComponent {
  render() {
    return (
      <Plugin>
        <Getter name="tableBodyRows" computed={tableBodyRowsComputed} />
        <Template
          name="tableRow"
          predicate={({ tableRow }) => isSummaryRow(tableRow)}
        >
          {params => (
            <TemplateConnector>
              {({ rows, getTableCellColSpan }) => {
                return (
                  <TableRow>
                    <TableCell>Summary...</TableCell>
                  </TableRow>
                );
              }}
            </TemplateConnector>
          )}
        </Template>
      </Plugin>
    );
  }
};

class TableMultiGrouping extends React.PureComponent {
  state = {
  	defaultTumbler: false
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.defaultTumbler) {
      return {
        rows: [...props.rows],
        columns: [...props.columns],
        grouping: [...props.grouping],
        defaultTumbler: true
      };
    }
    // No state update necessary
    return null;
  }

  render() {
    const { rows, columns, grouping} = this.state;

    return (
      <Paper>
        <Grid
          rows={rows}
          columns={columns}
        >
          <DragDropProvider />
          <GroupingState
            grouping={grouping}
            defaultExpandedGroups={['Las Vegas']}
            onGroupingChange={this.changeGrouping}
          />
          <IntegratedGrouping />
          <Table />
          <TableHeaderRow showGroupingControls />
          <TableGroupRow />
          <SummaryRow />
          <Toolbar />
          <GroupingPanel showGroupingControls />
        </Grid>
      </Paper>
    );
  }

  changeSelection = selection => this.setState({ selection });
  changeGrouping = grouping => this.setState({ grouping });
  changeSorting = sorting => this.setState({ sorting });
}

export default TableMultiGrouping