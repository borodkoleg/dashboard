import React, { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import {
  TreeDataState,
  CustomTreeData,
  FilteringState,
  IntegratedFiltering
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableTreeColumn,
  TableFilterRow,
  TableColumnResizing
} from '@devexpress/dx-react-grid-material-ui';

const getChildRows = (row, rootRows) => (row ? row.items : rootRows);

export default ({columns, rows, defaultColumnWidths}) => {
  const [tableColumnExtensions] = useState([
    { columnName: 'name', width: 240 },
  ]);

  return (
    <Paper>
      <Grid
        rows={rows}
        columns={columns}
      >
        <TreeDataState />
        <CustomTreeData
          getChildRows={getChildRows}
        />
        <FilteringState defaultFilters={[]} />
        <IntegratedFiltering />
        <Table
          columnExtensions={tableColumnExtensions}
        />
        <TableColumnResizing defaultColumnWidths={defaultColumnWidths}/>
        <TableHeaderRow />
        <TableTreeColumn
          for="name"
        />
      	<TableFilterRow />
      </Grid>
    </Paper>
  );
};