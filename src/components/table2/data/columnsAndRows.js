export const columns = [
  { name: 'name', title: 'Name' },
  { name: 'gender', title: 'Gender' },
  { name: 'city', title: 'City' },
  { name: 'car', title: 'Car' },
];

export const defaultColumnWidths = [
	{ columnName: 'name', width: 250 },
  { columnName: 'gender', width: 250 },
  { columnName: 'city', width: 250 },
  { columnName: 'car', width: 250 }
]

export const rows = [
  {
  	id: 1,
  	name: 'Lisa', 
  	gender: 'Female', 
  	city: 'Las Vegas', 
  	car: 'BMW 750',
  	details: 'Details for Lisa from Las Vegas'
  },
  {
  	id: 2,
  	name: 'Mark', 
  	gender: 'Male', 
  	city: 'Las Vegas', 
  	car: 'Toyota Corolla',
  	details: 'Details 2',
  	items: [{
  		id: 1,
	  	name: 'Test 1', 
	  	gender: 'Male', 
	  	city: 'Las Vegas', 
	  	car: 'Toyota Corolla'
  	},
  	{
  		id: 2,
	  	name: 'Test 2', 
	  	gender: 'Male', 
	  	city: 'Las Vegas', 
	  	car: 'Toyota Corolla',
	  	items: [{
	  		id: 1,
		  	name: 'Test Test 1', 
		  	gender: 'Male', 
		  	city: 'Las Vegas', 
		  	car: 'Toyota Corolla'
	  	}]
  	}]
  },
  {
  	id: 3,
  	name: 'Linda', 
  	gender: 'Female', 
  	city: 'London', 
  	car: 'Toyota Corolla',
  	details: 'Details 3 Linda'
  },
  {
  	id: 4,
  	name: 'Marina', 
  	gender: 'Female', 
  	city: 'London', 
  	car: 'Toyota Corolla',
  	details: 'Marina'
  },
  {
  	id: 5,
  	name: 'Yaroslav', 
  	gender: 'Male', 
  	city: 'Las Vegas', 
  	car: 'BMW 750',
  	details: 'Details 5'
  },
  {
  	id: 6,
  	name: 'Robert', 
  	gender: 'Male', 
  	city: 'Chicago', 
  	car: 'Chevrolet Cruze',
  	details: 'Details 6'
  },
  {
  	id: 7,
  	name: 'Paul', 
  	gender: 'Male', 
  	city: 'Chicago', 
  	car: 'Nissan Altima',
  	details: 'Details 7'
  }
];