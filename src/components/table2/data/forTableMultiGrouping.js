export const columns = [
  { name: "name", title: "Name" },
  { name: "sex", title: "Sex" },
  { name: "city", title: "City" },
  { name: "car", title: "Car" }
];

export const rows = [
  { 
  	sex: "Female", 
  	name: "Sandra", 
  	city: "Las Vegas", 
  	car: "Audi A4" 
  },
  { 
  	sex: "Male", 
  	name: "Paul", 
  	city: "Paris", 
  	car: "Nissan Altima" 
  },
  { 
  	sex: "Male", 
  	name: "Mark", 
  	city: "Paris", 
  	car: "Honda Accord" 
  },
  { 
  	sex: "Male", 
  	name: "Paul", 
  	city: "Paris", 
  	car: "Nissan Altima" 
  },
  { 
  	sex: "Female", 
  	name: "Linda", 
  	city: "Austin", 
  	car: "Toyota Corolla" 
  },
  {
    sex: "Male",
    name: "Robert",
    city: "Las Vegas",
    car: "Chevrolet Cruze"
  },
  { 
  	sex: "Female", 
  	name: "Lisa", 
  	city: "London", 
  	car: "BMW 750" 
  },
  { 
  	sex: "Male", 
  	name: "Mark", 
  	city: "Chicago", 
  	car: "Toyota Corolla" 
  },
  {
    sex: "Male",
    name: "Thomas",
    city: "Rio de Janeiro",
    car: "Honda Accord"
  },
  {
    sex: "Male",
    name: "Robert",
    city: "Los Angeles",
    car: "Honda Accord"
  },
  {
    sex: "Male",
    name: "William",
    city: "Los Angeles",
    car: "Honda Civic"
  },
  { 
  	sex: "Male", 
  	name: "Mark", 
  	city: "Austin", 
  	car: "Nissan Altima" 
  }
]

export const grouping = [
	{ columnName: 'city' }
]