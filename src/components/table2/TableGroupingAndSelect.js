import React, {useState} from 'react';

import { SelectionState } from '@devexpress/dx-react-grid';

import Paper from '@material-ui/core/Paper';
import {
  IntegratedGrouping,
  GroupingState,
  PagingState,
  IntegratedPaging
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableGroupRow,
  PagingPanel,
  TableSelection
} from '@devexpress/dx-react-grid-material-ui';

export default ({columns, rows}) => {

  const [selection, setSelection] = useState([1]); //default select element with index 1

  const onSelectionChange = (data) => {
  	setSelection(data);
  }

  return (
    <Paper>
      <Grid
        rows={rows}
        columns={columns}
      >
        <GroupingState
          grouping={[{ columnName: 'city' }]}
        />
        <IntegratedGrouping />
        <SelectionState
          selection={selection}
          onSelectionChange={onSelectionChange}
        />

        <PagingState
          defaultCurrentPage={0}
          pageSize={5}
        />
        <IntegratedPaging />
        <Table />
        <TableHeaderRow />
        <TableSelection />
        <TableGroupRow />
        <PagingPanel />
      </Grid>
    </Paper>
  );
};