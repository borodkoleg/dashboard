import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Calendar from './graphic-part3/Calendar';
import Speedometer from './graphic-part3/Speedometer';
import SpeedometerColorAuto from './graphic-part3/SpeedometerColorAuto';
import SpeedometerGradient from './graphic-part3/SpeedometerGradient';

class GraphicPart3 extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<Grid container>
    		<Grid item xs={12} sm={12} md={12} lg={12}>
    			<Calendar/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<Speedometer/>
    		</Grid>
    		<Grid item xs={12} sm={12} md={6} lg={6}>
    			<SpeedometerColorAuto/>
    		</Grid>
				<Grid item xs={12} sm={12} md={6} lg={6}>
    			<SpeedometerGradient/>
    		</Grid>
    	</Grid>
    )
  }
}

export default GraphicPart3;