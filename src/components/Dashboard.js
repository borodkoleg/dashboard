import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

import AgGridTable from './part/AgGridTable';
import DialogConfirm from './part/DialogConfirm';
import Notify from './part/Notify';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const { basis } = require('./data/themes');
const theme1 = createMuiTheme(basis);

const {Axios, checkToken} = require('./config/axios');

const {monitors} = require('./data/fields')


class Dashboard extends Component {
  state = {
    rowData: [],
    buttonsDisable: true,
    objSelectRow: {},
    dialogConfirmOpen: false
  };
  
  componentDidMount() {
  	this.dataUpdate();

  	if (this.props.globalState.message.message === 'success'){
  		this.refs.notifyC.notify('success','Success');
  		this.props.messageChange('');
  	}
  }

  dataUpdate(){
  	const thisHistory = this.props.history
		Axios.post(process.env.REACT_APP_HOST_PORT + '/api/get-monitors' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, thisHistory)
      if (result && result['data'] && result['data']['body']){
      	this.setState({
      		rowData: [ ...result['data']['body'] ]
      	})
      }
    });
  }

  clickEdit = () => {
  	this.props.history.push({
		  pathname: '/dashboard/edit-monitor',
		  state: { objSelectRow: this.state.objSelectRow }
		})
  }

  clickAdd = () => {
  	this.props.history.push({
		  pathname: '/dashboard/add-monitor'
		})
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };

  onRowClicked = () => {
  	const selectedRows = this.gridApi.getSelectedRows();
  	//console.log(JSON.stringify(selectedRows, null, 2));

  	if (selectedRows.length > 0) {
  		this.setState({
  			buttonsDisable: false,
  			objSelectRow: selectedRows[0]
  		})
  	}
  }

  confirmClick = (data) => {
  	if (data){
  		this.funcRemove();
  	} else {
  		this.setState({
  			dialogConfirmOpen: false
  		})
  	}
  }

  clickRemove = () => {
  	this.setState({
  		dialogConfirmOpen: true
  	})
  }

  funcRemove(){
  	const thisHistory = this.props.history
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/delete-monitor' , {id: this.state.objSelectRow['id']},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, thisHistory)
      if (result['data'] && result['data']['body']){
      	this.setState({
      		dialogConfirmOpen: false,
      		buttonsDisable: true
      	})
      	this.refs.notifyC.notify('success','Success');
      	this.dataUpdate();
      }
    });
  }

  onQuickFilterChanged = () => {
  	this.gridApi.setQuickFilter(document.getElementById("quickFilter").value);
  	this.setState({
   		buttonsDisable: true
   	});
  	this.gridApi.deselectAll();
  }

  render() {
    //const {} = this.props;
    const {rowData, buttonsDisable, dialogConfirmOpen, objSelectRow} = this.state;

    //console.log(JSON.stringify(this.props.globalState, null, 2));

    return (
    	<div style={{marginTop: '20px'}}>
    		<MuiThemeProvider theme={theme1}>
    		<TextField
	        variant="outlined"
	        label="quickFilter"
	        id="quickFilter"
	        onInput={this.onQuickFilterChanged}
	        style={{
          	float: 'right',
    				marginTop: '-10px',
    				marginBottom: '0'
          }}
				/>
				</MuiThemeProvider>

    		{/*<input
          type="text"
          onInput={this.onQuickFilterChanged}
          id="quickFilter"
          placeholder="quick filter..."
          style={{
          	float: 'right',
    				padding: '4px 8px 4px',
    				fontSize: '16px',
    				position: 'relative',
    				marginTop: '-10px'
          }}
        /> */}
    		<Button 
	    		variant="contained" 
	    		size="small"
	    		style={{top: '-10px'}}
	    		onClick={this.clickAdd}
			  > 
		      Add Monitor
		    </Button>
		    <Button 
	    		variant="contained" 
	    		size="small"
	    		style={{top: '-10px', left: '20px'}}
	    		onClick={this.clickEdit}
	    		disabled={buttonsDisable}
			  > 
		      Edit Monitor
		    </Button>
		    <Button 
	    		variant="contained" 
	    		size="small"
	    		style={{top: '-10px', left: '40px'}}
	    		onClick={this.clickRemove}
	    		disabled={buttonsDisable}
			  > 
		      Remove Monitor
		    </Button>
    		<AgGridTable
    			columnDefs = {monitors}
    			rowData = {rowData}
    			onRowClicked = {this.onRowClicked}
    			onGridReady = {this.onGridReady}
    		/>

    		<DialogConfirm
    			title = 'Do you want remove this item?'
    			text = {objSelectRow['code']}
    			open = {dialogConfirmOpen}
    			click = {(data) => this.confirmClick(data)}
    		/>
    		<Notify ref='notifyC' {...this.props} />
    	</div>
    )
  }
}

export default connect(
	state => ({
    globalState: state
  }),
  dispatch => ({
    messageChange: (data) => {
      dispatch({ type: 'MESSAGE_CHANGE', payload: data });
    }
  })
)(Dashboard);