import React, {Component} from 'react';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import Grid from '@material-ui/core/Grid';
//import TextField from '@material-ui/core/TextField';
//import Button from '@material-ui/core/Button';
//import { connect } from 'react-redux';
//const {Axios, checkToken} = require('./config/axios');

class DateBetween extends Component {
	static defaultProps = {
    disabled: false
	}

  state = {
    calendarOpenFrom: false,
    calendarOpenTo: false
  };
  
  componentDidMount() {

  }

  handleDateOpenFrom = () => {
  	this.setState({
  		calendarOpenFrom: true
  	})
  }

  handleDateOpenTo = () => {
  	this.setState({
  		calendarOpenTo: true
  	})
  }

  handleDateCloseFrom = () => {
  	this.setState({
  		calendarOpenFrom: false
  	})
  }

  handleDateCloseTo = () => {
  	this.setState({
  		calendarOpenTo: false
  	})
  }

  dataChangeFrom = (date) => {
  	this.setState({
  		calendarOpenFrom: false
  	})

  	if (date > this.props.dateTo){
  		this.props.changeFrom(this.props.dateTo)
  	} else {
  		this.props.changeFrom(date)
  	}
  }

  dataChangeTo = (date) => {
  	this.setState({
  		calendarOpenTo: false
  	})

  	if (this.props.dateFrom > date){
  		this.props.changeTo(this.props.dateFrom)
  	} else {
  		this.props.changeTo(date)
  	}
  }

  render() {
    //const {} = this.props;
    const {calendarOpenFrom, calendarOpenTo} = this.state;
    const {disabled} = this.props;

    return (
    	<Grid container>
    	<MuiPickersUtilsProvider utils={DateFnsUtils}> 
    		<Grid item xs={12} sm={12} md={6} lg={6}>
	    		<KeyboardDatePicker
	          disableToolbar
	          error={this.props.dateFrom > this.props.dateTo || this.props.error}
	          variant="inline"
	          format="MM-dd-yyyy"
	          margin="normal"
	          style={{
	          	width: '100%',
	          	marginTop: '0'
	          }}
	          open={calendarOpenFrom}
	          label={'From'}
	          value={this.props.dateFrom}
	          onChange={this.dataChangeFrom}
	          onOpen={this.handleDateOpenFrom}
	          onClose={this.handleDateCloseFrom}
	          className="datePickerBasis"
	          KeyboardButtonProps={{
	            'aria-label': 'change date',
	          }}
	          disabled={disabled}
	        />
				</Grid>
				<Grid item xs={12} sm={12} md={6} lg={6}>
	        <KeyboardDatePicker
	          disableToolbar
	          error={this.props.dateFrom > this.props.dateTo || this.props.error}
	          variant="inline"
	          format="MM-dd-yyyy"
	          margin="normal"
	          style={{
	          	width: '100%',
	          	marginTop: '0'
	          }}
	          open={calendarOpenTo}
	          label={'To'}
	          value={this.props.dateTo}
	          onChange={this.dataChangeTo}
	          onOpen={this.handleDateOpenTo}
	          onClose={this.handleDateCloseTo}
	          className="datePickerBasis"
	          KeyboardButtonProps={{
	            'aria-label': 'change date',
	          }}
	          disabled={disabled}
	        />
	      </Grid>  
	    </MuiPickersUtilsProvider>
    	</Grid>
    )
  }
}

export default DateBetween;