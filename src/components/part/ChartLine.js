import React, {Component} from 'react';
import {
  Line, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, ComposedChart
} from 'recharts';

class ChartLine extends Component {
	static defaultProps = {
		title: 'Sessions',
		data: [
		  {
		    Name: 'Sep 1', Value: 4500
		  },
		  {
		    Name: 'Sep 5', Value: 5000
		  },
		  {
		    Name: 'Sep 9', Value: 5000
		  },
		  {
		    Name: 'Sep 13', Value: 6000
		  },
		  {
		    Name: 'Sep 17', Value: 6000
		  },
		  {
		    Name: 'Sep 21', Value: 6500
		  },
		  {
		    Name: 'Sep 25', Value: 7000
		  },
		  {
		    Name: 'Sep 29', Value: 8000
		  }
		],
		YFormat: (value) => `${value}`,
		tooltipFormat: (value, name, props) => {
  		return `${value}`
  	},
  	loading: false
	}

  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    const {data, title, YFormat, YDomain, tooltipFormat, loading} = this.props;
    //const {} = this.state;

    return (
    	<div 
    		className={loading ? 'shadow loading' : 'shadow drag-me'}
    		style={{ 
	    		width: '97%', 
	    		height: '80%',
	    		padding: '10px 10px 30px 0',
	    		border: '1px solid #D3D3D3'
    	}}>
    		<div style={{
    			fontSize: '15px',
    			fontWeight: 'bold',
    			margin: '0 0 10px 10px'
    		}}>
    			{title}
    		</div>
        <ResponsiveContainer>
          <ComposedChart
            data={data}
          >
            <CartesianGrid stroke="#f5f5f5" strokeDasharray="3 3" />
            <XAxis dataKey="Name" scale="point" /> {/*auto */}
            <YAxis 
            	tickFormatter={YFormat} 
            	domain={YDomain}
            />
            <Tooltip 
            	formatter={tooltipFormat}
            />
            
            <Line type="monotone" dataKey="Value" stroke="#0088FE" />   		
      
          </ComposedChart>
        </ResponsiveContainer>
      </div>
    )
  }
}

export default ChartLine;