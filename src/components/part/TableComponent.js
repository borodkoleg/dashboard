import React from 'react';
import Paper from '@material-ui/core/Paper';

import { 
	SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging
} from '@devexpress/dx-react-grid';
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  PagingPanel
} from '@devexpress/dx-react-grid-material-ui';

//const {formatDayFromFormat} = require('../../../data/date.js')

const Root = props => <Grid.Root {...props} style={{ height: '100%' }} />;

const Cell = (props) => {
  const { column, value } = props;
  if (column.name === 'pages') {
    return <VirtualTable.Cell {...props} title={value}>
      {value}
    </VirtualTable.Cell>
  }
  return <VirtualTable.Cell {...props} />;
};

export default ({columns, rows, title, loading, columnExtensions}) => {

  return (
  	<div style={{height: '80%'}} className={loading ? 'loading' : ''}>
  		<div
  		// className={'drag-me'}
  		style={{
  			fontWeight: '700',
  			fontSize: '15px',
  			marginBottom: '10px'
  		}}>
  			{title}
  		</div>
    <Paper style={{ height: '300px' }} className='table-source-reporting-api'>
      <Grid
        rows={rows}
        columns={columns}
        rootComponent={Root}
      >
      	<PagingState
          defaultCurrentPage={0}
          pageSize={20}
        />
        <IntegratedPaging />
      	<SortingState/>
      	<IntegratedSorting />
        <VirtualTable
          height="auto"
          columnExtensions={columnExtensions}
          cellComponent={Cell}
        />
        <TableHeaderRow showSortingControls/>
        <PagingPanel />
      </Grid>
    </Paper>
    </div>
  );
};