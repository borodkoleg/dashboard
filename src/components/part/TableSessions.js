import React, {Component} from 'react';
//import { connect } from 'react-redux';

import TrendingDownIcon from '@material-ui/icons/TrendingDown';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
//const {Axios, checkToken} = require('./config/axios');

class TableSessions extends Component {
	static defaultProps = {
		data: [
			{
				title: 'SESSIONS', 
				value: '231,556', 
				increase: true,
				percent: '0.1'
			},
			{
				title: 'GOAL COMPLETIONS', 
				value: '2,159', 
				increase: true,
				percent: '39.6'
			},
			{
				title: 'BOUNCES', 
				value: '177,101', 
				increase: false,
				percent: '-0.4'
			},
			{
				title: 'AVG. TIME ON SITE', 
				value: '00:04:27', 
				increase: true,
				percent: '8.4'
			}
		],
		loading: false
	}

  state = {
    
  };
  
  componentDidMount() {

  }

  render() {
    const {data, loading} = this.props;
    //const {} = this.state;

    return (
    	<div className={loading ? "divTable table-reporting loading" : "divTable table-reporting drag-me"}>
				<div className="divTableBody">

				{data.map((obj, index) => {
        	return <div key={index} className="divTableRow">
						<div className="divTableCell table-reporting-title">
							{obj.title}
						</div>
						<div className="divTableCell table-reporting-value">
							<div className='table-reporting-big'>
								{obj.value}
							</div>
							{obj.increase 
							?
								<div 
									className='table-reporting-small table-reporting-increase'>
									<TrendingUpIcon
										className='table-reporting-up-icon'
									/> {isFinite(obj.percent) ? obj.percent : 0}%
								</div> 
							: 
								<div 
									className='table-reporting-small table-reporting-decrease'>
									<TrendingDownIcon
										className='table-reporting-down-icon'
									/> {isFinite(obj.percent) ? obj.percent : 0}%
								</div>
							}
						</div>
					</div>
      	})}

				</div>
			</div>
    )
  }
}

export default TableSessions;