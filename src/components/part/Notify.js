import React, {Component} from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Notify extends Component {
  state = {
    
  };

  toastErrorId = null;
  toastSuccessId = null;

  notify = (type, text) => {
  	if (type === 'error') {
	  	if (! toast.isActive(this.toastErrorId)) {
		  	this.toastErrorId = toast.error(text, { 
					position: "top-right",
					autoClose: 3000,
					hideProgressBar: true,
					closeOnClick: true,
					pauseOnHover: false,
					draggable: false
					//className: 'toast-error-main',
  				//bodyClassName: "toast-error-body",
		  	});
	  	}
  	}

  	if (type === 'success') {
			if (! toast.isActive(this.toastSuccessId)) {
		  	this.toastSuccessId = toast(text, { 
					position: "top-right",
					autoClose: 3000,
					hideProgressBar: true,
					closeOnClick: true,
					pauseOnHover: false,
					draggable: false,
					className: 'toast-success-main',
  				bodyClassName: "toast-success-body",
		  	});
	  	}
  	}
  }

  render() {

    return (
   		<ToastContainer 
		 	
		  />
    )
  }
}

export default Notify;