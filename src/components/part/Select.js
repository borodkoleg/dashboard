import React, {Component} from 'react';
import {Autocomplete} from '@material-ui/lab';
import TextField from '@material-ui/core/TextField';

class Select extends Component {
	static defaultProps = {
		data: [
			{title: '1', value: 10},
			{title: '2', value: 20},
			{title: '3', value: 30}
		],
		placeholder: 'Placeholder',
		style: {}
	}

  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<Autocomplete
			  options={this.props.data}
			  getOptionLabel={option => option.title}
			  className='autocomplete-select-root'
			  style={this.props.style}
			  onChange={this.props.onChange}
			  renderInput={params => (
			    <TextField {...params} 
			    	className={'autocomplete-select-text-field'}
			    	label={this.props.placeholder}
			    	variant="outlined"
			    	fullWidth 
			    />
			  )}
			/>
    )
  }
}

export default Select;