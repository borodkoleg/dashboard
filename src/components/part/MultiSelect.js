import React, {Component} from 'react';
import { Picky } from 'react-picky';
import "react-picky/dist/picky.css";

class MultiSelect extends Component {
	static defaultProps = {
		data: [
	  	//{key: 1, value: '1ww'},
    ],
    valuesSelect: [],
    disabled: false,
    className: 'multi-select-picky'
	}

	// state = {
	//   value: null,
	//   arrayValue: [],
	//   start: true
	// };

	// static getDerivedStateFromProps(props, state) {
 //    if (props.data !== state.arrayValue && state.start) {
 //      return {
 //        arrayValue: [...props.data],
 //        start: false
 //      };
 //    }
 //    // No state update necessary
 //    return null;
 //  }
  
  componentDidMount() {
  	//this.forceUpdate();
  }

  // selectOption = (value) => {
  //   this.setState({value: value});
  // }

  selectMultipleOption = (value) => {
    // this.setState({ 
    // 	arrayValue: value 
    // });

    this.props.onChange(value)
  }

  render() {
    const {data, placeholder, style, valuesSelect, disabled, className} = this.props;
    //const {arrayValue} = this.state;

    return (
    	<div style={style}>
    	 <Picky
        value={valuesSelect}
        options={data}
        disabled={disabled}
        onChange={this.selectMultipleOption}
        //open={false}
        valueKey="key"
        labelKey="value"
        multiple={true}
        includeSelectAll={true}
        includeFilter={true}
        className={className}
        placeholder={placeholder}
        numberDisplayed={0}
        allSelectedPlaceholder={`${placeholder} (${valuesSelect.length})`}
        //`${data.length} items`
        manySelectedPlaceholder={`${placeholder} (${valuesSelect.length})`}
        //dropdownHeight={600}
        render={({
          //index,
          style,
          item,
          isSelected,
          selectValue,
          labelKey,
          valueKey,
          multiple
        }) => {
          return (
            <li
              style={{ ...style }} // required
              className={isSelected ? "selected" : ""} // required to indicate is selected
              key={item[valueKey]} // required
              onClick={() => selectValue(item)}
            >
              
              <input type="checkbox" checked={isSelected} readOnly />
              <span className="checkmark"></span>
        			<span>
        				{item[labelKey]}
        				<span style={{
        					float: 'right',
									position: 'relative',
									top: '9px',
									right: '10px'
        				}}>
        					{item.number ? item.number : ''}
        				</span>
        			</span>
            </li>
          );
        }}
      />
      </div>
    )
  }
}

export default MultiSelect;