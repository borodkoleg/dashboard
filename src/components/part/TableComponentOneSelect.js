import React, {useState} from 'react';
import Paper from '@material-ui/core/Paper';

import {
  SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging,
  SelectionState
} from '@devexpress/dx-react-grid';
import {
  Grid,
  VirtualTable,
  TableHeaderRow,
  PagingPanel,
  TableSelection
} from '@devexpress/dx-react-grid-material-ui';

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
const theme = createMuiTheme({
  palette: {
    action: {
      selected: 'rgba(-300, -300, -300, 1)'
    }
  }
    // overrides: {
    //   MuiTableRow: {
    //     color: "#FFFFFF",
    //     root: {
    //       color: "#FFFFFF"
    //     }
    //   }
    // },
    // action: {
    //   selected: {
    //     MuiTableRow: {
    //       root: {
    //         color: "#FFFFFF"
    //       }
    //     }
    //   }
    // }
});

//const {formatDayFromFormat} = require('../../../data/date.js')

const Root = props => <Grid.Root {...props} style={{height: '100%'}}/>;

export default ({
                  columns,
                  rows,
                  title,
                  loading,
                  columnExtensions,
                  select,
                  onSelect,
                  styleP = {height: '300px'}
                }) => {
  const [selection, setSelection] = useState([select]);

  const changeSelection = (data) => {
    if (selection === undefined) {
      setSelection(data)
      onSelect(data)
    } else {
      const lastSelected = data.find(selected => selection.indexOf(selected) === -1);

      if (lastSelected !== undefined) {
        setSelection([lastSelected]);
        onSelect([lastSelected])
      }
    }
  }

  return (
    <div style={{height: '80%'}} className={loading ? 'loading' : ''}>
      <div
        // className={'drag-me'}
        style={{
          fontWeight: '700',
          fontSize: '15px',
          marginBottom: '10px'
        }}>
        {title}
      </div>
      <MuiThemeProvider theme={theme}>
        <Paper style={styleP} className='table-source-reporting-api-select'>
          <Grid
            rows={rows}
            columns={columns}
            rootComponent={Root}
          >
            <SelectionState
              selection={selection}
              onSelectionChange={changeSelection}
            />
            <PagingState
              defaultCurrentPage={0}
              pageSize={20}
            />
            <IntegratedPaging/>
            <SortingState/>
            <IntegratedSorting/>
            <VirtualTable
              height="auto"
              columnExtensions={columnExtensions}
            />
            <TableHeaderRow showSortingControls/>
            <TableSelection
              selectByRowClick
              highlightRow
              showSelectionColumn={false}
            />
            <PagingPanel/>
          </Grid>
        </Paper>
      </MuiThemeProvider>
    </div>
  );
};