import React, {Component} from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';


class AgGridTable extends Component {
  state = {
    
  };

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div 
			className="ag-theme-balham"
    	style={{ 
        height: '500px', 
        width: '100%' }} >
    		<AgGridReact
          columnDefs={this.props.columnDefs}
          pagination={true}
          paginationAutoPageSize={true}
          rowData={this.props.rowData}
          rowStyle = {{fontSize: '15px'}}
          onGridReady={this.props.onGridReady}
          rowSelection = {'single'}
          onRowClicked={this.props.onRowClicked}
        />
    	</div>
    )
  }
}

export default AgGridTable;