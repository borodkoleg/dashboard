import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class DialogConfirm extends Component {
	state = {
		
	}

  render() {
    const {title, text} = this.props;

  return (
    <div>
      <Dialog
        open={this.props.open}
        onClose={(data) => this.props.click(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle className={'text-headline'} style={{paddingButtom: '0!important'}}>{title}</DialogTitle>
        <DialogContent>
      		{text}
        </DialogContent>
        <DialogActions>
          <Button 
          	onClick={(data) => this.props.click(false)} 
          	color="primary"
          >
            Cancel
          </Button>
          <Button 
          	onClick={(data) => this.props.click(true)} 
          	color="primary"
          >
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
	}
}