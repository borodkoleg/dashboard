import React, {Component} from 'react';
//import { connect } from 'react-redux';
const {Axios, checkToken} = require('../config/axios');

class BtnLogout extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  LogoutClick = () => {
  	let this_ = this;
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/user_logout' , {},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    })
    .catch((err) => {
    	console.log(err);
    })
    .finally(function () {
    	localStorage.removeItem('token');
    	this_.props.history.push('/login');
    })
	}

  render() {
    return (
  		<div
    		onClick={this.LogoutClick}
    		className={"menu-link"}
    	>
          Log out
      </div>
    )
  }
}

export default BtnLogout;