import React, {Component} from 'react';
import ReactSpeedometer from "react-d3-speedometer"

class SpeedometerGradient extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{textAlign:'center'}}>
    	<ReactSpeedometer
    		height={250}
			  needleHeightRatio={0.7}
			  maxSegmentLabels={5}
			  segments={1000}
			  value={333}
			/>
			</div>
    )
  }
}

export default SpeedometerGradient;