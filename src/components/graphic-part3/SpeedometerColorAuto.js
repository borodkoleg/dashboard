import React, {Component} from 'react';
import ReactSpeedometer from "react-d3-speedometer"

class SpeedometerColorAuto extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{textAlign:'center'}}>
    	<ReactSpeedometer
			  maxValue={500}
			  height={250}
			  value={473}
			  needleColor="red"
			  startColor="green"
			  segments={10}
			  endColor="blue"
			  textColor="grey"
			/>
			</div>
    )
  }
}

export default SpeedometerColorAuto;