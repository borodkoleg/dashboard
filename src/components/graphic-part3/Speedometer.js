import React, {Component} from 'react';
import ReactSpeedometer from "react-d3-speedometer"

class Speedometer extends Component {
  state = {
    
  };
  
  componentDidMount() {
  }

  render() {
    //const {} = this.props;
    //const {} = this.state;

    return (
    	<div style={{textAlign:'center'}}>
    	<ReactSpeedometer
    		height={250}
    		value={34} 
    		maxValue={100}
    		minValue={0}
    		//maxSegmentLabels={10}
    		segments={5}
    		segmentColors={['#ECEFF4','#00C49F', '#138808', '#0088FE', '#FF9933']}
  			needleColor="#000080"
  			valueFormat="d"
  			needleTransition="easeElastic"
  			// eslint-disable-next-line
  			currentValueText="${value}%"
  			//forceRender={true}
    	/>
    	</div>
    )
  }
}

export default Speedometer;