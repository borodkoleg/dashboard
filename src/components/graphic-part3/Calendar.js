import React, {Component} from 'react';
import { ResponsiveCalendar } from '@nivo/calendar'
import data from './data/forCalendar';
import DialogWarning from '../part/DialogWarning';

class Calendar extends Component {
  state = {
    openWarning: false,
    textWarning: ''
  };
  
  componentDidMount() {
  }

  click = (day, event) => {
  	//console.log(day);
  	this.setState({
  		openWarning: true,
  		textWarning: day['day']
  	})
  }

  dialogClick = (data) => { //true || false
  	this.setState({
  		openWarning: false
  	})
  }

  render() {
    //const {} = this.props;
    const {openWarning, textWarning} = this.state;

    return (
    	<div style={{height:'200px', width: '100%'}}>
    	<ResponsiveCalendar
    		//width={100%}
    		//height={600}
    		align={'top-right'}
        data={data}
        from="2018-01-01"
        to="2018-12-31"
        emptyColor="#eeeeee"
        colors={[ '#61cdbb', '#97e3d5', '#e8c1a0', '#f47560' ]}
        margin={{ top: 40, right: 40, bottom: 40, left: 40 }}
        yearSpacing={40}
        monthBorderColor="#ffffff"
        monthBorderWidth={2}
        dayBorderWidth={1}
        dayBorderColor="#e0c6c6" //#ffffff
        onClick={this.click}
        legends={[
            {
                anchor: 'bottom-right',
                direction: 'row',
                translateY: 36,
                itemCount: 4,
                itemWidth: 42,
                itemHeight: 36,
                itemsSpacing: 14,
                itemDirection: 'right-to-left'
            }
        ]}
    	/>
    	<DialogWarning
    		open={openWarning}
    		click={(data)=>this.dialogClick(data)}
    		title='Action'
    		text={textWarning}
    	/>
    	</div>
    )
  }
}

export default Calendar;