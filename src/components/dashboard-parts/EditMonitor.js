import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Notify from '../part/Notify';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const { basis } = require('../data/themes');
const theme1 = createMuiTheme(basis);

const {isFloat, isYear} = require('../data/validation');

const {Axios, checkToken} = require('../config/axios');

let startInputsValue = {
	code: '',
	brand: '',
	color: '',
	type: '',
	diagonal: '',
	year: ''
};

class EditMonitor extends Component {
  state = {
    inputsValue: startInputsValue,
    curentId: ''
  };
  
  componentDidMount() {
  }

  static getDerivedStateFromProps(props, state) {
  	const selectRow = props.location.state.objSelectRow;
    if (state.curentId !== selectRow.code) {
    	//console.log(Math.random());
      return {
        inputsValue: {...selectRow},
        curentId: selectRow.code
      };
    }
    // No state update necessary
    return null;
  }

  inputChange = (data, key) => {
  	let newData = data.target.value;

  	this.setState(prevState => {
		  let obj = Object.assign({}, prevState.inputsValue);
		  obj[key] = newData.toUpperCase();              
		  return { inputsValue: obj };
		})
  }

  editItem = () => {
  	let obj = this.state.inputsValue;
  	let this_ = this;
  	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/edit-monitor' , {data: obj},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, this_.props.history)
    	
      if (result['data'] && result['data']['body']){
      	this.setState({
    			inputsValue: startInputsValue
    		})
      	this.props.messageChange('success');
      	this.props.history.push({
				  pathname: '/dashboard'
				})
      } else {
      	this.refs.notifyC.notify('error','This item already exists');
      }
    })
  }

  render() {
    //const {} = this.props;
    const {inputsValue} = this.state;
    //console.log(JSON.stringify(this.props.location.state.objSelectRow));

    return (
    	<Grid container>
    		<MuiThemeProvider theme={theme1}>
	    		<Grid item xs={12} sm={6} md={4}>
		        <div className={'block'}>
		        <div className={'text-headline'}>
		        	Edit Monitor
		        </div>

		        <TextField
			        placeholder="Code"
			        variant="outlined"
			        label="Code"
			        value={inputsValue['code']}
			        name="code"
			        onChange={(value, key) => this.inputChange(value, 'code')}
			        error={inputsValue['code']===''}
				    />

		        <TextField
			        placeholder="Brand"
			        variant="outlined"
			        label="Brand"
			        value={inputsValue['brand']}
			        name="brand"
			        onChange={(value, key) => this.inputChange(value, 'brand')}
			        error={inputsValue['brand']===''}
				    />

				    <TextField
				    	label="Color"
			        placeholder="Color"
			        variant="outlined"
			        value={inputsValue['color']}
			        name="color"
			        onChange={(value, key) => this.inputChange(value, 'color')}
			        error={inputsValue['color']===''}
					  />

					  <TextField
					  	label="Type"
			        placeholder="Type"
			        variant="outlined"
			        value={inputsValue['type']}
			        name="type"
			        onChange={(value, key) => this.inputChange(value, 'type')}
			        error={inputsValue['type']===''}
			      />

			      <TextField
			      	label="Diagonal"
			        placeholder="Diagonal"
			        variant="outlined"
			        value={inputsValue['diagonal']}
			        name="diagonal"
			        onChange={(value, key) => this.inputChange(value, 'diagonal')}
			        error={!isFloat(inputsValue['diagonal'], true)}
			      />

			      <TextField
			      	label="Year"
			        placeholder="Year"
			        variant="outlined"
			        value={inputsValue['year']}
			        name="year"
			        onChange={(value, key) => this.inputChange(value, 'year')}
			        error={!isYear(inputsValue['year'], true)}
			      />

				    <br/>
					  <Button 
		    		variant="contained" 
		    		size="medium"	    		
		    		onClick={this.editItem}
		    		disabled={
		    				inputsValue['brand']!=='' &&
		    				inputsValue['color']!=='' &&
		    				inputsValue['type']!=='' &&
		    				isFloat(inputsValue['diagonal'], true) &&
		    				isYear(inputsValue['year'], true)
		    			? false
		    			: true
		    		}
					  > 
				      Edit
				    </Button>
		        
		        </div>
		      </Grid>
		    <Notify ref='notifyC' {...this.props} />
		    </MuiThemeProvider>
	    </Grid>
    )
  }
}

export default connect(
	state => ({
    globalState: state
  }),
  dispatch => ({
    messageChange: (data) => {
      dispatch({ type: 'MESSAGE_CHANGE', payload: data });
    }
  })
)(EditMonitor);