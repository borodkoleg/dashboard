import React, { Component } from 'react';
import SortableTree from 'react-sortable-tree';
import 'react-sortable-tree/style.css'; // This only needs to be imported once in your app
import FileExplorerTheme from 'react-sortable-tree-theme-file-explorer';

export default class Tree2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      treeData: [
        { 
        	title: 'Chicken', 
        	link: 'test1', 
        	children: [
        		{ 
        			title: 'Egg',
        			link: 'test2'
        		}
        	] 
        },
        { 
        	title: 'Fish', 
        	children: [
	        	{ 
	        		title: 'fingerline'
	        	}
        	] 
        }
      ],
    };
  }



  treeDataChange = (data) => {
  	//console.log(data);
  	this.setState({
  	 	treeData: data
  	})
  }

  onSelectNode = (data) => {
  	//console.log(data);
  	if (data && data.node && data.node.children){
  		console.log('parent');
  	} else {
  		console.log('child');
  	}

  	console.log(data.node.link);
  }

  render() {
    return (
      <div className='tree-mind'>
        <SortableTree
          treeData={this.state.treeData}
          onChange={this.treeDataChange}
          //onChange={treeData => this.setState({ treeData })}
          theme={FileExplorerTheme}
           generateNodeProps={rowInfo => ({
             onClick: () => this.onSelectNode(rowInfo)})}
          //canDrag={false} 
        />
      </div>
    );
  }
}