require('dotenv-flow').config({silent: true});
const knexConfig = require('../../knexfile');

const environment = process.env.NODE_ENV;
const configEnv = knexConfig[environment];

module.exports = require('knex')(configEnv);