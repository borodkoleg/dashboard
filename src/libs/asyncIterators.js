const INTERVAL = 5;

exports.asyncArrayEach = (array, fn) => {
  let time = Date.now();
  let i = 0;
  const last = array.length - 1;

  const next = () => {
    while (i <= last) {
      const now = Date.now();
      const diff = now - time;
      if (diff > INTERVAL) {
        time = now;
        setTimeout(next, 0);
        break;
      } else {
        fn(array[i], i++);
      }
    }
  };

  next();
};

//=========

exports.asyncObjectEach = (object, fn) => {
  let time = Date.now();
  let i = 0;
  
  const keys = Object.keys(object);
  const last = keys.length - 1;

  const next = () => {
    while (i <= last) {
      const now = Date.now();
      const diff = now - time;

      if (diff > INTERVAL) {
        time = now;
        setTimeout(next, 0);
        break;
      } else {
        fn(keys[i], i++);
      }
    }
  };

  next();
};