require('dotenv-flow').config({silent: true});
const jwt = require('jsonwebtoken');

exports.jwtEncode = function(data){ 
	return new Promise(function(resolve) {
		jwt.sign(
			data, 
			process.env.REACT_APP_JWT_SECRET,
			{ expiresIn: process.env.REACT_APP_JWT_TIME },
			function(err, token) {
			if (!err){
	   		resolve(token);
	   	} else {
	   		resolve(false);
	   	}
		});
	});
};

exports.jwtDecode = function(token){
	return new Promise(function(resolve) {
		jwt.verify(token, process.env.REACT_APP_JWT_SECRET, function(err, decoded) {
			//console.log(err);
	  	if (!err){
	  		resolve(decoded);
	  	} else {
	  		resolve(false);
	  	}
		});
	})
};