import React, { Component } from 'react';
//import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import LogoutGoogle from '../components/1Prod/LogoutGoogle'
import TreeItem from '@material-ui/lab/TreeItem';
import TreeView from '@material-ui/lab/TreeView';
import { connect } from 'react-redux';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
//import g_newReturningUsers from "../actions/cro/newReturningUsers";
//import g_landingPages from "../actions/cro/landingPages";
//import g_channelsGrouped from "../actions/cro/channelsGrouped";
import g_resetAll from "../actions/layout/ac_adminLayout";

//import MenuMgmt from '../components/MenuMgmt';
//import BtnLogout from '../components/part/BtnLogout';

class AdminLayout extends Component {

	logoutClick = () => {
		this.props.g_resetAll();
	}

  render() {
		//console.log(JSON.stringify(this.props.gl_state, null, 2))

    return (
    <Grid container>
    	<Grid item xs={12} sm={2} style={{paddingRight: '10px'}}>
    	<TreeView
      	defaultCollapseIcon={<ExpandMoreIcon />}
      	defaultExpandIcon={<ChevronRightIcon />}
      	defaultExpanded={['1']}
      	// classes={{	
      	// }}
    	>
    		<TreeItem 
    			nodeId="1" 
    			label="Google Analytics"
    			classes={{
    				label: 'menu-link',
    				content: 'tree-item-content',
    				root: 'tree-item-root'
    			}}
    		>
    			{/*<Link className="menu-link" to="/reporting-API">
    				Reporting API
    			</Link>*/}
    			{/*<Link className="menu-link" to="/call-tracking">
    				Call Tracking
    			</Link>*/}
					<Link className="menu-link" to="/management">
						Management
					</Link>
					<Link className="menu-link" to="/google-search-console">
						Google Search Console
					</Link>
					<Link className="menu-link" to="/choose-elements">
    				Choose Elements
    			</Link>
					<Link className="menu-link" to="/cro">
						CRO
					</Link>

    		</TreeItem>

    		<TreeItem 
    			nodeId="2" 
    			label="Examples"
    			classes={{
    				label: 'menu-link',
    				content: 'tree-item-content',
    				root: 'tree-item-root'
    			}}
    		>
	    		<Link className="menu-link" to="/charts">
						Charts
	    		</Link>
	    		<Link className="menu-link" to="/charts-part2">
						Charts 2
	    		</Link>
	    		<Link className="menu-link" to="/charts-part3">
						Charts 3
	    		</Link>
	    		<Link className="menu-link" to="/forms">
						Forms
	    		</Link>
	    		<Link className="menu-link" to="/validation">
						Validation
	    		</Link>
	    		<Link className="menu-link" to="/images">
						Images
	    		</Link>
	    		<Link className="menu-link" to="/dashboard">
						Table
	    		</Link>
					<Link className="menu-link" to="/table2">
	    		Table 2
	    		</Link>
	    		<Link className="menu-link" to="/maps">
	    		Maps
	    		</Link>
	    		<Link className="menu-link" to="/others">
	    		Others
	    		</Link>
	    	</TreeItem>
	    <LogoutGoogle
    		history={this.props.history}
				logoutClick={() => this.logoutClick()}
    	/>	
	    </TreeView>
    		{/*<BtnLogout
	      	history={this.props.history}
	      />*/}
      </Grid>
      <Grid item xs={12} sm={10} style={{position: 'relative'}}>
      		{this.props.children}
      </Grid>
    </Grid>
    );
  }
}

export default connect(
  state => ({
    loading: state.storeFilter.loading
  }),
	dispatch => ({
		g_resetAll: () => {
			dispatch(g_resetAll())
		}
	})
)(AdminLayout);