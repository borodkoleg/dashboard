const {Axios, checkToken} = require('../../components/config/axios');
const {formatDay} = require('../../components/data/date');

module.exports = (dispatch, getState, history) => {
	dispatch({type: 'LOADING_CHANGE', payload:
		['tableCalls', true]
	})

	Axios.post(process.env.REACT_APP_HOST_PORT + '/api/ctm-list-of-calls-duration' , {
				startDate: formatDay(getState().storeFilter.dateFrom),
				endDate: formatDay(getState().storeFilter.dateTo),
			},
	  	{
	      headers: {
	          "Content-Type": "application/json",
	          "Authorization": `Bearer ${localStorage.getItem('token')}`
	      },
	      params: {}
	})
  .then((result) => {
  	checkToken(result, history)

  	if (result && 
  			result['data'] &&
  			result['data']['body']){

  		let ar = result['data']['body']

  		dispatch({type: 'CALL_CHANGE_ALL', payload: 
	  		{
	  			calls: {
	  				rows: ar
	  			}
	  		}
  		})

  	} else {
  		dispatch({type: 'CALL_CHANGE_ALL', payload: 
	  		{
	  			calls: {
	  				rows: []
	  			}
	  		}
  		})
  	}
  })
  .catch(err => {
    console.log(err)
  })
  .finally(function () {
  	dispatch({type: 'LOADING_CHANGE', payload: 
			['tableCalls', false]
		})
	});	
}