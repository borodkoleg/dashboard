const {Axios, checkToken} = require('../../components/config/axios');
const {formatDay} = require('../../components/data/date');

export default (history) => (dispatch, getState) => {
  // dispatch({type: 'GOOGLE_SEARCH_LOADING', payload:
  //     ['landingPages', true]
  // })

  if (!getState().st_googleSearch.sites || !getState().st_googleSearch.sites.length > 0) {
    return;
  }

  Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-sc-get-l-pages' , {
      dateFrom: formatDay(getState().st_googleSearch.dateFrom),
      dateTo: formatDay(getState().st_googleSearch.dateTo),
      site: getState().st_googleSearch.sites[getState().st_googleSearch.currentSiteIndex].siteUrl
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      params: {}
    })
    .then((result) => {
      checkToken(result, history);

      const st_g_loading = getState().st_googleSearch.loading;
      st_g_loading.landingPages = false;

      if (result &&
        result.data &&
        result.data.body){
        dispatch({ type: 'GOOGLE_SEARCH_CHANGE_ALL', payload:
            {
              landingPages: result.data.body,
              loading: st_g_loading
            }
        })
      } else {
        dispatch({ type: 'GOOGLE_SEARCH_CHANGE_ALL', payload:
            {
              landingPages: [],
              loading: st_g_loading
            }
        })
      }
    })
    .catch(err => {
      console.log(err)
    });
}