const {Axios, checkToken} = require('../../components/config/axios');
//const {formatDay} = require('../../components/data/date');

// module.exports = (history) => (dispatch, getState) => Promise.resolve().then(() => {
//   const { someReducer } = getState();
//   return dispatch({
//     type: 'GOOGLE_SEARCH_CHANGE_ALL',
//     payload:
//       {
//         sites: []
//       }
//   });
// });

const func = (history) => (dispatch, getState) => async () => {
  // dispatch({type: 'GOOGLE_SEARCH_LOADING', payload:
  //     ['sites', true]
  // })

  await Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-sc-get-sites' , {
      //dateFrom: storeFilter.dateFrom,
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      params: {}
    })
    .then((result) => {
      checkToken(result, history)

      //console.log(result.data.body)
      const st_g_loading = getState().st_googleSearch.loading;
      st_g_loading.sites = false;

      if (result &&
        result.data &&
        result.data.body){
        dispatch({ type: 'GOOGLE_SEARCH_CHANGE_ALL', payload:
          {
            sites: result.data.body,
            currentSiteUrl: result.data.body.length > 0 ? result.data.body[0].siteUrl : '',
            loading: st_g_loading
          }
        })
      } else {
        dispatch({ type: 'GOOGLE_SEARCH_CHANGE_ALL', payload:
            {
              sites: [],
              currentSiteUrl: '',
              loading: st_g_loading
            }
        })
      }
    })
    .catch(err => {
      console.log(err)
    })
    .finally(function () {
      // dispatch({type: 'GOOGLE_SEARCH_LOADING', payload:
      //     ['sites', false]
      // })
    });
}

export default func;