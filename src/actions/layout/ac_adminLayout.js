export default () => (dispatch, getState) => {
  dispatch({type: 'CALL_RESET'});
  dispatch({type: 'CRO_RESET'});
  dispatch({type: 'GOOGLE_SEARCH_RESET'});
  dispatch({type: 'MANAGEMENT_RESET'});
  dispatch({type: 'SELECTED_EL_RESET'});
  dispatch({type: 'FILTER_RESET'});
}