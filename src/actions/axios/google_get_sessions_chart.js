const {Axios, checkToken} = require('../../components/config/axios');
const {formatDayFromFormat} = require('../../components/data/date.js');

module.exports = (dispatch, getState, history, storeFilter) => {
	dispatch({type: 'LOADING_CHANGE', payload: 
		['sessionsChart', true]
	})
Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-get-sessions' , {
  			dateFrom: storeFilter.dateFrom,
  			dateTo: storeFilter.dateTo,
  			filters: getState().storeFilter.filterStringBackend.result,
				view_id: localStorage.getItem('viewId')
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, history)
    	//console.log(JSON.stringify(result, null, 2));
      if (result && 
      		result['data'] && 
      		result['data']['body'] &&
      		result['data']['body']['reports'][0]['data']['rows']){
      	let resultArray = []
	    	//{
			  // Name: 'Sep 1', Value: 4500
			  //}
      	result['data']['body']['reports'][0]['data']['rows'].forEach((obj) => {
						const day = formatDayFromFormat(obj['dimensions'], 'YYYYMMDD', 'MM.DD.YY');
						const value = parseInt(obj['metrics'][0]['values'][0], 10)

						resultArray.push({
							Name: day,
							Value: value
						})
      	})

      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
	    			sessionsChart: resultArray
	    		}
				})
      	
      } else {
      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
	    			sessionsChart: []
	    		}
				})
      }
    })
    .catch(err => {
      console.log(err)
    })
    .finally(function () {
    	dispatch({type: 'LOADING_CHANGE', payload: 
				['sessionsChart', false]
			})
  	});
}