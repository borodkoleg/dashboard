const {Axios, checkToken} = require('../../components/config/axios');

module.exports = (dispatch, getState, history, storeFilter) => {
	dispatch({type: 'LOADING_CHANGE', payload: 
		['sessionsBySources', true]
	})
Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-sessions-by-sources' , {
  			dateFrom: storeFilter.dateFrom,
  			dateTo: storeFilter.dateTo,
  			filters: getState().storeFilter.filterStringBackend.resultSessionsBySourcesF,
				view_id: localStorage.getItem('viewId')
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, history)

      if (result && 
      		result['data'] && 
      		result['data']['body'] &&
      		result['data']['body']['reports'][0]['data']['rows']){
      	let resultArray = []
	    	//{
			  // 	id: 1,
			  // 	source: 'google', 
			  // 	sessions: '195,566'
			  // }
      	result['data']['body']['reports'][0]['data']['rows'].forEach((obj, index) => {
						const source = obj['dimensions'];
						const sessions = obj['metrics'][0]['values'][0]

						resultArray.push({
							id: index + 1,
							source: source,
							sessions: sessions
						})
      	})

      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
	    			sessionsBySources: resultArray
	    		}
				})
      	
      } else {
      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
	    			sessionsBySources: []
	    		}
				})
      }
    })
    .catch(err => {
      console.log(err)
    })
    .finally(function () {
    	dispatch({type: 'LOADING_CHANGE', payload: 
				['sessionsBySources', false]
			})
  	});
}