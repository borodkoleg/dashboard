const {Axios, checkToken} = require('../../components/config/axios');
const {formatDayFromFormat} = require('../../components/data/date.js');

module.exports = (dispatch, getState, history, storeFilter) => {
	dispatch({type: 'LOADING_CHANGE', payload: 
		['goalConversionChart', true]
	})
Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-get-goal-conversion-rate' , {
  			dateFrom: storeFilter.dateFrom,
  			dateTo: storeFilter.dateTo,
  			filters: getState().storeFilter.filterStringBackend.result,
				view_id: localStorage.getItem('viewId')
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, history)
      if (result && 
      		result['data'] && 
      		result['data']['body'] &&
      		result['data']['body']['reports'][0]['data']['rows']){
      	let resultArray = []
	    	//{
			  // Name: 'Sep 1', Value: 4500
			  //}
      	result['data']['body']['reports'][0]['data']['rows'].forEach((obj) => {
						const day = formatDayFromFormat(obj['dimensions'], 'YYYYMMDD', 'MM.DD.YY');
						const value = +parseFloat(obj['metrics'][0]['values'][0]).toFixed(2)

						resultArray.push({
							Name: day,
							Value: value
						})
      	})

      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
	    			goalConversionRate: resultArray
	    		}
				})
      	
      } else {
      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
	    			goalConversionRate: []
	    		}
				})
      }
    })
    .catch(err => {
      console.log(err)
    })
    .finally(function () {
    	dispatch({type: 'LOADING_CHANGE', payload: 
				['goalConversionChart', false]
			})
  	});
}