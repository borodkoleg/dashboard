const {Axios, checkToken} = require('../../components/config/axios');

module.exports = (dispatch, getState, history, storeFilter) => {

	dispatch({type: 'LOADING_CHANGE', payload: 
		['tableSessions', true]
	})

Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-get-4-metrics' , {
  			dateFrom: storeFilter.dateFrom,
  			dateTo: storeFilter.dateTo,
  			filters: getState().storeFilter.filterStringBackend.result,
				view_id: localStorage.getItem('viewId')
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, history)
    	//console.log(result['data']['body']['reports']);
      if (result && 
      		result['data'] && 
      		result['data']['body'] && 
      		result['data']['body']['reports']){
				const values = result['data']['body']['reports'][0]['data']['totals'][0]['values'];

				const valuesDelta = result['data']['body']['reports'][0]['data']['totals'][1]['values'];

				//console.log(result['data']['body']['reports'][0]['data']['totals'])

				function deltaPercent(value, valueDelta){
					return ((value - valueDelta)/valueDelta) * 100;
				}

				dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
      		TableSessions: {
      			Sessions: values[0],
      			SessionsDelta: deltaPercent(values[0], valuesDelta[0]),
      			Goal: values[1],
      			GoalDelta: deltaPercent(values[1], valuesDelta[1]),
      			Bounces: values[2],
      			BouncesDelta: deltaPercent(values[2], valuesDelta[2]),
      			Avg: values[3],
      			AvgDelta: deltaPercent(values[3], valuesDelta[3])
      		}
      	}
				});
      } else {
      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
      		TableSessions: {
      			Sessions: 0,
      			SessionsDelta: 0,
      			Goal: 0,
      			GoalDelta: 0,
      			Bounces: 0,
      			BouncesDelta: 0,
      			Avg: 0,
      			AvgDelta: 0
      		}
      	}
				});
      }
    })
    .catch(err => {
      console.log(err)
    })
    .finally(function () {
    	dispatch({type: 'LOADING_CHANGE', payload: 
				['tableSessions', false]
			})
  	});
}