const {Axios, checkToken} = require('../../components/config/axios');

module.exports = (dispatch, getState, history, storeFilter) => {
	dispatch({type: 'LOADING_CHANGE', payload: 
		['filters', true]
	})
Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-get-filters' , {
  			dateFrom: storeFilter.dateFrom,
  			dateTo: storeFilter.dateTo,
  			filters: getState().storeFilter.filterStringBackend.result,
				view_id: localStorage.getItem('viewId')
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, history)
      if (result && 
      		result['data'] && 
      		result['data']['body'] && 
      		result['data']['body']['reports']){

      		const responseArray = result['data']['body']['reports'][0]['data']['rows']

      		//console.log(JSON.stringify(responseArray, null, 2));

      		let filterObject = {
      			source: {},
      			medium: {},
      			deviceCategory: {},
      			channelGrouping: {},
      			country: {}
      		}
      		responseArray.forEach((obj) => {
      			const metricValue = parseInt(obj['metrics'][0]['values'][0], 10);

      			if (!filterObject['source'][obj['dimensions'][0]]) {filterObject['source'][obj['dimensions'][0]] = 0
      			}
      			if (!filterObject['medium'][obj['dimensions'][1]]) {filterObject['medium'][obj['dimensions'][1]] = 0
      			}
      			if (!filterObject['deviceCategory'][obj['dimensions'][2]]) {filterObject['deviceCategory'][obj['dimensions'][2]] = 0
      			}
      			if (!filterObject['channelGrouping'][obj['dimensions'][3]]) {filterObject['channelGrouping'][obj['dimensions'][3]] = 0
      			}
      			if (!filterObject['country'][obj['dimensions'][4]]) {filterObject['country'][obj['dimensions'][4]] = 0
      			}

      			filterObject['source'][obj['dimensions'][0]] = 
      			filterObject['source'][obj['dimensions'][0]] + 
      			metricValue

      			filterObject['medium'][obj['dimensions'][1]] = 
      			filterObject['medium'][obj['dimensions'][1]] + 
      			metricValue

      			filterObject['deviceCategory'][obj['dimensions'][2]] = 
      			filterObject['deviceCategory'][obj['dimensions'][2]] + 
      			metricValue

      			filterObject['channelGrouping'][obj['dimensions'][3]] = 
      			filterObject['channelGrouping'][obj['dimensions'][3]] + 
      			metricValue

      			filterObject['country'][obj['dimensions'][4]] = 
      			filterObject['country'][obj['dimensions'][4]] + 
      			metricValue
      		});

      		let filterObjectResult = {
      			source: [],
      			medium: [],
      			deviceCategory: [],
      			channelGrouping: [],
      			country: []
      		}

      		function toResult(filterObjectKey){
      			let result = []
      			let iterator = 1;
	      		Object.keys(filterObjectKey).forEach((key) => {
	      			result.push({
	      		 		key: iterator, value: key, number: filterObjectKey[key]
	      		 	})
	      		 	iterator += 1;
	      		})
	      		return result
      		}

      		//for MultiSelect
      		Object.keys(filterObject).forEach((key) => {
      			filterObjectResult[key] = toResult(filterObject[key])
      		});

      		//console.log(JSON.stringify(getFilters(storeFilter), null, 2));

					dispatch({type: 'FILTER_CHANGE_ALL', payload: 
						{
							filtersList: filterObjectResult,
							filtersListSelect: filterObjectResult
						}
					})
      } else {
      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
						filtersList: {},
						filtersListSelect:{}
					}
				})
      }
    })
    .catch(err => {
      console.log(err)
    })
    .finally(function () {
    	dispatch({type: 'LOADING_CHANGE', payload: 
				['filters', false]
			})
  	});
}