const {Axios, checkToken} = require('../../components/config/axios');
//const {formatDayFromFormat} = require('../../components/data/date.js');

module.exports = (dispatch, getState, history, storeFilter) => {
	dispatch({type: 'LOADING_CHANGE', payload: 
		['sessionsByUserType', true]
	})
Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-sessions-by-user-type' , {
  			dateFrom: storeFilter.dateFrom,
  			dateTo: storeFilter.dateTo,
  			filters: getState().storeFilter.filterStringBackend.result,
				view_id: localStorage.getItem('viewId')
  		},
    	{
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        params: {}
    })
    .then((result) => {
    	checkToken(result, history)
      if (result && 
      		result['data'] && 
      		result['data']['body'] &&
      		result['data']['body']['reports'][0]['data']){

      		const data = result['data']['body']['reports'][0]['data']

      		//let totals = data['totals'][0]['values'][0]
      		let newVisitor = data['rows'][0]['metrics'][0]['values'][0]
      		let returningVisitor = data['rows'][1]['metrics'][0]['values'][0]

	      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
						{
		    			sessionsByUserType: [
		    				{ name: 'New Visitor', value: parseInt(newVisitor, 10) },
  							{ name: 'Returning Visitor', value: parseInt(returningVisitor, 10) }
		    			]
		    		}
					})
      	
      } else {
      	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
					{
	    			sessionsByUserType: []
	    		}
				})
      }
    })
    .catch(err => {
      console.log(err)
    })
    .finally(function () {
    	dispatch({type: 'LOADING_CHANGE', payload: 
				['sessionsByUserType', false]
			})
  	});
}