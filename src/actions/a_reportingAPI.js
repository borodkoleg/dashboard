
const googleGet4Metrics = require('./axios/google_get_4_metrics')
const googleGetFilters = require('./axios/google_get_filters')
const googleGetSessionsChart = require('./axios/google_get_sessions_chart')
const googleGetSessionsBySources = require('./axios/google_get_sessions_by_sources')
const googleGetGoalConversionRateChart = require('./axios/google_get_goal_conversion_rate_chart')
const googleGetSessionsByUserType = require('./axios/google_get_sessions_by_user_type')

const getCall = require('./callTracking/getCall')

//const {diffBetweenDate, dayFromMinusDiffDays} = require('../components/data/date')
//const moment = require('moment');

function getFilters(dataStoreFilter){
	let filtersOthers = ''
	let filtersSources = ''
	Object.keys(dataStoreFilter).forEach((key) => {
			if (key === 'source'){
				dataStoreFilter[key].forEach((obj) => {
					filtersSources += `ga:${key}==${obj.value},`
				});
			} else {
				dataStoreFilter[key].forEach((obj) => {
					filtersOthers += `ga:${key}==${obj.value},`
				});
			}

			if (filtersOthers.length > 0) {
				filtersOthers = filtersOthers.substring(0, filtersOthers.length - 1);
				filtersOthers +=';'
			}
			if (filtersSources.length > 0) {
				filtersSources = filtersSources.substring(0, filtersSources.length - 1);
				filtersSources +=';'
			}

	});
	if (filtersOthers.length > 0) {
				filtersOthers = filtersOthers.substring(0, filtersOthers.length - 1);
			}
	if (filtersSources.length > 0) {
		filtersSources = filtersSources.substring(0, filtersSources.length - 1);
	}

	return {
		sources: filtersSources,
		others: filtersOthers
	};
}

function getConcatFilter(filterObj){
		let result = ''
		let resultSBySF = ''

		let sources = filterObj['sources']
		let sessionsBySourcesF = filterObj['sessionsBySourcesF']
		let others = filterObj['others']
		let userType = filterObj['userType']

		if (sessionsBySourcesF.length > 0){
			result += `${sessionsBySourcesF};`
		} else {
			result += `${sources};`
		}

		resultSBySF += `${sources};`

		if (others.length > 0){
			result += `${others};`
			resultSBySF += `${others};`
		}

		if (userType.length > 0){
			result += `${userType};`
			resultSBySF += `${userType};`
		}

		if (result.charAt(0) === ';') {
			result = result.slice(1, result.length)
		}
		if (result.charAt(result.length-1) === ';') {
			result = result.slice(0,-1)
		}

		if (resultSBySF.charAt(0) === ';') {
			resultSBySF = resultSBySF.slice(1, resultSBySF.length)
		}
		if (resultSBySF.charAt(resultSBySF.length-1) === ';') {
			resultSBySF = resultSBySF.slice(0,-1)
		}

	return {
		sources: filterObj['sources'],
		sessionsBySourcesF: filterObj['sessionsBySourcesF'],
		others: filterObj['others'],
		userType: filterObj['userType'],
		result: result,
		resultSessionsBySourcesF: resultSBySF
	}
}

export const g_updateData = (history, storeFilter) => (dispatch, getState) => {
	let getStateNow = getState()

	if (getStateNow.st_selected_elements.tableSessions){
		googleGet4Metrics(dispatch, getState, history, storeFilter)
	}
	if (getStateNow.st_selected_elements.sessionsChart){
		googleGetSessionsChart(dispatch, getState, history, storeFilter)
	}
	if (getStateNow.st_selected_elements.goalConversionRate){
		googleGetGoalConversionRateChart(dispatch, getState, history, storeFilter)
	}
	if (getStateNow.st_selected_elements.sessionsBySources){
		googleGetSessionsBySources(dispatch, getState, history, storeFilter)
	}
	if (getStateNow.st_selected_elements.sessionsByUserType){
		googleGetSessionsByUserType(dispatch, getState, history, storeFilter)
	}

	if (getStateNow.st_selected_elements.tableCalls){
		getCall(dispatch, getState, history)
	}
}

//=================================

export const g_getFilters = (history, storeFilter) => (dispatch, getState) => {

	//let getStateNow = getState() //temp!

	//if (getStateNow.st_selected_elements.ifOneIsTrue){ //temp!
		googleGetFilters(dispatch, getState, history, storeFilter)
	//} //temp!
}

export const g_beforeFilter = (history, storeFilter) => (dispatch, getState) => {

	const result = getFilters(storeFilter.filtersListSelect);

	let nowFilter = storeFilter.filterStringBackend;
	nowFilter['sources'] = result['sources']
	nowFilter['others'] = result['others']

	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
		{
			filterStringBackend: getConcatFilter(nowFilter)
		}
	})
}

export const g_updateDataForSessions = (history, storeFilter, sources) => (dispatch, getState) => {

	let resultString = '';
	sources.forEach((obj) => {
		resultString += `ga:source==${obj['source'][0]},`
	});
	if (resultString.length>0){
		resultString = resultString.slice(0,-1);
	}

	let nowFilter = storeFilter.filterStringBackend;
	nowFilter['sessionsBySourcesF'] = resultString
	
	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
		{
			filterStringBackend: getConcatFilter(nowFilter)
		}
	})

	googleGet4Metrics(dispatch, getState, history, storeFilter)
	googleGetSessionsChart(dispatch, getState, history, storeFilter)
	googleGetGoalConversionRateChart(dispatch, getState, history, storeFilter)

}

export const g_updateDataSessionsByUserType = (history, storeFilter, data) => (dispatch, getState) => {
	let nowFilter = storeFilter.filterStringBackend;

	if (nowFilter['userType'].length === 0){
		nowFilter['userType'] = `ga:userType==${data['name']}`
	} else {
		nowFilter['userType'] = ''
	}
	
	dispatch({type: 'FILTER_CHANGE_ALL', payload: 
		{
			filterStringBackend: getConcatFilter(nowFilter)
		}
	})

	let getStateNow = getState()

	if (getStateNow.st_selected_elements.tableSessions){
		googleGet4Metrics(dispatch, getState, history, storeFilter)
	}
	if (getStateNow.st_selected_elements.sessionsChart){
		googleGetSessionsChart(dispatch, getState, history, storeFilter)
	}
	if (getStateNow.st_selected_elements.goalConversionRate){
		googleGetGoalConversionRateChart(dispatch, getState, history, storeFilter)
	}
	if (getStateNow.st_selected_elements.sessionsBySources){
		googleGetSessionsBySources(dispatch, getState, history, storeFilter)
	}

}