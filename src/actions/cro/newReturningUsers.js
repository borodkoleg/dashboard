const {Axios, checkToken} = require('../../components/config/axios');
const {today, dayMinusDays} = require('../../components/data/date');

export default (history) => (dispatch, getState) => {
  // dispatch({type: 'CRO_LOADING', payload:
  //     ['newReturningUsers', true]
  // });

  Axios.post(process.env.REACT_APP_HOST_PORT + '/api/google-sessions-by-user-type' , {
      dateFrom: dayMinusDays(today, 30),
      dateTo: today,
      filters: '',
      view_id: localStorage.getItem('viewId')
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      params: {}
    })
    .then((result) => {
      checkToken(result, history);
      const loading = getState().st_cro.loading;
      loading.newReturningUsers = false;

      if (result &&
        result['data'] &&
        result['data']['body'] &&
        result['data']['body']['reports'][0]['data']){

        const data = result['data']['body']['reports'][0]['data']

        //let totals = data['totals'][0]['values'][0]
        let newVisitor = data['rows'][0]['metrics'][0]['values'][0]
        let returningVisitor = data['rows'][1]['metrics'][0]['values'][0]

        dispatch({type: 'CRO_CHANGE_ALL', payload:
            {
              newReturningUsers: [
                { name: 'New Users', value: parseInt(newVisitor, 10) },
                { name: 'Returning Users', value: parseInt(returningVisitor, 10) }
              ],
              loading: loading
            }
        })

      } else {
        dispatch({type: 'CRO_CHANGE_ALL', payload:
            {
              newReturningUsers: [],
              loading: loading
            }
        })
      }
    })
    .catch(err => {
      console.log(err)
    });
}