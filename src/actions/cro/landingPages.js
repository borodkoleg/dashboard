const {Axios, checkToken} = require('../../components/config/axios');
const {today, dayMinusDays} = require('../../components/data/date');

export default (history) => (dispatch, getState) => {

  Axios.post(process.env.REACT_APP_HOST_PORT + '/api/cro-get-landing-pages' , {
      dateFrom: dayMinusDays(today, 30),
      dateTo: today,
      //filters: '',
      view_id: localStorage.getItem('viewId')
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      params: {}
    })
    .then((result) => {
      checkToken(result, history);
      const loading = getState().st_cro.loading;
      loading.landingPages = false;

      if (result &&
        result['data'] &&
        result['data']['body'] &&
        result['data']['body']['reports'][0]['data']['rows']) {

        let resultArray = []

        result['data']['body']['reports'][0]['data']['rows'].forEach((obj, index) => {
          const pages = obj['dimensions'];
          const entrances = obj['metrics'][0]['values'][0];
          const bounces = obj['metrics'][0]['values'][1];

          resultArray.push({
            id: index + 1,
            pages: pages,
            entrances: entrances,
            bounces: bounces
          })
        });

        dispatch({type: 'CRO_CHANGE_ALL', payload:
          {
            landingPages: resultArray,
            loading: loading
          }
        })

      } else {
        dispatch({type: 'CRO_CHANGE_ALL', payload:
            {
              landingPages: [],
              loading: loading
            }
        })
      }
    })
    .catch(err => {
      console.log(err)
    });
}