const {Axios, checkToken} = require('../../components/config/axios');
const {today, dayMinusDays} = require('../../components/data/date');

export default (history) => (dispatch, getState) => {

  Axios.post(process.env.REACT_APP_HOST_PORT + '/api/cro-get-channels-grouped' , {
      dateFrom: dayMinusDays(today, 30),
      dateTo: today,
      //filters: '',
      view_id: localStorage.getItem('viewId')
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      params: {}
    })
    .then((result) => {
      checkToken(result, history);
      const loading = getState().st_cro.loading;
      loading.groupedChannels = false;

      if (result &&
        result['data'] &&
        result['data']['body'] &&
        result['data']['body']['reports'][0]['data']['rows']) {

        let resultArray = [];

        result['data']['body']['reports'][0]['data']['rows'].forEach((obj, index) => {
          const channels = obj['dimensions'];
          const entrances = obj['metrics'][0]['values'][0];
          const goalConversionRate = obj['metrics'][0]['values'][1];

          resultArray.push({
            id: index + 1,
            channels: channels,
            entrances: entrances,
            goalConversionRate: goalConversionRate
          })
        });

        dispatch({type: 'CRO_CHANGE_ALL', payload:
            {
              groupedChannels: resultArray,
              loading: loading,
              viewIdCro: localStorage.getItem('viewId')
            }
        })

      } else {
        dispatch({type: 'CRO_CHANGE_ALL', payload:
            {
              groupedChannels: [],
              loading: loading,
              viewIdCro: ''
            }
        })
      }
    })
    .catch(err => {
      console.log(err)
    });
}