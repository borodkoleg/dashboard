require('dotenv').config()

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('monitors').del()
    .then(function () {
      // Inserts seed entries 	
  		return knex('monitors').insert([
    	{
    		id: 1, 
    		code: '1', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 2, 
    		code: '2', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 3, 
    		code: '3', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 4, 
    		code: '4', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 5, 
    		code: '5', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 6, 
    		code: '6', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '18',
    		year: '2018'
    	},
    	{
    		id: 7, 
    		code: '7', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 8, 
    		code: '8', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2019'
    	},
    	{
    		id: 9, 
    		code: '9', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 10, 
    		code: 'WE', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 11, 
    		code: 'NB', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '22.1',
    		year: '2019'
    	},
    	{
    		id: 12, 
    		code: 'T', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 13, 
    		code: '11A', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 14, 
    		code: '12R', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '23',
    		year: '2018'
    	},
    	{
    		id: 15, 
    		code: '13S', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2017'
    	},
    	{
    		id: 16, 
    		code: '0', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '13',
    		year: '2018'
    	},
    	{
    		id: 17, 
    		code: '-1', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 18, 
    		code: '18-19', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 19, 
    		code: '%', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2018'
    	},
    	{
    		id: 20, 
    		code: 'U2', 
    		brand: 'SAMSUNG',
    		color: 'black',
    		type: 'for work',
    		diagonal: '15',
    		year: '2020'
    	}
  		]);
    });
};