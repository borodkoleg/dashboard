exports.up = knex => knex.schema.createTable('monitors', (table) => {
  table.increments('id').primary();
  table.string('code').notNullable();
  table.string('brand').notNullable();
  table.string('color').notNullable();
  table.string('type');
  table.float('diagonal');
  table.string('year');

	table.timestamp('created_at').defaultTo(knex.fn.now());

  table.unique('id');
  table.unique('code');
});

exports.down = knex => knex.schema.dropTable('monitors');