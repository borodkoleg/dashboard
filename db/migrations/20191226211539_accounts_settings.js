exports.up = knex => knex.schema.createTable('accounts_settings', (table) => {
  table.increments('id').primary();

  table.integer('account_id').notNullable();
  table.date('min_date').notNullable();
  table.date('max_date').notNullable();

	table.timestamp('created_at').defaultTo(knex.fn.now());

  table.unique('id');
});

exports.down = knex => knex.schema.dropTable('accounts_settings');

