exports.up = knex => knex.schema.createTable('users', (table) => {
  table.increments('id').primary();
  table.string('userId').notNullable();
  table.string('key').notNullable();
  table.string('def_profile');

	table.timestamp('created_at').defaultTo(knex.fn.now());

  table.unique('userId');
  table.unique('id');
});

exports.down = knex => knex.schema.dropTable('users');