exports.up = knex => knex.schema.createTable('images', (table) => {
  table.increments('id').primary();
  table.string('image');
  table.string('title').notNullable();

	table.timestamp('created_at').defaultTo(knex.fn.now());

  table.unique('id');
  table.unique('title');
});

exports.down = knex => knex.schema.dropTable('images');