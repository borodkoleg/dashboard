exports.up = knex => knex.schema.createTable('accounts_calls', (table) => {
  table.increments('id').primary();

  table.integer('account_id').notNullable();
  table.date('date').notNullable();
  table.mediumtext('calls');

	table.timestamp('created_at').defaultTo(knex.fn.now());

  table.unique('id');
});

exports.down = knex => knex.schema.dropTable('accounts_calls');
